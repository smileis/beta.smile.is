
var base_url_m = "loadPageMethod/";
var base_url_c = "loadPageClass/";

    function refresh_data_ajax_class(target_class,target_method,target_container,source_form){
        //jquery selector (get element by id)
        var container = $("#"+target_container+"");
        container.html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
        var data_from_form = $('#'+source_form).serialize();
		
        $.ajax({
            'url' : base_url_c +  target_class + "/" +target_method, //define this target_method in ajax class
            'type' : 'POST', //the way you want to send data to your URL
            'data' : data_from_form,
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                //fill some container id name
                if(data){
                 container.html(data);
                }
            }
        });
    }

    function load_data_ajax_class(target_class,target_method, target_container,source_form){
		var data_from_form = $('#'+source_form).serialize();
		//alert(data_from_form);
        $.ajax({
            'url' : base_url_c +  target_class + "/" +  target_method, //define this target_method in ajax class,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : data_from_form,
            'success' : function(data){ 
			//probably this request will return anything, it'll be put in var "data"
                // do nothing after operations
				
                var container = $("#"+target_container+"");
                 if(data){
                     container.html(data);
                 }
            }
        });
    }

    function alert_data_ajax_class(target_class,target_method, target_container){
        $.ajax({
            'url' : base_url_c +  target_class + "/" +  target_method, //define this target_method in ajax class,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : 'val = demo',
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                alert(data);
                var container = $("#"+target_container+"");
                 if(data){
                     container.html(data);
                 }
            }
        });
    }

    function post_data_ajax_class(target_class,method,form_id){
		alert(form_id);
        var formid = $('#'+form_id).serialize();
        alert(formid);
        $.ajax({
            'url' : base_url_c +  target_class + "/" +method,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : formid,
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                alert(data);
            }
        });
    }

    function refresh_data_ajax_method(target_method,target_container,source_form){
        //jquery selector (get element by id)
        var container = $("#"+target_container+"");
        container.html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
        var data_from_form = $('#'+source_form).serialize();

        $.ajax({
            'url' : base_url_m +  target_method, //define this target_method in ajax class
            'type' : 'POST', //the way you want to send data to your URL
            'data' : data_from_form,
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                //fill some container id name
                if(data){
                 container.html(data);
                }
            }
        });
    }

    function load_data_ajax_method(target_method, target_container){
        $.ajax({
            'url' : base_url_m +  target_method, //define this target_method in ajax class,
            'type' : 'GET', //the way you want to send data to your URL
            'data' : 'val = demo',
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                // do nothing after operations
                var container = $("#"+target_container+"");
                 if(data){
                     container.html(data);
                 }
            }
        });
    }

    function alert_data_ajax_method(target_method, target_container){
        $.ajax({
            'url' : base_url_m +  target_method, //define this target_method in ajax class,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : 'val = demo',
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                alert(data);
                var container = $("#"+target_container+"");
                 if(data){
                     container.html(data);
                 }
            }
        });
    }

    function post_data_ajax_method(method,form_id){
     
        var formid = $('#'+form_id).serialize();
        //alert(base_url +  method + formid);
        $.ajax({
            'url' : base_url_m +  method,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : formid,
            'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
                alert(data);
            }
        });
    }
