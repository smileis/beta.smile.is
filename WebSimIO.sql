CREATE DATABASE  IF NOT EXISTS `samperk1_smile` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `samperk1_smile`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: samperkstech.com    Database: samperk1_smile
-- ------------------------------------------------------
-- Server version	5.5.37-35.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `ADMIN_Menu`
--

DROP TABLE IF EXISTS `ADMIN_Menu`;
/*!50001 DROP VIEW IF EXISTS `ADMIN_Menu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ADMIN_Menu` AS SELECT 
 1 AS `id`,
 1 AS `menuname`,
 1 AS `description`,
 1 AS `urlname`,
 1 AS `parentid`,
 1 AS `position`,
 1 AS `status`,
 1 AS `class`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ADMIN_Menu_Parent`
--

DROP TABLE IF EXISTS `ADMIN_Menu_Parent`;
/*!50001 DROP VIEW IF EXISTS `ADMIN_Menu_Parent`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ADMIN_Menu_Parent` AS SELECT 
 1 AS `id`,
 1 AS `menuname`,
 1 AS `description`,
 1 AS `urlname`,
 1 AS `parentid`,
 1 AS `position`,
 1 AS `status`,
 1 AS `class`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ADMIN_Widget_View`
--

DROP TABLE IF EXISTS `ADMIN_Widget_View`;
/*!50001 DROP VIEW IF EXISTS `ADMIN_Widget_View`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ADMIN_Widget_View` AS SELECT 
 1 AS `id`,
 1 AS `widgetName`,
 1 AS `widgetStatus`,
 1 AS `widgetVersion`,
 1 AS `actiontype`,
 1 AS `columns`,
 1 AS `display_name`,
 1 AS `fields`,
 1 AS `setrelation`,
 1 AS `table_name`,
 1 AS `unsetfields`,
 1 AS `actions`,
 1 AS `afterinsert`,
 1 AS `afterupdate`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `UI_Footer`
--

DROP TABLE IF EXISTS `UI_Footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `footerurl` varchar(100) DEFAULT NULL,
  `code` longtext,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Footer`
--

LOCK TABLES `UI_Footer` WRITE;
/*!40000 ALTER TABLE `UI_Footer` DISABLE KEYS */;
INSERT INTO `UI_Footer` VALUES (1,'frontend/include/footer.tpl','<footer>\n	<p>\n		Pieces put together by Sachin</p>\n</footer>\n<!-- Placed at the end of the document so the pages load faster --><!-- Placed at the end of the document so the pages load faster --><script src=\"<?php echo base_url(\'assets/js/jquery.js\'); ?>\"></script><script src=\"<?php echo base_url(\"assets/js/respond.min.js\"); ?>\"></script><script src=\"<?php echo base_url(\'assets/js/bootstrap.min.js\'); ?>\"></script>','Basic login footer');
/*!40000 ALTER TABLE `UI_Footer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Header`
--

DROP TABLE IF EXISTS `UI_Header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headerurl` varchar(100) DEFAULT NULL,
  `code` longtext,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Header`
--

LOCK TABLES `UI_Header` WRITE;
/*!40000 ALTER TABLE `UI_Header` DISABLE KEYS */;
INSERT INTO `UI_Header` VALUES (1,'frontend/include/demoheader1.tpl','<div style=\"font-size:8px;\">\n	heelo</div>\n','demoheader');
/*!40000 ALTER TABLE `UI_Header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Page`
--

DROP TABLE IF EXISTS `UI_Page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(30) NOT NULL,
  `pageurl` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_layouts_id` mediumint(8) unsigned NOT NULL,
  `templateurl` varchar(100) DEFAULT NULL,
  `UI_Header_id` int(11) NOT NULL,
  `UI_Footer_id` int(11) NOT NULL,
  `UI_Templates_id` int(11) NOT NULL,
  `variables` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`,`admin_layouts_id`,`UI_Header_id`,`UI_Footer_id`,`UI_Templates_id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `fk_UI_Page_admin_layouts1_idx` (`admin_layouts_id`),
  KEY `fk_UI_Page_UI_Header1_idx` (`UI_Header_id`),
  KEY `fk_UI_Page_UI_Footer1_idx` (`UI_Footer_id`),
  KEY `fk_UI_Page_UI_Templates1_idx` (`UI_Templates_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Page`
--

LOCK TABLES `UI_Page` WRITE;
/*!40000 ALTER TABLE `UI_Page` DISABLE KEYS */;
INSERT INTO `UI_Page` VALUES (1,'Home','home',1,2,'frontend/home.tpl',0,0,0,'3,4,5,6'),(2,'Profile','profile',1,4,'frontend/profile.tpl',0,0,0,'3,4,5,6'),(4,'Settings','settings',1,2,'frontend/settings.tpl',0,0,0,'3,4,5');
/*!40000 ALTER TABLE `UI_Page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Page_blocks`
--

DROP TABLE IF EXISTS `UI_Page_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Page_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `colrowno` varchar(10) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `UI_Page_id` int(11) NOT NULL,
  `admin_blocks_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`,`UI_Page_id`,`admin_blocks_id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `fk_UI_Page_blocks_UI_Page1_idx` (`UI_Page_id`),
  KEY `fk_UI_Page_blocks_admin_blocks1_idx` (`admin_blocks_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Page_blocks`
--

LOCK TABLES `UI_Page_blocks` WRITE;
/*!40000 ALTER TABLE `UI_Page_blocks` DISABLE KEYS */;
INSERT INTO `UI_Page_blocks` VALUES (1,'1:1',1,1,1,2),(2,'1:2',1,1,1,2),(3,'1:1',2,1,1,2),(4,'1:1',1,1,2,2),(5,'1:2',1,1,2,2),(6,'2:1',1,1,2,2),(7,'1:2',2,1,1,2),(8,'2:2',1,1,2,2),(9,'1:1',1,1,4,2),(10,'1:2',1,1,4,2);
/*!40000 ALTER TABLE `UI_Page_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Page_modules`
--

DROP TABLE IF EXISTS `UI_Page_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Page_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `UI_Page_id` int(11) NOT NULL,
  `admin_modules_id` mediumint(8) unsigned NOT NULL,
  `UI_Page_blocks_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`UI_Page_id`,`admin_modules_id`,`UI_Page_blocks_id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `fk_UI_Page_modules_UI_Page1_idx` (`UI_Page_id`),
  KEY `fk_UI_Page_modules_admin_modules1_idx` (`admin_modules_id`),
  KEY `fk_UI_Page_modules_UI_Page_blocks1_idx` (`UI_Page_blocks_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Page_modules`
--

LOCK TABLES `UI_Page_modules` WRITE;
/*!40000 ALTER TABLE `UI_Page_modules` DISABLE KEYS */;
INSERT INTO `UI_Page_modules` VALUES (1,1,1,1,4,1),(2,1,1,1,6,2),(3,1,1,1,5,3),(4,1,1,2,4,4),(5,1,1,2,5,5),(6,1,1,2,1,6),(7,1,1,1,7,7),(11,1,1,4,8,9),(10,1,1,2,7,8),(12,1,1,4,9,10);
/*!40000 ALTER TABLE `UI_Page_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Templates`
--

DROP TABLE IF EXISTS `UI_Templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateurl` varchar(100) DEFAULT NULL,
  `code` longtext,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Templates`
--

LOCK TABLES `UI_Templates` WRITE;
/*!40000 ALTER TABLE `UI_Templates` DISABLE KEYS */;
INSERT INTO `UI_Templates` VALUES (1,'demotemp.tpl','<div>\n	adsa</div>\n','DemoTPL'),(2,'frontend/home.tpl','<div class=\"container\">\n	<div class=\"row clearfix\">\n		<div class=\"col-md-3 column\">\n			<div class=\"panel panel-default\">\n				<div class=\"panel-body\" style=\"padding: 1px!important;\">\n					<a class=\"btn\" data-toggle=\"modal\" href=\"#modal-container-{$modal_id}\" id=\"modal-{$modal_id}\" role=\"button\"><img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/210/210/\" width=\"100%\" /> </a>\n					<div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" class=\"modal fade\" id=\"modal-container-{$modal_id}\" role=\"dialog\">\n						<div class=\"modal-dialog\">\n							<div class=\"modal-content\">\n								<div class=\"modal-header\">\n									<button aria-hidden=\"true\" class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>\n									<h4 class=\"modal-title\" id=\"myModalLabel\">\n										{$modal_title}</h4>\n								</div>\n								<div class=\"modal-body\">\n									POST /....</div>\n								<div class=\"modal-footer\">\n									<button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button><button class=\"btn btn-primary\" type=\"button\">Save changes</button></div>\n							</div>\n						</div>\n					</div>\n					<span class=\"label label-default\">{$userdetails.firstname} {$userdetails.lastname} </span><br />\n					<span class=\"label label-default\">{$userdetails.username} </span><br />\n					<span class=\"label label-default\">Last Logged--in: {$userdetails.userlastlogin} </span></div>\n			</div>\n			<p>\n				&nbsp;</p>\n			<div class=\"panel panel-default\">\n				<div class=\"panel-body\" style=\"padding: 1px!important;\">\n					<div class=\"list-group\">\n						<a class=\"list-group-item active\" href=\"#\">Friends</a>\n						<div class=\"list-group-item text-center\">\n							<img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /></div>\n						<div class=\"list-group-item text-center\">\n							<img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /></div>\n						<div class=\"list-group-item text-center\">\n							<img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /> <img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/42/42/\" /></div>\n						<a class=\"list-group-item active\"><span class=\"badge\">14</span>Help</a></div>\n				</div>\n			</div>\n		</div>\n		<div class=\"col-md-9 column\">\n			<div class=\"panel panel-default\">\n				<div class=\"panel-heading\">\n					<h3 class=\"panel-title\">\n						Smile Newsfeed</h3>\n				</div>\n				<div class=\"panel-body\">\n					<div class=\"media well\">\n						<a class=\"pull-left\" href=\"#\"><img alt=\"\" class=\"media-object\" src=\"http://lorempixel.com/64/64/\" /></a>\n						<div class=\"media-body\">\n							<h4 class=\"media-heading\">\n								Nested media heading</h4>\n							Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.\n							<div class=\"media\">\n								<a class=\"pull-left\" href=\"#\"><img alt=\"\" class=\"media-object\" src=\"http://lorempixel.com/64/64/\" /></a>\n								<div class=\"media-body\">\n									<h4 class=\"media-heading\">\n										Nested media heading</h4>\n									Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>\n							</div>\n						</div>\n					</div>\n				</div>\n				<div class=\"panel-footer\">\n					<ul class=\"pagination\">\n						<li>\n							<a href=\"#\">Prev</a></li>\n						<li>\n							<a href=\"#\">1</a></li>\n						<li>\n							<a href=\"#\">2</a></li>\n						<li>\n							<a href=\"#\">3</a></li>\n						<li>\n							<a href=\"#\">4</a></li>\n						<li>\n							<a href=\"#\">5</a></li>\n						<li>\n							<a href=\"#\">Next</a></li>\n					</ul>\n				</div>\n			</div>\n			<p>\n				hfslahbhbhgabba</p>\n			<p>\n				{$demotest}</p>\n			<p>\n				&nbsp;</p>\n		</div>\n	</div>\n</div>\n<p>\n	&nbsp;</p>\n','Home Template File'),(3,'frontend/settings.tpl','<div class=\"container\">\n	<div class=\"row clearfix\">\n		<div class=\"col-md-3 column\">\n			<ul class=\"nav nav-pills nav-stacked\">\n				<li class=\"active\">\n					<a href=\"#\">Home</a></li>\n				<li>\n					<a href=\"#\">Profile</a></li>\n				<li class=\"disabled\">\n					<a href=\"#\">Messages</a></li>\n			</ul>\n		</div>\n		<div class=\"col-md-9 column\">\n			<div class=\"panel panel-primary\">\n				<div class=\"panel-heading\">\n					<h3 class=\"panel-title\">\n						Panel title</h3>\n				</div>\n				<div class=\"panel-body\">\n					Panel content</div>\n				<div class=\"panel-footer\">\n					Panel footer</div>\n			</div>\n		</div>\n	</div>\n</div>\n<p>\n	demo</p>\n','Settings Template File'),(4,'frontend/profile.tpl','<div class=\"container\"><div class=\"row clearfix\"><div class=\"col-md-2 column\"><div class=\"panel panel-default\">\n	<div class=\"panel-body\" style=\"padding: 1px!important;\">\n		<a class=\"btn\" data-toggle=\"modal\" href=\"#modal-container-{$modal_id}\" id=\"modal-{$modal_id}\" role=\"button\"><img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/210/210/\" width=\"100%\" /> </a>\n		<div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" class=\"modal fade\" id=\"modal-container-{$modal_id}\" role=\"dialog\">\n			<div class=\"modal-dialog\">\n				<div class=\"modal-content\">\n					<div class=\"modal-header\">\n						<button aria-hidden=\"true\" class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>\n						<h4 class=\"modal-title\" id=\"myModalLabel\">\n							{$modal_title}</h4>\n					</div>\n					<div class=\"modal-body\">\n						POST /....</div>\n					<div class=\"modal-footer\">\n						<button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button><button class=\"btn btn-primary\" type=\"button\">Save changes</button></div>\n				</div>\n			</div>\n		</div>\n		<span class=\"label label-default\">{$userdetails.firstname} {$userdetails.lastname} </span><br />\n		<span class=\"label label-default\">{$userdetails.username} </span><br />\n		<span class=\"label label-default\">Last Loggedin: {$userdetails.userlastlogin} </span></div>\n</div>\n<p>\n	&nbsp;</p>\n</div><div class=\"col-md-10 column\"><div class=\"panel panel-default\">\n        <div class=\"panel-body\" style=\"padding: 1px!important;\">\n            <div class=\"list-group\">\n                <a href=\"#\" class=\"list-group-item active\">Friends</a>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n\n                <a class=\"list-group-item active\"><span class=\"badge\">14</span>Help</a>\n            </div>\n        </div>\n    </div></div></div><div class=\"row clearfix\"><div class=\"col-md-6 column\"><div style=\"color:red;\">Hello,\n	{$userdetails.firstname}!</div>\n</div><div class=\"col-md-6 column\"><p>\n	hfslahbhbhgabba</p>\n<p>\n	{$demotest}</p>\n<p>\n	&nbsp;</p>\n</div></div></div>','Profile Template File');
/*!40000 ALTER TABLE `UI_Templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UI_Variables`
--

DROP TABLE IF EXISTS `UI_Variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UI_Variables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `function_name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UI_Variables`
--

LOCK TABLES `UI_Variables` WRITE;
/*!40000 ALTER TABLE `UI_Variables` DISABLE KEYS */;
INSERT INTO `UI_Variables` VALUES (3,'modalVar','loadModalVars'),(4,'userdetails','loadUserDetails'),(5,'headerVar','loadHeaderVar'),(6,'demotestVar','loadDemotest');
/*!40000 ALTER TABLE `UI_Variables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_block_config`
--

DROP TABLE IF EXISTS `admin_block_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_block_config` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `blockparentid` int(11) DEFAULT NULL,
  `blockparent` int(11) DEFAULT NULL,
  `blockcode` longtext,
  `admin_blocks_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`,`admin_blocks_id`),
  KEY `fk_admin_block_config_admin_blocks1_idx` (`admin_blocks_id`),
  CONSTRAINT `fk_admin_block_config_admin_blocks1` FOREIGN KEY (`admin_blocks_id`) REFERENCES `admin_blocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_block_config`
--

LOCK TABLES `admin_block_config` WRITE;
/*!40000 ALTER TABLE `admin_block_config` DISABLE KEYS */;
INSERT INTO `admin_block_config` VALUES (1,NULL,NULL,'<div>\n	--</div>\n',1),(2,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `admin_block_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_blocks`
--

DROP TABLE IF EXISTS `admin_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_blocks` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `blockname` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_blocks`
--

LOCK TABLES `admin_blocks` WRITE;
/*!40000 ALTER TABLE `admin_blocks` DISABLE KEYS */;
INSERT INTO `admin_blocks` VALUES (1,'simpleBlock',1,'2014-10-23 05:04:14'),(2,'noBlock',1,'2014-10-27 10:44:46');
/*!40000 ALTER TABLE `admin_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_editors`
--

DROP TABLE IF EXISTS `admin_editors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_editors` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(50) DEFAULT NULL,
  `editorname` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_editors`
--

LOCK TABLES `admin_editors` WRITE;
/*!40000 ALTER TABLE `admin_editors` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_editors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_elements`
--

DROP TABLE IF EXISTS `admin_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_elements` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `elementcode` blob,
  `elementname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_elements`
--

LOCK TABLES `admin_elements` WRITE;
/*!40000 ALTER TABLE `admin_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_layout_config`
--

DROP TABLE IF EXISTS `admin_layout_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_layout_config` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `numbercols` int(11) DEFAULT NULL,
  `numberofrows` int(11) DEFAULT NULL,
  `colsconfig` varchar(20) DEFAULT NULL,
  `code` longtext,
  `layoutconfigname` varchar(25) NOT NULL,
  `admin_layouts_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`,`admin_layouts_id`),
  KEY `fk_admin_layout_config_admin_layouts1_idx` (`admin_layouts_id`),
  CONSTRAINT `fk_admin_layout_config_admin_layouts1` FOREIGN KEY (`admin_layouts_id`) REFERENCES `admin_layouts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_layout_config`
--

LOCK TABLES `admin_layout_config` WRITE;
/*!40000 ALTER TABLE `admin_layout_config` DISABLE KEYS */;
INSERT INTO `admin_layout_config` VALUES (1,1,1,'12:',NULL,'1col:12',3),(2,2,1,'3,9:',NULL,'2cols:3,9',2),(3,3,1,'3,6,3:',NULL,'3cols:3,6,3',1),(4,2,2,'2,10:6,6',NULL,'2cols2rows:2,10:6,6',4),(5,2,1,'3,9:',NULL,'settingconfig',5);
/*!40000 ALTER TABLE `admin_layout_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_layouts`
--

DROP TABLE IF EXISTS `admin_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_layouts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `layoutname` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_layouts`
--

LOCK TABLES `admin_layouts` WRITE;
/*!40000 ALTER TABLE `admin_layouts` DISABLE KEYS */;
INSERT INTO `admin_layouts` VALUES (1,'3 columns',1,'2014-10-13 12:00:00'),(2,'2 columns - 2cols:3,9',1,'2014-10-26 03:48:03'),(3,'1 column',1,'2014-10-26 03:48:13'),(4,'2cols2rows:2,10:6,6',1,'2014-10-27 04:44:52'),(5,'Settings',1,'2014-10-28 04:25:03');
/*!40000 ALTER TABLE `admin_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_module_config`
--

DROP TABLE IF EXISTS `admin_module_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_module_config` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `modulecode` longtext,
  `filelocation` varchar(200) DEFAULT NULL,
  `admin_modules_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`,`admin_modules_id`),
  KEY `fk_admin_module_config_admin_modules1_idx` (`admin_modules_id`),
  CONSTRAINT `fk_admin_module_config_admin_modules1` FOREIGN KEY (`admin_modules_id`) REFERENCES `admin_modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_module_config`
--

LOCK TABLES `admin_module_config` WRITE;
/*!40000 ALTER TABLE `admin_module_config` DISABLE KEYS */;
INSERT INTO `admin_module_config` VALUES (1,'<div style=\"color:red;\">Hello,\n	{$userdetails.firstname}!</div>\n',NULL,1),(2,'<p>\n	<span style=\"font-size:14px;\"><span style=\"font-family:comic sans ms,cursive;\"><em><strong>Hello there! </strong></em></span></span></p>\n<p>\n	<em><strong>How was your day so far?</strong></em></p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n',NULL,2),(3,'<div class=\"panel panel-default\">\n	<div class=\"panel-body\" style=\"padding: 1px!important;\">\n		{$userdetails.username}</div>\n</div>\n',NULL,3),(4,'<div class=\"panel panel-default\">\n	<div class=\"panel-body\" style=\"padding: 1px!important;\">\n		<a class=\"btn\" data-toggle=\"modal\" href=\"#modal-container-{$modal_id}\" id=\"modal-{$modal_id}\" role=\"button\"><img alt=\"140x140\" class=\"img-circle\" src=\"http://lorempixel.com/210/210/\" width=\"100%\" /> </a>\n		<div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" class=\"modal fade\" id=\"modal-container-{$modal_id}\" role=\"dialog\">\n			<div class=\"modal-dialog\">\n				<div class=\"modal-content\">\n					<div class=\"modal-header\">\n						<button aria-hidden=\"true\" class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>\n						<h4 class=\"modal-title\" id=\"myModalLabel\">\n							{$modal_title}</h4>\n					</div>\n					<div class=\"modal-body\">\n						POST /....</div>\n					<div class=\"modal-footer\">\n						<button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button><button class=\"btn btn-primary\" type=\"button\">Save changes</button></div>\n				</div>\n			</div>\n		</div>\n		<span class=\"label label-default\">{$userdetails.firstname} {$userdetails.lastname} </span><br />\n		<span class=\"label label-default\">{$userdetails.username} </span><br />\n		<span class=\"label label-default\">Last Loggedin: {$userdetails.userlastlogin} </span></div>\n</div>\n<p>\n	&nbsp;</p>\n',NULL,4),(5,'<div class=\"panel panel-default\">\n        <div class=\"panel-body\" style=\"padding: 1px!important;\">\n            <div class=\"list-group\">\n                <a href=\"#\" class=\"list-group-item active\">Friends</a>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n                <div class=\"list-group-item text-center\">\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                    <img alt=\"140x140\" src=\"http://lorempixel.com/42/42/\" class=\"img-circle\" />\n                </div>\n\n                <a class=\"list-group-item active\"><span class=\"badge\">14</span>Help</a>\n            </div>\n        </div>\n    </div>',NULL,5),(6,'<div class=\"panel panel-default\">\n	<div class=\"panel-heading\">\n		<h3 class=\"panel-title\">\n			Smile Newsfeed</h3>\n	</div>\n	<div class=\"panel-body\">\n		<div class=\"media well\">\n			<a class=\"pull-left\" href=\"#\"><img alt=\"\" class=\"media-object\" src=\"http://lorempixel.com/64/64/\" /></a>\n			<div class=\"media-body\">\n				<h4 class=\"media-heading\">\n					Nested media heading</h4>\n				Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.\n				<div class=\"media\">\n					<a class=\"pull-left\" href=\"#\"><img alt=\"\" class=\"media-object\" src=\"http://lorempixel.com/64/64/\" /></a>\n					<div class=\"media-body\">\n						<h4 class=\"media-heading\">\n							Nested media heading</h4>\n						Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class=\"panel-footer\">\n		<ul class=\"pagination\">\n			<li>\n				<a href=\"#\">Prev</a></li>\n			<li>\n				<a href=\"#\">1</a></li>\n			<li>\n				<a href=\"#\">2</a></li>\n			<li>\n				<a href=\"#\">3</a></li>\n			<li>\n				<a href=\"#\">4</a></li>\n			<li>\n				<a href=\"#\">5</a></li>\n			<li>\n				<a href=\"#\">Next</a></li>\n		</ul>\n	</div>\n</div>\n',NULL,6),(7,'<p>\n	hfslahbhbhgabba</p>\n<p>\n	{$demotest}</p>\n<p>\n	&nbsp;</p>\n',NULL,7),(8,'<ul class=\"nav nav-pills nav-stacked\">\n	<li class=\"active\">\n		<a href=\"#\">Home</a></li>\n	<li>\n		<a href=\"#\">Profile</a></li>\n	<li class=\"disabled\">\n		<a href=\"#\">Messages</a></li>\n</ul>\n',NULL,8),(9,'<div class=\"panel panel-primary\">\n	<div class=\"panel-heading\">\n		<h3 class=\"panel-title\">\n			Panel title</h3>\n	</div>\n	<div class=\"panel-body\">\n		Panel content</div>\n	<div class=\"panel-footer\">\n		Panel footer</div>\n</div>\n',NULL,9);
/*!40000 ALTER TABLE `admin_module_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_modules`
--

DROP TABLE IF EXISTS `admin_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_modules` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `modulename` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_modules`
--

LOCK TABLES `admin_modules` WRITE;
/*!40000 ALTER TABLE `admin_modules` DISABLE KEYS */;
INSERT INTO `admin_modules` VALUES (1,'hellomodule',1,'2014-10-23 04:59:59'),(2,'welcomemessage',1,'2014-10-24 01:22:18'),(3,'Panel - Basic',0,'2014-10-27 10:47:45'),(4,'Display Pic - Panel',1,'2014-10-27 11:17:49'),(5,'Friend List Panel Box',1,'2014-10-27 11:31:59'),(6,'Newsfeed Panel',1,'2014-10-27 11:43:43'),(7,'demo mod',1,'2014-10-27 12:37:00'),(8,'SettingMenu',1,'2014-10-28 04:27:30'),(9,'SettingsDetails',1,'2014-10-28 04:34:51');
/*!40000 ALTER TABLE `admin_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_navigation_menu`
--

DROP TABLE IF EXISTS `admin_navigation_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_navigation_menu` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `menuname` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `urlname` varchar(50) DEFAULT NULL,
  `parentid` int(10) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `class` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_navigation_menu`
--

LOCK TABLES `admin_navigation_menu` WRITE;
/*!40000 ALTER TABLE `admin_navigation_menu` DISABLE KEYS */;
INSERT INTO `admin_navigation_menu` VALUES (1,'Dashboard','View Dashboard','dashboard',NULL,1,1,''),(2,'Layouts','Manage Layouts','#',NULL,4,1,'has_sub'),(3,'Manage Layouts','Manage Layouts','manage/managewidget/layouts',2,1,1,''),(4,'Blocks','Manage Blocks','#',NULL,5,1,'has_sub'),(5,'Manage Blocks','Manage Blocks','manage/managewidget/blocks',4,1,1,''),(6,'Config Blocks','Config a block','manage/managewidget/blocksconfig',4,2,1,' '),(7,'Menu','Manage Menu','#',NULL,2,1,'has_sub'),(8,'Manage Menu','Manage Menu Items','manage/managewidget/menu',7,1,1,''),(9,'CSS','Add Styling to Site','#',NULL,11,1,'has_sub'),(10,'Add CSS','Add CSS to site','add/css',9,1,1,' '),(18,'JS','Add JS to site','#',NULL,10,1,'has_sub'),(20,'Add JS','Add content which is JS','add/js',18,1,1,' '),(22,'Manage Layout Config','Manage Layouts Configuration','manage/managewidget/layoutconfig',2,2,1,''),(23,'Widgets','Widgets Manager','#',NULL,8,1,'has_sub'),(24,'Manage Widget','Add/Edit/Delete Widgets','manage/widgets',23,1,1,''),(25,'Widgets Config','Widgets Configuration','manage/widgets_config',23,2,1,''),(27,'Modules','Manage modules','#',NULL,6,1,'has_sub'),(28,'Pages','Add Pages','#',NULL,3,1,'has_sub'),(29,'Manage Modules','Manage Modules','manage/managewidget/modules',27,1,1,''),(30,'Module Config','Configuration for modules','manage/managewidget/modulesconfig',27,2,1,''),(31,'Add Menu Item','Add a menu item to the existing navigation menu.','manage/managewidget/menu/add',7,2,1,''),(32,'Add Blocks','Add Blocks','manage/managewidget/blocks/add',4,2,1,''),(33,'Add Layouts','Add Layouts','manage/managewidget/layouts/add',2,4,1,''),(34,'Add Layout Config','Add Layout Config','manage/managewidget/layoutconfig/add',2,5,1,''),(36,'Add Blocks config','Add Blocks config','manage/managewidget/blocksconfig/add',4,4,1,''),(37,'Add Modules','Add Modules','manage/managewidget/modules/add',27,3,1,''),(38,'Add Modules Config','Add Modules Config','manage/managewidget/modulesconfig/add',27,4,1,''),(39,'Add WIdgets','Add a WIdget','manage/widgets/add',23,3,1,''),(40,'Add Widgets Config','Add Widgets Config','manage/widgets_config/add',23,4,1,''),(41,'Manage Page','Manage Pages','manage/managewidget/pages',28,1,1,''),(42,'Manage Blocks ','Add Blocks to pages','manage/managewidget/pageblocks',28,2,1,''),(43,'Manage Modules','Add Modules to pages.','manage/managewidget/pagemodules',28,3,1,''),(44,'Add Blocks','Add Blocks to pages','manage/managewidget/pageblocks/add',28,4,1,''),(45,'Add Modules','Add Modules to page','manage/managewidget/pagemodules/add',28,5,1,''),(46,'Templates','Add .tpl pages','#',NULL,7,1,'has_sub'),(47,'Manage Header','Add header file','manage/managewidget/headers',46,1,1,''),(48,'Add Headers','Add header files','manage/managewidget/headers/add',46,2,1,''),(49,'Manage Footer','Manage all footers','manage/managewidget/footers',46,3,1,''),(50,'Add Footers','add footers','manage/managewidget/footers/add',46,4,1,''),(51,'Manage Template','Manage template files','manage/managewidget/templates',46,5,1,''),(52,'Add Templates','Add template files','manage/managewidget/templates/add',46,6,1,''),(53,'Variables','Add Variables for pages','#',NULL,6,1,'has_sub'),(54,'Manage Variables','Manage all variables for all pages','manage/managewidget/variables',53,1,1,'');
/*!40000 ALTER TABLE `admin_navigation_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_widget_config`
--

DROP TABLE IF EXISTS `admin_widget_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_widget_config` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `adminwidgetid` int(11) DEFAULT NULL,
  `actiontype` varchar(10) NOT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `columns` varchar(1000) DEFAULT NULL,
  `display_name` varchar(1000) DEFAULT NULL,
  `fields` varchar(1000) DEFAULT NULL,
  `setrelation` varchar(1000) DEFAULT NULL,
  `unsetfields` varchar(1000) DEFAULT NULL,
  `actions` varchar(1000) DEFAULT NULL,
  `afterinsert` varchar(1000) NOT NULL,
  `afterupdate` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `adminwidgetid` (`adminwidgetid`),
  CONSTRAINT `fk_admin_widget_config_admin_widgets1` FOREIGN KEY (`adminwidgetid`) REFERENCES `admin_widgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_widget_config`
--

LOCK TABLES `admin_widget_config` WRITE;
/*!40000 ALTER TABLE `admin_widget_config` DISABLE KEYS */;
INSERT INTO `admin_widget_config` VALUES (1,8,'post','admin_layout_config','id,admin_layouts_id,numbercols,numberofrows,colsconfig,layoutconfigname','Layout Name,No of Columns,No of Rows,Column Config,Layout Config Name','admin_layouts_id,numbercols,numberofrows,colsconfig,code,layoutconfigname','admin_layouts_id,admin_layouts,layoutname','',NULL,'',''),(3,2,'post','admin_layouts','id,layoutname,status,datecreated','Layout Name,Status,Date Created','layoutname,status,datecreated',NULL,'',NULL,'',''),(4,3,'post','admin_blocks','id,blockname,status,datecreated','Block Name, Status, DateCreated','blockname,status,datecreated',NULL,'',NULL,'',''),(5,10,'post','admin_block_config','id,admin_blocks_id,blockcode,blockparent,blockparentid','Block Name,Block Code,Blockparent,Blockparentid','admin_blocks_id,blockcode,blockparent,blockparentid','admin_blocks_id,admin_blocks,blockname','blockclass',NULL,'',''),(6,7,'post','admin_navigation_menu','id,menuname,description,urlname,parentid,position,status,class','Menu Name,Description,URLName,ParentID,Position,Status,Class','menuname,description,urlname,parentid,position,status,class','parentid,admin_navigation_menu,menuname','',NULL,'',''),(7,4,'post','admin_modules','id,modulename,status,datecreated','Module Name,Status,Datecreated','modulename,status,datecreated',NULL,NULL,NULL,'',''),(8,11,'post','admin_module_config','id,admin_modules_id,modulecode,filelocation','Module Name,Module Code, File location','admin_modules_id,modulecode,filelocation','admin_modules_id,admin_modules,modulename',NULL,NULL,'',''),(9,1,'post','UI_Page','id,admin_layouts_id,pagename,pageurl,templateurl,variables,ui_header_id,ui_footer_id,ui_templates_id,status','Layout Name,Page Name,PageURL,TemplateURL,Variables,HeaderName,FooterName,Template Name,Status','admin_layouts_id,pagename,pageurl,templateurl,variables,UI_Header_id,UI_Footer_id,UI_Templates_id,status','admin_layouts_id,admin_layouts,layoutname,UI_Header_id,UI_Header,name,UI_Footer_id,UI_Footer,name,UI_Templates_id,UI_Templates,name',NULL,'Generate Template,,admin/manage/generatetemplate,,null','',''),(10,12,'post','UI_Page_blocks','id,colrowno,position,status,UI_Page_id,admin_blocks_id','Row:Column,Position,Status,Page Name,Block Name','colrowno,position,status,UI_Page_id,admin_blocks_id','UI_Page_id,UI_Page,pagename,admin_blocks_id,admin_blocks,blockname',NULL,NULL,'',''),(11,13,'post','UI_Page_modules','id,UI_Page_id,admin_modules_id,UI_Page_blocks_id,position,status','Page Name,Module Name,PageBlockID,Position,Status','UI_Page_id,admin_modules_id,UI_Page_blocks_id,position,status','UI_Page_id,UI_Page,pagename,admin_modules_id,admin_modules,modulename,UI_Page_blocks_id,UI_Page_blocks,id',NULL,NULL,'',''),(12,14,'post','UI_Header','id,headerurl,name','HeaderURL, Code,Name','headerurl,code,name',NULL,NULL,'Generate Template,,,,generateHeaderFile','generateHeaderFile','generateHeaderFile'),(13,15,'post','UI_Footer','id,footerurl,name','FooterURL,Code,Name','footerurl,code,name',NULL,NULL,'Generate Template,,,,generateFooterFile','generateFooterFile','generateFooterFile'),(14,16,'post','UI_Templates','templateurl,name','TemplateURL,Name','templateurl,name,code',NULL,NULL,'Generate Template,,,,generateTemplateFile','generateTemplateFile','generateTemplateFile'),(15,17,'post','UI_Variables','id,name,function_name','Name, Function Name','name,function_name',NULL,NULL,NULL,'','');
/*!40000 ALTER TABLE `admin_widget_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_widgets`
--

DROP TABLE IF EXISTS `admin_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetName` varchar(50) NOT NULL,
  `widgetVersion` decimal(10,0) NOT NULL,
  `widgetStatus` int(1) NOT NULL,
  `widgetConfigID` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_widgets`
--

LOCK TABLES `admin_widgets` WRITE;
/*!40000 ALTER TABLE `admin_widgets` DISABLE KEYS */;
INSERT INTO `admin_widgets` VALUES (1,'Pages',1,1,1),(2,'Layouts',1,1,2),(3,'Blocks',1,1,3),(4,'Modules',1,1,4),(5,'Elements',1,1,5),(6,'Editor',1,1,6),(7,'Menu',1,1,7),(8,'Layoutconfig',1,1,0),(10,'Blocksconfig',1,1,0),(11,'Modulesconfig',1,1,0),(12,'Pageblocks',1,1,0),(13,'Pagemodules',1,1,0),(14,'Headers',1,1,0),(15,'Footers',1,1,0),(16,'Templates',1,1,0),(17,'Variables',1,1,0);
/*!40000 ALTER TABLE `admin_widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customerNumber` int(11) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(50) NOT NULL,
  `contactLastName` varchar(50) NOT NULL,
  `contactFirstName` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `postalCode` varchar(15) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `salesRepEmployeeNumber` int(11) DEFAULT NULL,
  `creditLimit` double DEFAULT NULL,
  PRIMARY KEY (`customerNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (103,'Atelier graphique','Schmitt','Carine ','40.32.2555','54, rue Royale',NULL,'Nantes',NULL,'44000','France',1370,21000),(112,'Signal Gift Stores','King','Jean','7025551838','8489 Strong St.',NULL,'Las Vegas','NV','83030','USA',1166,71800),(114,'Australian Collectors, Co.','Ferguson','Peter','03 9520 4555','636 St Kilda Road','Level 3','Melbourne','Victoria','3004','Australia',1611,117300),(119,'La Rochelle Gifts','Labrune','Janine ','40.67.8555','67, rue des Cinquante Otages',NULL,'Nantes',NULL,'44000','France',1370,118200),(121,'Baane Mini Imports','Bergulfsen','Jonas ','07-98 9555','Erling Skakkes gate 78',NULL,'Stavern',NULL,'4110','Norway',1504,81700),(124,'Mini Gifts Distributors Ltd.','Nelson','Susan','4155551450','5677 Strong St.',NULL,'San Rafael','CA','97562','USA',1165,210500),(125,'Havel & Zbyszek Co','Piestrzeniewicz','Zbyszek ','(26) 642-7555','ul. Filtrowa 68',NULL,'Warszawa',NULL,'01-012','Poland',NULL,0),(128,'Blauer See Auto, Co.','Keitel','Roland','+49 69 66 90 2555','Lyonerstr. 34',NULL,'Frankfurt',NULL,'60528','Germany',1504,59700),(129,'Mini Wheels Co.','Murphy','Julie','6505555787','5557 North Pendale Street',NULL,'San Francisco','CA','94217','USA',1165,64600),(131,'Land of Toys Inc.','Lee','Kwai','2125557818','897 Long Airport Avenue',NULL,'NYC','NY','10022','USA',1323,114900),(141,'Euro+ Shopping Channel','Freyre','Diego ','(91) 555 94 44','C/ Moralzarzal, 86',NULL,'Madrid',NULL,'28034','Spain',1370,227600),(144,'Volvo Model Replicas, Co','Berglund','Christina ','0921-12 3555','Berguvsvägen  8',NULL,'Luleå',NULL,'S-958 22','Sweden',1504,53100),(145,'Danish Wholesale Imports','Petersen','Jytte ','31 12 3555','Vinbæltet 34',NULL,'Kobenhavn',NULL,'1734','Denmark',1401,83400),(146,'Saveley & Henriot, Co.','Saveley','Mary ','78.32.5555','2, rue du Commerce',NULL,'Lyon',NULL,'69004','France',1337,123900),(148,'Dragon Souveniers, Ltd.','Natividad','Eric','+65 221 7555','Bronz Sok.','Bronz Apt. 3/6 Tesvikiye','Singapore',NULL,'079903','Singapore',1621,103800),(151,'Muscle Machine Inc','Young','Jeff','2125557413','4092 Furth Circle','Suite 400','NYC','NY','10022','USA',1286,138500),(157,'Diecast Classics Inc.','Leong','Kelvin','2155551555','7586 Pompton St.',NULL,'Allentown','PA','70267','USA',1216,100600),(161,'Technics Stores Inc.','Hashimoto','Juri','6505556809','9408 Furth Circle',NULL,'Burlingame','CA','94217','USA',1165,84600),(166,'Handji Gifts& Co','Victorino','Wendy','+65 224 1555','106 Linden Road Sandown','2nd Floor','Singapore',NULL,'069045','Singapore',1612,97900),(167,'Herkku Gifts','Oeztan','Veysel','+47 2267 3215','Brehmen St. 121','PR 334 Sentrum','Bergen',NULL,'N 5804','Norway  ',1504,96800),(168,'American Souvenirs Inc','Franco','Keith','2035557845','149 Spinnaker Dr.','Suite 101','New Haven','CT','97823','USA',1286,0),(169,'Porto Imports Co.','de Castro','Isabel ','(1) 356-5555','Estrada da saúde n. 58',NULL,'Lisboa',NULL,'1756','Portugal',NULL,0),(171,'Daedalus Designs Imports','Rancé','Martine ','20.16.1555','184, chaussée de Tournai',NULL,'Lille',NULL,'59000','France',1370,82900),(172,'La Corne D\'abondance, Co.','Bertrand','Marie','(1) 42.34.2555','265, boulevard Charonne',NULL,'Paris',NULL,'75012','France',1337,84300),(173,'Cambridge Collectables Co.','Tseng','Jerry','6175555555','4658 Baden Av.',NULL,'Cambridge','MA','51247','USA',1188,43400),(175,'Gift Depot Inc.','King','Julie','2035552570','25593 South Bay Ln.',NULL,'Bridgewater','CT','97562','USA',1323,84300),(177,'Osaka Souveniers Co.','Kentary','Mory','+81 06 6342 5555','1-6-20 Dojima',NULL,'Kita-ku','Osaka',' 530-0003','Japan',1621,81200),(181,'Vitachrome Inc.','Frick','Michael','2125551500','2678 Kingston Rd.','Suite 101','NYC','NY','10022','USA',1286,76400),(186,'Toys of Finland, Co.','Karttunen','Matti','90-224 8555','Keskuskatu 45',NULL,'Helsinki',NULL,'21240','Finland',1501,96500),(187,'AV Stores, Co.','Ashworth','Rachel','(171) 555-1555','Fauntleroy Circus',NULL,'Manchester',NULL,'EC2 5NT','UK',1501,136800),(189,'Clover Collections, Co.','Cassidy','Dean','+353 1862 1555','25 Maiden Lane','Floor No. 4','Dublin',NULL,'2','Ireland',1504,69400),(198,'Auto-Moto Classics Inc.','Taylor','Leslie','6175558428','16780 Pompton St.',NULL,'Brickhaven','MA','58339','USA',1216,23000),(201,'UK Collectables, Ltd.','Devon','Elizabeth','(171) 555-2282','12, Berkeley Gardens Blvd',NULL,'Liverpool',NULL,'WX1 6LT','UK',1501,92700),(202,'Canadian Gift Exchange Network','Tamuri','Yoshi ','(604) 555-3392','1900 Oak St.',NULL,'Vancouver','BC','V3F 2K1','Canada',1323,90300),(204,'Online Mini Collectables','Barajas','Miguel','6175557555','7635 Spinnaker Dr.',NULL,'Brickhaven','MA','58339','USA',1188,68700),(205,'Toys4GrownUps.com','Young','Julie','6265557265','78934 Hillside Dr.',NULL,'Pasadena','CA','90003','USA',1166,90700),(206,'Asian Shopping Network, Co','Walker','Brydey','+612 9411 1555','Suntec Tower Three','8 Temasek','Singapore',NULL,'038988','Singapore',NULL,0),(209,'Mini Caravy','Citeaux','Frédérique ','88.60.1555','24, place Kléber',NULL,'Strasbourg',NULL,'67000','France',1370,53800),(211,'King Kong Collectables, Co.','Gao','Mike','+852 2251 1555','Bank of China Tower','1 Garden Road','Central Hong Kong',NULL,NULL,'Hong Kong',1621,58600),(216,'Enaco Distributors','Saavedra','Eduardo ','(93) 203 4555','Rambla de Cataluña, 23',NULL,'Barcelona',NULL,'08022','Spain',1702,60300),(219,'Boards & Toys Co.','Young','Mary','3105552373','4097 Douglas Av.',NULL,'Glendale','CA','92561','USA',1166,11000),(223,'Natürlich Autos','Kloss','Horst ','0372-555188','Taucherstraße 10',NULL,'Cunewalde',NULL,'01307','Germany',NULL,0),(227,'Heintze Collectables','Ibsen','Palle','86 21 3555','Smagsloget 45',NULL,'Århus',NULL,'8200','Denmark',1401,120800),(233,'Québec Home Shopping Network','Fresnière','Jean ','(514) 555-8054','43 rue St. Laurent',NULL,'Montréal','Québec','H1J 1C3','Canada',1286,48700),(237,'ANG Resellers','Camino','Alejandra ','(91) 745 6555','Gran Vía, 1',NULL,'Madrid',NULL,'28001','Spain',NULL,0),(239,'Collectable Mini Designs Co.','Thompson','Valarie','7605558146','361 Furth Circle',NULL,'San Diego','CA','91217','USA',1166,105000),(240,'giftsbymail.co.uk','Bennett','Helen ','(198) 555-8888','Garden House','Crowther Way 23','Cowes','Isle of Wight','PO31 7PJ','UK',1501,93900),(242,'Alpha Cognac','Roulet','Annette ','61.77.6555','1 rue Alsace-Lorraine',NULL,'Toulouse',NULL,'31000','France',1370,61100),(247,'Messner Shopping Network','Messner','Renate ','069-0555984','Magazinweg 7',NULL,'Frankfurt',NULL,'60528','Germany',NULL,0),(249,'Amica Models & Co.','Accorti','Paolo ','011-4988555','Via Monte Bianco 34',NULL,'Torino',NULL,'10100','Italy',1401,113000),(250,'Lyon Souveniers','Da Silva','Daniel','+33 1 46 62 7555','27 rue du Colonel Pierre Avia',NULL,'Paris',NULL,'75508','France',1337,68100),(256,'Auto Associés & Cie.','Tonini','Daniel ','30.59.8555','67, avenue de l\'Europe',NULL,'Versailles',NULL,'78000','France',1370,77900),(259,'Toms Spezialitäten, Ltd','Pfalzheim','Henriette ','0221-5554327','Mehrheimerstr. 369',NULL,'Köln',NULL,'50739','Germany',1504,120400),(260,'Royal Canadian Collectables, Ltd.','Lincoln','Elizabeth ','(604) 555-4555','23 Tsawassen Blvd.',NULL,'Tsawassen','BC','T2F 8M4','Canada',1323,89600),(273,'Franken Gifts, Co','Franken','Peter ','089-0877555','Berliner Platz 43',NULL,'München',NULL,'80805','Germany',NULL,0),(276,'Anna\'s Decorations, Ltd','O\'Hara','Anna','02 9936 8555','201 Miller Street','Level 15','North Sydney','NSW','2060','Australia',1611,107800),(278,'Rovelli Gifts','Rovelli','Giovanni ','035-640555','Via Ludovico il Moro 22',NULL,'Bergamo',NULL,'24100','Italy',1401,119600),(282,'Souveniers And Things Co.','Huxley','Adrian','+61 2 9495 8555','Monitor Money Building','815 Pacific Hwy','Chatswood','NSW','2067','Australia',1611,93300),(286,'Marta\'s Replicas Co.','Hernandez','Marta','6175558555','39323 Spinnaker Dr.',NULL,'Cambridge','MA','51247','USA',1216,123700),(293,'BG&E Collectables','Harrison','Ed','+41 26 425 50 01','Rte des Arsenaux 41 ',NULL,'Fribourg',NULL,'1700','Switzerland',NULL,0),(298,'Vida Sport, Ltd','Holz','Mihael','0897-034555','Grenzacherweg 237',NULL,'Genève',NULL,'1203','Switzerland',1702,141300),(299,'Norway Gifts By Mail, Co.','Klaeboe','Jan','+47 2212 1555','Drammensveien 126A','PB 211 Sentrum','Oslo',NULL,'N 0106','Norway  ',1504,95100),(303,'Schuyler Imports','Schuyler','Bradley','+31 20 491 9555','Kingsfordweg 151',NULL,'Amsterdam',NULL,'1043 GR','Netherlands',NULL,0),(307,'Der Hund Imports','Andersen','Mel','030-0074555','Obere Str. 57',NULL,'Berlin',NULL,'12209','Germany',NULL,0),(311,'Oulu Toy Supplies, Inc.','Koskitalo','Pirkko','981-443655','Torikatu 38',NULL,'Oulu',NULL,'90110','Finland',1501,90500),(314,'Petit Auto','Dewey','Catherine ','(02) 5554 67','Rue Joseph-Bens 532',NULL,'Bruxelles',NULL,'B-1180','Belgium',1401,79900),(319,'Mini Classics','Frick','Steve','9145554562','3758 North Pendale Street',NULL,'White Plains','NY','24067','USA',1323,102700),(320,'Mini Creations Ltd.','Huang','Wing','5085559555','4575 Hillside Dr.',NULL,'New Bedford','MA','50553','USA',1188,94500),(321,'Corporate Gift Ideas Co.','Brown','Julie','6505551386','7734 Strong St.',NULL,'San Francisco','CA','94217','USA',1165,105000),(323,'Down Under Souveniers, Inc','Graham','Mike','+64 9 312 5555','162-164 Grafton Road','Level 2','Auckland  ',NULL,NULL,'New Zealand',1612,88000),(324,'Stylish Desk Decors, Co.','Brown','Ann ','(171) 555-0297','35 King George',NULL,'London',NULL,'WX3 6FW','UK',1501,77000),(328,'Tekni Collectables Inc.','Brown','William','2015559350','7476 Moss Rd.',NULL,'Newark','NJ','94019','USA',1323,43000),(333,'Australian Gift Network, Co','Calaghan','Ben','61-7-3844-6555','31 Duncan St. West End',NULL,'South Brisbane','Queensland','4101','Australia',1611,51600),(334,'Suominen Souveniers','Suominen','Kalle','+358 9 8045 555','Software Engineering Center','SEC Oy','Espoo',NULL,'FIN-02271','Finland',1501,98800),(335,'Cramer Spezialitäten, Ltd','Cramer','Philip ','0555-09555','Maubelstr. 90',NULL,'Brandenburg',NULL,'14776','Germany',NULL,0),(339,'Classic Gift Ideas, Inc','Cervantes','Francisca','2155554695','782 First Street',NULL,'Philadelphia','PA','71270','USA',1188,81100),(344,'CAF Imports','Fernandez','Jesus','+34 913 728 555','Merchants House','27-30 Merchant\'s Quay','Madrid',NULL,'28023','Spain',1702,59600),(347,'Men \'R\' US Retailers, Ltd.','Chandler','Brian','2155554369','6047 Douglas Av.',NULL,'Los Angeles','CA','91003','USA',1166,57700),(348,'Asian Treasures, Inc.','McKenna','Patricia ','2967 555','8 Johnstown Road',NULL,'Cork','Co. Cork',NULL,'Ireland',NULL,0),(350,'Marseille Mini Autos','Lebihan','Laurence ','91.24.4555','12, rue des Bouchers',NULL,'Marseille',NULL,'13008','France',1337,65000),(353,'Reims Collectables','Henriot','Paul ','26.47.1555','59 rue de l\'Abbaye',NULL,'Reims',NULL,'51100','France',1337,81100),(356,'SAR Distributors, Co','Kuger','Armand','+27 21 550 3555','1250 Pretorius Street',NULL,'Hatfield','Pretoria','0028','South Africa',NULL,0),(357,'GiftsForHim.com','MacKinlay','Wales','64-9-3763555','199 Great North Road',NULL,'Auckland',NULL,NULL,'New Zealand',1612,77700),(361,'Kommission Auto','Josephs','Karin','0251-555259','Luisenstr. 48',NULL,'Münster',NULL,'44087','Germany',NULL,0),(362,'Gifts4AllAges.com','Yoshido','Juri','6175559555','8616 Spinnaker Dr.',NULL,'Boston','MA','51003','USA',1216,41900),(363,'Online Diecast Creations Co.','Young','Dorothy','6035558647','2304 Long Airport Avenue',NULL,'Nashua','NH','62005','USA',1216,114200),(369,'Lisboa Souveniers, Inc','Rodriguez','Lino ','(1) 354-2555','Jardim das rosas n. 32',NULL,'Lisboa',NULL,'1675','Portugal',NULL,0),(376,'Precious Collectables','Urs','Braun','0452-076555','Hauptstr. 29',NULL,'Bern',NULL,'3012','Switzerland',1702,0),(379,'Collectables For Less Inc.','Nelson','Allen','6175558555','7825 Douglas Av.',NULL,'Brickhaven','MA','58339','USA',1188,70700),(381,'Royale Belge','Cartrain','Pascale ','(071) 23 67 2555','Boulevard Tirou, 255',NULL,'Charleroi',NULL,'B-6000','Belgium',1401,23500),(382,'Salzburg Collectables','Pipps','Georg ','6562-9555','Geislweg 14',NULL,'Salzburg',NULL,'5020','Austria',1401,71700),(385,'Cruz & Sons Co.','Cruz','Arnold','+63 2 555 3587','15 McCallum Street','NatWest Center #13-03','Makati City',NULL,'1227 MM','Philippines',1621,81500),(386,'L\'ordine Souveniers','Moroni','Maurizio ','0522-556555','Strada Provinciale 124',NULL,'Reggio Emilia',NULL,'42100','Italy',1401,121400),(398,'Tokyo Collectables, Ltd','Shimamura','Akiko','+81 3 3584 0555','2-2-8 Roppongi',NULL,'Minato-ku','Tokyo','106-0032','Japan',1621,94400),(406,'Auto Canal+ Petit','Perrier','Dominique','(1) 47.55.6555','25, rue Lauriston',NULL,'Paris',NULL,'75016','France',1337,95000),(409,'Stuttgart Collectable Exchange','Müller','Rita ','0711-555361','Adenauerallee 900',NULL,'Stuttgart',NULL,'70563','Germany',NULL,0),(412,'Extreme Desk Decorations, Ltd','McRoy','Sarah','04 499 9555','101 Lambton Quay','Level 11','Wellington',NULL,NULL,'New Zealand',1612,86800),(415,'Bavarian Collectables Imports, Co.','Donnermeyer','Michael',' +49 89 61 08 9555','Hansastr. 15',NULL,'Munich',NULL,'80686','Germany',1504,77000),(424,'Classic Legends Inc.','Hernandez','Maria','2125558493','5905 Pompton St.','Suite 750','NYC','NY','10022','USA',1286,67500),(443,'Feuer Online Stores, Inc','Feuer','Alexander ','0342-555176','Heerstr. 22',NULL,'Leipzig',NULL,'04179','Germany',NULL,0),(447,'Gift Ideas Corp.','Lewis','Dan','2035554407','2440 Pompton St.',NULL,'Glendale','CT','97561','USA',1323,49700),(448,'Scandinavian Gift Ideas','Larsson','Martha','0695-34 6555','Åkergatan 24',NULL,'Bräcke',NULL,'S-844 67','Sweden',1504,116400),(450,'The Sharp Gifts Warehouse','Frick','Sue','4085553659','3086 Ingle Ln.',NULL,'San Jose','CA','94217','USA',1165,77600),(452,'Mini Auto Werke','Mendel','Roland ','7675-3555','Kirchgasse 6',NULL,'Graz',NULL,'8010','Austria',1401,45300),(455,'Super Scale Inc.','Murphy','Leslie','2035559545','567 North Pendale Street',NULL,'New Haven','CT','97823','USA',1286,95400),(456,'Microscale Inc.','Choi','Yu','2125551957','5290 North Pendale Street','Suite 200','NYC','NY','10022','USA',1286,39800),(458,'Corrida Auto Replicas, Ltd','Sommer','Martín ','(91) 555 22 82','C/ Araquil, 67',NULL,'Madrid',NULL,'28023','Spain',1702,104600),(459,'Warburg Exchange','Ottlieb','Sven ','0241-039123','Walserweg 21',NULL,'Aachen',NULL,'52066','Germany',NULL,0),(462,'FunGiftIdeas.com','Benitez','Violeta','5085552555','1785 First Street',NULL,'New Bedford','MA','50553','USA',1216,85800),(465,'Anton Designs, Ltd.','Anton','Carmen','+34 913 728555','c/ Gobelas, 19-1 Urb. La Florida',NULL,'Madrid',NULL,'28023','Spain',NULL,0),(471,'Australian Collectables, Ltd','Clenahan','Sean','61-9-3844-6555','7 Allen Street',NULL,'Glen Waverly','Victoria','3150','Australia',1611,60300),(473,'Frau da Collezione','Ricotti','Franco','+39 022515555','20093 Cologno Monzese','Alessandro Volta 16','Milan',NULL,NULL,'Italy',1401,34800),(475,'West Coast Collectables Co.','Thompson','Steve','3105553722','3675 Furth Circle',NULL,'Burbank','CA','94019','USA',1166,55400),(477,'Mit Vergnügen & Co.','Moos','Hanna ','0621-08555','Forsterstr. 57',NULL,'Mannheim',NULL,'68306','Germany',NULL,0),(480,'Kremlin Collectables, Co.','Semenov','Alexander ','+7 812 293 0521','2 Pobedy Square',NULL,'Saint Petersburg',NULL,'196143','Russia',NULL,0),(481,'Raanan Stores, Inc','Altagar,G M','Raanan','+ 972 9 959 8555','3 Hagalim Blv.',NULL,'Herzlia',NULL,'47625','Israel',NULL,0),(484,'Iberia Gift Imports, Corp.','Roel','José Pedro ','(95) 555 82 82','C/ Romero, 33',NULL,'Sevilla',NULL,'41101','Spain',1702,65700),(486,'Motor Mint Distributors Inc.','Salazar','Rosa','2155559857','11328 Douglas Av.',NULL,'Philadelphia','PA','71270','USA',1323,72600),(487,'Signal Collectibles Ltd.','Taylor','Sue','4155554312','2793 Furth Circle',NULL,'Brisbane','CA','94217','USA',1165,60300),(489,'Double Decker Gift Stores, Ltd','Smith','Thomas ','(171) 555-7555','120 Hanover Sq.',NULL,'London',NULL,'WA1 1DP','UK',1501,43300),(495,'Diecast Collectables','Franco','Valarie','6175552555','6251 Ingle Ln.',NULL,'Boston','MA','51003','USA',1188,85100),(496,'Kelly\'s Gift Shop','Snowden','Tony','+64 9 5555500','Arenales 1938 3\'A\'',NULL,'Auckland  ',NULL,NULL,'New Zealand',1612,110000);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'members','General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,NULL,NULL,1268889823,1268889823,1,'Admin','istrator','ADMIN','0'),(2,'71.168.175.53','sachin_rajput','$2y$08$0SO2eoaOL/140tY1krS4Qeaa3Xc4v7DPuiDxJ6J6/45HlLoCO90/2',NULL,'sachin.rajput@hotmail.com',NULL,NULL,NULL,'A3.Vrd292pZRHrgvf4Brj.',1410390545,1415199511,1,'Sachin','Rajput',NULL,'--'),(3,'209.23.223.4','dinesh\"><abc>_shetty\"><abc>','$2y$08$MgHCpyI36YHVcG.44cFkWe/g44IpYrHOW8iTTdCp0M5Wri3PhoyUa',NULL,'dnstest1@yopmail.com',NULL,NULL,NULL,NULL,1410453583,1410537801,1,'Dinesh\"><abc>','Shetty\"><abc>',NULL,'--'),(4,'205.203.128.224','demofn_demoln','$2y$08$nNorVBdAANmdUp0YmvzuDuiIxku8hYsVNYFWwXJRXKUa6fhA/rU72',NULL,'demo@email.com',NULL,NULL,NULL,'MZhPAuvoxZsA36dv67eS0u',1414164974,1415057274,1,'DemoFN','DemoLN',NULL,'--');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(2,1,2),(3,2,1),(5,2,2),(4,3,2),(6,4,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `ADMIN_Menu`
--

/*!50001 DROP VIEW IF EXISTS `ADMIN_Menu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`samperk1`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ADMIN_Menu` AS select `admin_navigation_menu`.`id` AS `id`,`admin_navigation_menu`.`menuname` AS `menuname`,`admin_navigation_menu`.`description` AS `description`,`admin_navigation_menu`.`urlname` AS `urlname`,`admin_navigation_menu`.`parentid` AS `parentid`,`admin_navigation_menu`.`position` AS `position`,`admin_navigation_menu`.`status` AS `status`,`admin_navigation_menu`.`class` AS `class` from `admin_navigation_menu` where (`admin_navigation_menu`.`status` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ADMIN_Menu_Parent`
--

/*!50001 DROP VIEW IF EXISTS `ADMIN_Menu_Parent`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`samperk1_smileis`@`71.168.175.53` SQL SECURITY DEFINER */
/*!50001 VIEW `ADMIN_Menu_Parent` AS select `admin_navigation_menu`.`id` AS `id`,`admin_navigation_menu`.`menuname` AS `menuname`,`admin_navigation_menu`.`description` AS `description`,`admin_navigation_menu`.`urlname` AS `urlname`,`admin_navigation_menu`.`parentid` AS `parentid`,`admin_navigation_menu`.`position` AS `position`,`admin_navigation_menu`.`status` AS `status`,`admin_navigation_menu`.`class` AS `class` from `admin_navigation_menu` where isnull(`admin_navigation_menu`.`parentid`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ADMIN_Widget_View`
--

/*!50001 DROP VIEW IF EXISTS `ADMIN_Widget_View`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`samperk1_smileis`@`71.168.175.53` SQL SECURITY DEFINER */
/*!50001 VIEW `ADMIN_Widget_View` AS select `w`.`id` AS `id`,`w`.`widgetName` AS `widgetName`,`w`.`widgetStatus` AS `widgetStatus`,`w`.`widgetVersion` AS `widgetVersion`,`c`.`actiontype` AS `actiontype`,`c`.`columns` AS `columns`,`c`.`display_name` AS `display_name`,`c`.`fields` AS `fields`,`c`.`setrelation` AS `setrelation`,`c`.`table_name` AS `table_name`,`c`.`unsetfields` AS `unsetfields`,`c`.`actions` AS `actions`,`c`.`afterinsert` AS `afterinsert`,`c`.`afterupdate` AS `afterupdate` from (`admin_widget_config` `c` join `admin_widgets` `w` on((`c`.`adminwidgetid` = `w`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-05 10:32:26
