{if $auth == false}
    {include file=$FUIheader_login_tag}
    {include file=$main_body}
    {include file=$FUIfooter_login_tag}
{else}
    {include file=$FUIheader_tag}
    {include file=$FUInav_menu}
    {include file=$main_body}
    {include file=$FUIfooter_tag}
{/if}

