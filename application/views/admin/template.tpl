{if $auth == 0}
    {include file=$header_login_tag}
    {include file=$main_body}
    {include file=$footer_login_tag}
{else}
    {include file=$header_tag}
    {include file=$nav_menu}
    {include file=$main_body}
    {include file=$footer_tag}
{/if}

