<!-- Main bar -->
<div class="mainbar">

<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="fa fa-home"></i> Dashboard</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="index.html"><i class="fa fa-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="#" class="bread-current">Dashboard</a>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
<div class="container">

<!-- Today status. jQuery Sparkline plugin used. -->

<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-head">
                <div class="pull-left">Run SQL</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-content">
                <div class="padd">

                    <div class="form quick-post">
                        <!-- Edit profile form (not working)-->
                        <form class="form-horizontal" method="post" action="{$base_url}admin/sqlrun">
                            <!-- table name -->
                            <div class="form-group">
                                <label class="control-label col-lg-2">Type</label>
                                <div class="col-lg-5">
                                    <select name="type" class="form-control">
                                        <option value="">- Choose Type of Query -</option>
                                        <option value="1">SELECT</option>
                                        <option value="2">UPDATE</option>
                                        <option value="3">DELETE</option>
                                        <option value="4">INSERT</option>
                                        <option value="5">MULTIPLE/MIX</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="content">Content</label>
                                <div class="col-lg-8">
                                    <textarea name="query" class="form-control" rows="5" id="query"></textarea>
                                </div>
                            </div>

                            <!-- Tags -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="tags">Tags</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="tags">
                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="form-group">
                                <!-- Buttons -->
                                <div class="col-lg-offset-2 col-lg-6">
                                    <button name="submit" type="submit" class="btn btn-sm btn-success">Run</button>
                                    <!--<button type="submit" class="btn btn-sm btn-danger">Save Draft</button>
                                    <button type="reset" class="btn btn-sm btn-default">Reset</button>-->
                                </div>
                            </div>
                        </form>
                    </div>


                </div>
                <div class="widget-foot">
                    <!-- Footer goes here -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Today status ends -->


</div>
</div>

<!-- Matter ends -->

</div>

<!-- Mainbar ends -->
<div class="clearfix"></div>

</div>
<!-- Content ends -->

