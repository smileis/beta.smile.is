<!-- Sidebar -->
<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <!--- Sidebar navigation -->
    <ul id="nav">
        {navmenu}
        <li {class}>
        <a href="{url}">
            <i class="fa fa-list-alt"></i> {name} <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
        </a>
        {submenu}
        <ul>
            <li>
                <a href="{sub_url}">
                    {sub_name}
                </a>
            </li>
        </ul>
        {/submenu}
        </li>
        {/navmenu}
    </ul>
</div>