<!-- Sidebar -->
<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <!--- Sidebar navigation -->
    <ul id="nav">
        {foreach $navmenus navmenu}
            <li class="{if $topmenuname == lower($navmenu.name)} open {/if} {$navmenu.class}" id="{lower($navmenu.name)}">
                <a href="{$navmenu.url}">
                    <i class="fa fa-list-alt"></i> {$navmenu.name} <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
                </a>
                {foreach $navmenu.submenu submenus}
                    <ul>
                        <li>
                            <a href="{$submenus.sub_url}">
                                {$submenus.sub_name}
                            </a>
                        </li>
                    </ul>
                {/foreach}
            </li>
        {/foreach}
    </ul>
</div>