<!-- Main bar -->
  	<div class="mainbar">
      
	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"><i class="fa fa-file-o"></i> Add Pages</h2>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="<?=base_url('admin_home');?>"><i class="fa fa-home"></i> Pages</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="<?=base_url('addpage');?>" class="bread-current">Add Pages</a>
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->



	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">


              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Add Page</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <br />
                    <!-- Form starts.  -->
                     <form class="form-horizontal" role="form">
                              
                              	<?php 
                              	
                              		$this->formio_model->generateInputTextBox('PageName','Please enter the page name','pagename');
                                
                                	$valarray = array(1,0);
                                	$textarray = array('Yes','No');
                                	
                                	$this->formio_model->generateRadioBox('Directory',$valarray,$textarray,'directoryOption');
                                	
                                	$this->formio_model->generateInputTextBox('Directory Location','Please enter the directory location','directoryloc');
                                	 
                                	$valarray = array(1,2,3);
                                	$textarray = array('Layout 1x3','Layout 1x2','Layout 2x3');
                                	
                                	$this->formio_model->generateRadioBox('Layout',$valarray,$textarray,'layoutOption');
                                	
                                	$this->formio_model->generateInputTextBox('No of Blocks','Please enter no of blocks','noofblocks');
                                	
                                	$this->formio_model->generateInputTextBox('Alias','Please enter alias','aliasname');
                                	
                                	$valarray = array(1,2,3);
                                	$textarray = array('Layout 1x3','Layout 1x2','Layout 2x3');
                                	
                                	//$this->formio_model->generateSelectBox('Layout',$valarray,$textarray,'layoutOption');
                                	
                                	//$this->formio_model->generateCheckBox('Layout',$valarray,$textarray,'layoutOption');
                                	
                                	$this->formio_model->generateButton('success','Save');
                                ?>
                                
                                
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
              </div>  

            </div>

          </div>

        </div>
		  </div>

		<!-- Matter ends -->

    </div>

   <!-- Mainbar ends -->	    	
   <div class="clearfix"></div>

</div>
<!-- Content ends -->