<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">

            <form class="form-horizontal" action="{$base_url}pins/save/999" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Pin it to your Home Page</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EventID">EventID</label>
                        <div class="col-md-4">
                            <input id="EventID" name="EventID" type="text" placeholder="" disabled value="{$pinsSaveandloadObj.eventid}" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="ExecutiveID">ExecutiveID</label>
                        <div class="col-md-4">
                            <input id="ExecutiveID" name="ExecutiveID" type="text" placeholder="" disabled value="{$pinsSaveandloadObj.execid}" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="category">Category</label>
                        <div class="col-md-4">
                            <select id="category" name="category" class="form-control">
                            </select>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="savePin"></label>
                        <div class="col-md-8">
                            <button id="savePin" name="savePin" class="btn btn-success">Save</button>
                            <button id="cancel" name="cancel" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>

                </fieldset>
            </form>

            <form class="form-horizontal" action="{$base_url}register_user1" method="post">
                <fieldset>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button id="createCategory" name="createCategory" class="btn btn-primary">Create Category</button>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>
</div>