<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			
			<div class="admin-form">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- Widget starts -->
							<div class="widget wred">
								<div class="widget-head">
									<i class="fa fa-lock"></i> Register
								</div>
								<div class="widget-content">
									<div class="padd">
										<form class="form-horizontal" action="{$base_url}register_user" method="post" id="register_form">

											<!-- <form class="form-horizontal"> -->
											<!-- Registration form starts -->
											<!-- Error -->
											{if $message}
												<div class="alert alert-warning">
													{$message}
												</div>
											{/if}
											<!-- F Name -->
											<div class="form-group">
												<label class="control-label col-lg-3" for="first_name">First Name</label>
												<div class="col-lg-9">
													<input type="text" class="form-control" id="first_name" name="first_name">
												</div>
											</div>
											<!-- L Name -->
											<div class="form-group">
												<label class="control-label col-lg-3" for="last_name">Last Name</label>
												<div class="col-lg-9">
													<input type="text" class="form-control" id="last_name" name="last_name">
												</div>
											</div>
											<!-- Email -->
											<div class="form-group">
												<label class="control-label col-lg-3" for="email">Email</label>
												<div class="col-lg-9">
													<input type="text" class="form-control" id="email" name="email">
												</div>
											</div>
											<!-- Select box -->
											<!-- <div class="form-group">
											  <label class="control-label col-lg-3">Drop Down</label>
											  <div class="col-lg-4">
												  <select class="form-control">
												  <option>&nbsp;</option>
												  <option>1</option>
												  <option>2</option>
												  <option>3</option>
												  <option>4</option>
												  <option>5</option>
												  </select>
											  </div>
											</div>      -->
											<!-- Username -->
											<!-- <div class="form-group">
											  <label class="control-label col-lg-3" for="username">Username</label>
											  <div class="col-lg-9">
												<input type="text" class="form-control" id="username">
											  </div>
											</div> -->
											<!-- Password -->
											<div class="form-group">
												<label class="control-label col-lg-3" for="password">Password</label>
												<div class="col-lg-9">
													<input type="password" class="form-control" id="password" name="password">
												</div>
											</div>
											<!--Confirm Password -->
											<div class="form-group">
												<label class="control-label col-lg-3" for="confirm-password">Confirm Password</label>
												<div class="col-lg-9">
													<input type="password" class="form-control" id="confirm-password" name="password_confirm">
												</div>
											</div>
											<!-- Accept box and button s-->
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<div class="checkbox">
														<label>
															<input id="terms" type="checkbox"> Accept Terms &amp; Conditions
														</label>
													</div>
													<br />
													<button type="submit" class="btn btn-sm btn-info" id="register">Register</button>
													<button type="reset" class="btn btn-sm btn-default">Reset</button>
												</div>
											</div>
										</form>

									</div>
								</div>
								<div class="widget-foot">
									Already Registered? <a href="<?=base_url('login');?>">Login</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>