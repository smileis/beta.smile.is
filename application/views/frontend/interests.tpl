<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			
			<div class="admin-form">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- Widget starts -->
							<div class="widget wred">
								<div class="widget-head">
									
								</div>
								<div class="widget-content">
									<div class="padd">
										<form class="form-horizontal" action="" method="post" id="interest_search">

											<!-- Registration form starts -->
											<!-- Error -->
											{if $message}
												<div class="alert alert-warning">
													{$message}
												</div>
											{/if}
											<!-- F Name -->
											<div class="panel panel-primary">
												<div class="panel-heading"><i class="fa fa-heart-o fa-2x"></i> Pick at least 3 Industry</div>
												<div class="panel-body">
													<div class="col-lg-10">
													<input type="text" class="form-control" id="search_content" name="params">
													<input type="hidden" name="servicename" value="suggest_industry" />
													<input type="hidden" name="custom" value="false" />
													
													</div>
													<div class="col-lg-2">
														<button type="submit" class="btn btn-sm btn-info" id="search_interest" onclick="load_data_ajax_class('djapi','suggestIndustry','interest_result','interest_search')" width="100%">Search</button>
													</div>
												</div>
											</div>
											
										</form>
										<div class="panel panel-primary" id="interest_result">
											
										</div>
										<div class="panel panel-primary" id="interest_selected">
											<div class="panel-heading">Selected Industries</div>
											<div class="panel-body" id="selected_industry">
											<form class="form-horizontal" action="" method="post" id="industry_submit">
												<div class="col-lg-10">
													<select name="sel_ind" id="sel_ind" multiple size="5">
														
													</select>
												</div>
												<div class="col-lg-2">
													<button type="submit" class="btn btn-sm btn-info" id="submit_industry"  width="100%">Next</button>
												</div>
											</form>
											</div>
										</div>
										
									</div>
								</div>
								<div class="widget-foot">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>