<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class="">{$execdetails.Executive->FirstName} {$execdetails.Executive->LastName}</h3>
                        <hr/>
                        <h4>{$execdetails.Executive->DisplayName}</h4>
                    </div>
                    <div class="panel-body">
                        {if isset($execprofimg)}
                            <img src="{$execprofimg}" width="260px" height="260px"
                                 class="img-responsive">
                        {else}
                            <img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
                                 class="img-responsive">
                        {/if}



                        <br/><br/>
                        <dl class=""> <dt class="">Job Title:</dt>
                            <dd class="">{$execdetails.Executive->JobTitle}</dd>
                            <!--<dd class="">11 March 1931 (age 83) Sydney, Australia</dd> -->
                            <dt class="">Biography:</dt>

                            <dd class="" style="text-align: justify">{$execdetails.Biography->Paragraph}</dd>

                        </dl>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="">Keywords</h4>
                        </div>
                        <div class="panel-body">
                            {$counter = 0 }
                            {assign var=class value='info'}
                            {foreach from=$keywords item=keyword}
                                {$counter = $counter+1}
                                {if $counter==1}
                                    {assign var=class value='info'}
                                {elseif $counter==2}
                                    {assign var=class value='danger'}
                                {elseif $counter==3}
                                    {assign var=class value='warning'}
                                {elseif $counter==4}
                                    {assign var=class value='default'}
                                    {$counter = 0}
                                {/if}
                                <a href="#"><span class="badge alert-{$class}">{$keyword}</span></a>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div id="timeline-embed"></div>
                <script type="text/javascript">
                    var timeline_config = {
                        width: "100%",
                        height: "500",
                        source: {$timelinejSONObj}
                    }
                </script>

            <!-- {$base_url}assets/frontend/css/example.json -->


                <!--<iframe src="http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=1T8NgS-TVfDS-VaxSAQrwyuHIEmah5qVJ-C2I70mYzXs&font=DroidSerif-DroidSans&maptype=toner&lang=en&height=450"
                        width="100%" height="450" frameborder="0" class=""></iframe>-->
            </div>
            <div class="row well">
                <div class="tabbable">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="#tab1" data-toggle="tab">Colleagues</a></li>
                        <li><a href="#tab2" data-toggle="tab">Inspiring Statements</a></li>
                        <li><a href="#tab3" data-toggle="tab">Questions Poll</a></li>
                    </ul>
                    <div class="tab-content" style="margin: 10px;">
                        <div class="tab-pane active" id="tab1">
                            <div class="container">
                                {foreach from=$colleagues item=contact}
                                    <div class="row">
                                            <a href="{$base_url}{$contact->FirstName}_{$contact->LastName}">{$contact->FullName}</a>,
                                            <b><i>{$contact->JobTitle}</i></b>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="container">
								<dl>
                                    <dt>Followers Name</dt>
                                    <dd>Inspiring statement 1.......</dd>
                                    <dt>Followers Name</dt>
                                    <dd>Inspiring statement 2.......</dd>
									<dt>Followers Name</dt>
                                    <dd>Inspiring statement 3.......</dd>
									<dt>Followers Name</dt>
                                    <dd>Inspiring statement 4.......</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="container">
                                <dl>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center" style="background-color:black;color:white">
        <h2 style="color:white">
            SAINT LAURENT
        </h2>
        <p>SAC DE JOUR</p>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <iframe width="490" height="315" src="{$youtube_videos[0]['link']}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-3" style="height: 315px;overflow: scroll;">
                    <div class="row">
                        <iframe width="200" height="150" src="{$youtube_videos[1]['link']}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="{$youtube_videos[2]['link']}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="{$youtube_videos[3]['link']}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="{$youtube_videos[4]['link']}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row well" style="">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab11" data-toggle="tab">Tweets</a></li>
                        <li><a href="#tab22" data-toggle="tab">Tweets & Photos</a></li>
                        <li><a href="#tab33" data-toggle="tab">Retweets</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab11">
                            <div class="row">
                                {if isset($twitter_timeline)}
                                    {foreach from=$twitter_timeline item=status}

                                            <b><i>{$status->date|date_format} </i></b>
                                            {$status->text}<br/>
                                            @<b>{$twitter_screenName}</b><br/>

                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab22">
                            <div class="row">
                                {if isset($twitter_timeline)}
                                    {foreach from=$twitter_timeline item=status}

                                            <b><i>{$status->date} </i></b>
                                            {$status->text}

                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab33">
                            <div class="row">
                                {if isset($twitter_screenName)}
                                    <a class="twitter-timeline" href="https://twitter.com/{$twitter_screenName}" data-widget-id="582556794754580480">Tweets by @rupertmurdoch</a>
                                {literal}
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                {/literal}
                                {else}
                                    Twitter User Details Not Found!
                                {/if}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>