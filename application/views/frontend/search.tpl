<div class="container">
	<div class="row clearfix">
		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-body">
					{if isset($profimg)}
						<img src="{$execprofimg}" width="160px" height="160px"
							 class="img-responsive">
					{else}
						<img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 class="img-responsive">
					{/if}
					<b>{$userdetails.firstname}</b>
					<div class="list-group">
						<a href="#" class="list-group-item active">
							All
						</a>
						{foreach from=$execsObj item=execdetails}
							<a href="{strip}{$execdetails.Executive->FirstName}_{$execdetails.Executive->LastName}{/strip}" class="list-group-item">{$execdetails.Executive->DisplayName}</a>
						{/foreach}
					</div>
					<button class="btn btn-xs btn-primary" type="button">Messages</button>
						<button class="btn btn-xs btn-success" type="button">Messages</button>
						<button class="btn btn-xs btn-info" type="button">Messages</button>
						<button class="btn btn-xs btn-warning" type="button">Messages</button>
				
				</div>
			</div>
        </div>
		<div class="col-md-9 column">
			{if $searchCheck}
				<b>Keyword:</b> {$searchKeyword} 
				<div class="row" id="result_list_data">
					<div class="panel-header">
						<button type="submit" class="btn btn-sm btn-info" id="update_execs"  width="100%">Save</button>
					</div>
					{foreach from=$resultsExecs item=exec}
						<div class="col-sm-5 col-md-3">
							<div class="thumbnail" id="{$exec->Executive->FCode}_box">
							  <img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
											 class="img-responsive">
							  <div class="caption">
								<a href="{$base_url}{$exec->Executive->FirstName}_{$exec->Executive->LastName}" ><h3>{$exec->Executive->DisplayName}</h3></a>
								<p>
								Company Name: <span class="label label-info">{$exec->Executive->CompanyName}</span><br/>Job Title: <span class="label label-primary">{$exec->Executive->JobTitle}</span><br/> Industry: <span class="label label-success">{$exec->Executive->Industry}</span></p>
								<p><a href="javascript:" id="{$exec->Executive->FCode}" class="addExec btn btn-primary" role="button">Add</a>
								<a href="javascript:" id="{$exec->Executive->FCode}" class="removeExec btn btn-primary" role="button">Remove</a></p>
							  </div>
							</div>
						  </div>
					{/foreach}
				</div>
				<div class="panel panel-primary" id="execs_selected" style="display:none">
					<div class="panel-heading">Selected Execs</div>
					<div class="panel-body" id="">
						<select name="sel_exec" id="sel_exec" multiple size="5">
								
						</select>
					</div>
				</div>
			{else}
				search form
				{$searchKeyword}
			{/if}
		</div>
	</div>
</div>