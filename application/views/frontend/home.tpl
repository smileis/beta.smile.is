<div class="container">
    <div class="row">
		<div class="col-md-2 sidebar-outer" >
			<div class="panel panel-default sidebar">
				<div class="panel-body">
					
					{if isset($profimg)}
						<img src="{$execprofimg}" width="160px" height="160px"
							 >
					{else}
						<img width="160px" height="160px" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 >
					{/if}<br/>
					<b>{$userdetails.firstname}</b>
					<ul class="list-group">
                        <li class="list-group-item">
                            <a href="home" >
                                All
                            </a>
                        </li>

						{foreach from=$execsObj item=execdetails}
                            <li class="list-group-item">
                                <a target="_blank" id="execart" href="home/single/{strip}{$execdetails.Executive->FCode}{/strip}">
                                    {$execdetails.Executive->DisplayName}
                                </a>
                                <a target="_blank" href="{strip}{$execdetails.Executive->FirstName}_{$execdetails.Executive->LastName}{/strip}" style="float:right;">
                                    <i class="fa fa-share"></i>
                                </a>

                            </li>
						{/foreach}
					</ul>
					<!--<button class="btn btn-xs btn-primary" type="button">Messages</button>
						<button class="btn btn-xs btn-success" type="button">Messages</button>
						<button class="btn btn-xs btn-info" type="button">Messages</button>
						<button class="btn btn-xs btn-warning" type="button">Messages</button>-->
				
				</div>
			</div>
        </div>
        <div class="col-md-10" style="position:relative;">
			{$counter = 0 }
            {assign var=class value='info'}
			{foreach from=$articlesObj item=articles}
			{$counter = $counter+1}
			{if $counter==1}
				{assign var=class value='info'}
			{elseif $counter==2}
				{assign var=class value='danger'}
			{elseif $counter==3}
				{assign var=class value='warning'}
			{elseif $counter==4}
				{assign var=class value='default'}
				{$counter = 0}
			{/if}
            <div class="panel panel-{$class}">
			  <div class="panel-body">
				<div class="media">
				  
				  <div class="media-body">
					<h4 class="media-heading"><a target="_blank" href="/article/{$articles.execId}/{$articles.articleID}"><b>{$articles.headline}</b></a></h4>
					{$articles.text}
				  </div>
				  <div class="media-right">
					<a href="#">
						{if isset($articles.execImgURL)}
						<img class="media-object" src="{$articles.execImgURL}" 
							 class="img-responsive">
						{else}
						<img class="media-object" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 class="img-responsive">
						{/if}
					</a>
					<h5>{$articles.execFullName}</h5><br/>
					{$articles.startDate|date_format} 
				  </div>
				</div>
			  </div>
			</div>
			{/foreach}
        </div>
    </div>

    
</div>