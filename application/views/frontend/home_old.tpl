<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class="">Rupert Murdoch</h3>

                    </div>
                    <div class="panel-body">
                        <img src="http://images.bwbx.io/cms/2012-12-03/1203_murdoch_daily_630x420.jpg"
                             class="img-responsive">
                        <br/><br/>
                        <dl class=""> <dt class="">Born:</dt>

                            <dd class="">11 March 1931 (age 83) Sydney, Australia</dd> <dt class="">Occupation:</dt>

                            <dd
                                    class="">Chairman and CEO of News Corporation (1979–2013)</dd>
                            <dd class="">Executive Chairman of News Corp (2013–present)</dd>
                            <dd class="">Chairman and CEO of 21st Century Fox (2013–present)</dd> <dt class="">Net-Worth:</dt>

                            <dd
                                    class="">US$ 14.9 billion (2014)</dd> <dt class="">Board member of:</dt>

                            <dd class="">News Corp, 21st Century Fox</dd>
                        </dl>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="">Keywords</h4>
                        </div>
                        <div class="panel-body">
                            <a href="#"><span class="badge alert-info">Media Mogul</span></a>
                            <a href="#"><span class="badge alert-danger">Conglomerate</span></a>
                            <a href="#"><span class="badge alert-success">News Corp</span></a>
                            <a href="#"><span class="badge alert-warning">Purchase</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <iframe src="http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=1T8NgS-TVfDS-VaxSAQrwyuHIEmah5qVJ-C2I70mYzXs&font=DroidSerif-DroidSans&maptype=toner&lang=en&height=450"
                        width="100%" height="450" frameborder="0" class=""></iframe>
            </div>
            <div class="row well">
                <div class="tabbable">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="#tab1" data-toggle="tab">Most Viewed Answers</a></li>
                        <li><a href="#tab2" data-toggle="tab">Best Questions</a></li>
                        <li><a href="#tab3" data-toggle="tab">Recently Answered</a></li>
                    </ul>
                    <div class="tab-content" style="margin: 10px;">
                        <div class="tab-pane active" id="tab1">
                            <div class="container">
                                <dl>

                                    <dt>145 <i class="cus-eye"></i> Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>140 <i class="cus-eye"></i> What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>133 <i class="cus-eye"></i> Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>124 <i class="cus-eye"></i> What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>120 <i class="cus-eye"></i> Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>110 <i class="cus-eye"></i> What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="container">
                                <dl>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="container">
                                <dl>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center" style="background-color:black;color:white">
        <h2 style="color:white">
            SAINT LAURENT
        </h2>
        <p>SAC DE JOUR</p>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <iframe width="490" height="315" src="https://www.youtube.com/embed/e9TC12UQ8og" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-3" style="height: 315px;overflow: scroll;">
                    <div class="row">
                        <iframe width="200" height="150" src="https://www.youtube.com/embed/P74oHhU5MDk" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="https://www.youtube.com/embed/khSX6jxuV6A" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="https://www.youtube.com/embed/Lgw0D2wYtZA" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="https://www.youtube.com/embed/sjQxs7ErXuw" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row well" style="">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab11" data-toggle="tab">Tweets</a></li>
                        <li><a href="#tab22" data-toggle="tab">Tweets & Photos</a></li>
                        <li><a href="#tab33" data-toggle="tab">Retweets</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab11">
                            <div class="row">
                                <a class="twitter-timeline" href="https://twitter.com/rupertmurdoch" data-widget-id="582548650905235456">Tweets by @rupertmurdoch</a>
                                {literal}
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                {/literal}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab22">
                            <div class="row">
                                <a class="twitter-timeline" href="https://twitter.com/rupertmurdoch/favorites" data-favorites-screen-name="rupertmurdoch" data-widget-id="582548650905235456">Favorite Tweets by @rupertmurdoch</a>
                                {literal}
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                {/literal}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab33">
                            <div class="row">
                                <a class="twitter-timeline" href="https://twitter.com/rupertmurdoch" data-widget-id="582556794754580480">Tweets by @rupertmurdoch</a>
                                {literal}
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                {/literal}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>