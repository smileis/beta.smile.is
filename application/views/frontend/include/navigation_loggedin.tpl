<div class="navbar navbar-default navbar-fixed-top"  style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
    </div>
    <div class="navbar-collapse collapse ">
        <ul class="nav navbar-nav">
            <li {if $uri_1 == "home"}class="active"{/if}><a href="{$base_url}home">Home</a></li>
            <li {if $uri_1 == "profile"}class="active"{/if}><a href="{$base_url}profile">Profile</a></li>
            <li {if $uri_1 == "smiletree" || $uri_1 == "issues" || $uri_1 == "initiatives" ||$uri_1 == "impact"}
                class="active"{/if}
                    class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">View <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{$base_url}smiletree">Timeline</a></li>
                    <li><a href="{$base_url}issues">Followers</a></li>
                    <li><a href="{$base_url}issues">Idea Network</a></li>
                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-left" method="POST" action="{$base_url}search">
            <input name="search" type="text" class="form-control col-lg-8" placeholder="Search...">
        </form>
        <ul class="nav navbar-nav navbar-right">
            <!-- <li>
                <blockquote style="font-size: small">
                    <p>Hello there ! this is daily quote</p>
                </blockquote>
            </li> -->
            <li {if $uri_1 == "smiletree" || $uri_1 == "issues" || $uri_1 == "initiatives" ||$uri_1 == "impact"}
                class="active"{/if}
                    class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{$base_url}smiletree">Edit Account Info</a></li>
                    <li><a href="{$base_url}issues">Edit Timeline</a></li>
                    <li><a href="{$base_url}issues">Edit Privacy</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$userdetails.firstname} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li {if $uri_1 == "messages"}class="active"{/if}><a href="{$base_url}messages">Messages</a></li>
                    <li {if $uri_1 == "requests"}class="active"{/if}><a href="{$base_url}requests">Requests</a></li>
                    <li class="divider"></li>
                    <li><a href="{$base_url}logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
