    </div> <!--end of row -->
</div> <!-- end of content -->

<div class="navbar navbar-default footer" style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
		<blockquote>
        <p style="color:white">To inspire and learn from the most successful people around the world, in one place.</p>
        </blockquote>
    </div>
	<div class="navbar-collapse collapse ">
        
        
		<a class="navbar-brand navbar-right" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
        
    </div>
</div>

<!--<div class="footer">
    <div class="container " >
        
    </div>
</div>-->

<!-- container ends-->
<!-- JS -->
<!-- Included in header now
<script src="{$base_url}assets/admin/js/jquery.js"></script> jQuery
<script src="{$base_url}assets/admin/js/bootstrap.min.js"></script>  Bootstrap
<script src="{$base_url}assets/admin/js/jquery-ui.min.js"></script>  jQuery UI -->

    <script src="{$base_url}assets/frontend/js/home.js"></script>
    <script src="{$base_url}assets/frontend/js/storyjs-embed.js"></script>
	<script src="{$base_url}assets/frontend/js/register.js"></script>
	
<script src="{$base_url}assets/admin/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
<script src="{$base_url}assets/admin/js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
<script src="{$base_url}assets/admin/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
<script src="{$base_url}assets/admin/js/jquery.slimscroll.min.js"></script> <!-- jQuery Slim Scroll -->
<script src="{$base_url}assets/admin/js/jquery.dataTables.min.js"></script> <!-- Data tables -->

<!-- jQuery Flot -->
<script src="{$base_url}assets/admin/js/excanvas.min.js"></script>
<script src="{$base_url}assets/admin/js/jquery.flot.js"></script>
<script src="{$base_url}assets/admin/js/jquery.flot.resize.js"></script>
<script src="{$base_url}assets/admin/js/jquery.flot.pie.js"></script>
<script src="{$base_url}assets/admin/js/jquery.flot.stack.js"></script>

<!-- jQuery Notification - Noty -->
<script src="{$base_url}assets/admin/js/jquery.noty.js"></script> <!-- jQuery Notify -->
<script src="{$base_url}assets/admin/js/themes/default.js"></script> <!-- jQuery Notify -->
<script src="{$base_url}assets/admin/js/layouts/bottom.js"></script> <!-- jQuery Notify -->
<script src="{$base_url}assets/admin/js/layouts/topRight.js"></script> <!-- jQuery Notify -->
<script src="{$base_url}assets/admin/js/layouts/top.js"></script> <!-- jQuery Notify -->
<!-- jQuery Notification ends -->

<script src="{$base_url}assets/admin/js/sparklines.js"></script> <!-- Sparklines -->
<script src="{$base_url}assets/admin/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
<script src="{$base_url}assets/admin/js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
<script src="{$base_url}assets/admin/js/jquery.onoff.min.js"></script> <!-- Bootstrap Toggle -->
<script src="{$base_url}assets/admin/js/filter.js"></script> <!-- Filter for support page -->

<script src="{$base_url}assets/admin/js/charts.js"></script> <!-- Charts & Graphs -->

</body>
</html>
