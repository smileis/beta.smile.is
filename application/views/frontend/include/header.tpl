<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{$title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Stylesheets
        Theme options: boot_flatly yeti cosmo cerulean journal{*$theme*}
    -->
    <link rel="stylesheet" href="{$base_url}assets/frontend/css/theme-core.min.css">

    <link href="{$base_url}assets/frontend/themes/yeti/bootstrap.min.css" rel="stylesheet">
    <!-- Font awesome icon -->
    <link href="{$base_url}assets/css/cus-icons.css" rel="stylesheet">
    {for i $cssArray}
        <link href="{$cssArray.$i}" rel="stylesheet"> {* or $arr[$i] *}
    {/for}
	<link rel="stylesheet" href="{$base_url}assets/frontend/css/custom_login.css">
    <link rel="stylesheet" href="{$base_url}assets/admin/css/font-awesome.min.css">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/jquery-ui.css">
    <!-- Calendar -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/fullcalendar.css">
    <!-- prettyPhoto -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/prettyPhoto.css">
    <!-- Star rating -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/rateit.css">
    <!-- Date picker -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/bootstrap-datetimepicker.min.css">
    <!-- CLEditor -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/jquery.cleditor.css">
    <!-- Data tables -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/jquery.dataTables.css">
    <!-- Bootstrap toggle -->
    <link rel="stylesheet" href="{$base_url}assets/admin/css/jquery.onoff.css">
	


    <!-- Le fav and touch icons -->
    <link href="{$base_url}assets/ico/favicon.ico'); ?>" rel="shortcut icon">

    <script src="{$base_url}assets/admin/js/jquery.js"></script> <!-- jQuery -->
    <script src="{$base_url}assets/admin/js/bootstrap.min.js"></script> <!-- Bootstrap -->
    <script src="{$base_url}assets/admin/js/jquery-ui.min.js"></script> <!-- jQuery UI -->


    {if $auth == 0}

    {else}

    {/if}




</head>
<body>
{include file='navigation.tpl'}
<div class="container">
    <div class="row clearfix">