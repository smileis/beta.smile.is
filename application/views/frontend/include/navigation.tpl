<div class="navbar navbar-default navbar-fixed-top" style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
    </div>
    <div class="navbar-collapse collapse ">
        <ul class="nav navbar-nav">
            <li {if $uri_1 == "login"}class="active"{/if}><a href="{$base_url}login">Login</a></li>
            <li {if $uri_1 == "register"}class="active"{/if}><a href="{$base_url}register">Register</a></li>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
            <li {if $uri_1 == "howitworks"}class="active"{/if}><a href="{$base_url}howitworks">How it Works!</a></li>
            <li {if $uri_1 == "about"}class="active"{/if}><a href="{$base_url}about">About Us</a></li>
        </ul>
    </div>
</div>
