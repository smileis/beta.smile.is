<div class="container">
    <div class="row">

        <div class="col-md-10" style="position:relative;">


                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="media">

                            <div class="media-body">
                                <h4 class="media-heading"><b>{$articlePageObj.headline}</b></h4>

                                {foreach from=$articlePageObj.leadParagraph item=paragraphs}
                                    <p>{$paragraphs->Text}</p>
                                {/foreach}

                                {foreach from=$articlePageObj.body item=paragraphs}
                                    <p>{$paragraphs->Text}</p>
                                {/foreach}

                                {foreach from=$articlePageObj.trailParagraph item=paragraphs}
                                    <p>{$paragraphs->Text}</p>
                                {/foreach}
                            </div>
                            <div class="media-right">
                                <a href="#">
                                    {if isset($articlePageObj.execImgURL)}
                                        <img class="media-object" src="{$articlePageObj.execImgURL}"
                                             class="img-responsive">
                                    {else}
                                        <img class="media-object" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
                                             class="img-responsive">
                                    {/if}
                                </a>
                                <h5>{$articlePageObj.execFullName}</h5><br/>
                                {$articlePageObj.startDate|date_format}
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        <div class="col-md-2 sidebar-outer" >
            <div class="panel panel-default sidebar">
                <div class="panel-body">

                    {if isset($profimg)}
                <img src="{$execprofimg}" width="160px" height="160px"
                        >
                    {else}
                <img width="160px" height="160px" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
                        >
                    {/if}<br/>
                    <b>{$userdetails.firstname}</b>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="../../home" >
                                All
                            </a>
                        </li>

                        {foreach from=$execsObj item=execdetails}
                            <li class="list-group-item">
                                <a id="execart" href="{strip}{$execdetails.Executive->FCode}{/strip}">
                                    {$execdetails.Executive->DisplayName}
                                </a>
                                <a target="_blank" href="{strip}{$execdetails.Executive->FirstName}_{$execdetails.Executive->LastName}{/strip}" style="float:right;">
                                    <i class="fa fa-share"></i>
                                </a>

                            </li>
                        {/foreach}
                    </ul>
                    <!--<button class="btn btn-xs btn-primary" type="button">Messages</button>
                        <button class="btn btn-xs btn-success" type="button">Messages</button>
                        <button class="btn btn-xs btn-info" type="button">Messages</button>
                        <button class="btn btn-xs btn-warning" type="button">Messages</button>-->

                </div>
            </div>
        </div>
    </div>


</div>