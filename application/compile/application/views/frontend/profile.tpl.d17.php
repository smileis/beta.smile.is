<?php
/* template head */
if (function_exists('Dwoo_Plugin_date_format')===false)
	$this->getLoader()->loadPlugin('date_format');
/* end template head */ ob_start(); /* template body */ ?><div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class=""><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FirstName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?> <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'LastName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?></h3>
                        <hr/>
                        <h4><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'DisplayName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?></h4>
                    </div>
                    <div class="panel-body">
                        <?php if (((isset($this->scope["execprofimg"]) ? $this->scope["execprofimg"] : null) !== null)) {
?>
                            <img src="<?php echo $this->scope["execprofimg"];?>" width="260px" height="260px"
                                 class="img-responsive">
                        <?php 
}
else {
?>
                            <img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
                                 class="img-responsive">
                        <?php 
}?>




                        <br/><br/>
                        <dl class=""> <dt class="">Job Title:</dt>
                            <dd class=""><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'JobTitle',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?></dd>
                            <!--<dd class="">11 March 1931 (age 83) Sydney, Australia</dd> -->
                            <dt class="">Biography:</dt>

                            <dd class="" style="text-align: justify"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'Paragraph',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Biography"], false);?></dd>

                        </dl>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="">Keywords</h4>
                        </div>
                        <div class="panel-body">
                            <?php $this->scope["counter"]=0?>

                            <?php echo $this->assignInScope('info', 'class');?>

                            <?php 
$_fh0_data = (isset($this->scope["keywords"]) ? $this->scope["keywords"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['keyword'])
	{
/* -- foreach start output */
?>
                                <?php $this->scope["counter"]=((isset($this->scope["counter"]) ? $this->scope["counter"] : null) + 1)?>

                                <?php if ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 1) {
?>
                                    <?php echo $this->assignInScope('info', 'class');?>

                                <?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 2) {
?>
                                    <?php echo $this->assignInScope('danger', 'class');?>

                                <?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 3) {
?>
                                    <?php echo $this->assignInScope('warning', 'class');?>

                                <?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 4) {
?>
                                    <?php echo $this->assignInScope('default', 'class');?>

                                    <?php $this->scope["counter"]=0?>

                                <?php 
}?>

                                <a href="#"><span class="badge alert-<?php echo $this->scope["class"];?>"><?php echo $this->scope["keyword"];?></span></a>
                            <?php 
/* -- foreach end output */
	}
}?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div id="timeline-embed"></div>
                <script type="text/javascript">
                    var timeline_config = {
                        width: "100%",
                        height: "500",
                        source: <?php echo $this->scope["timelinejSONObj"];?>

                    }
                </script>

            <!-- <?php echo $this->scope["base_url"];?>assets/frontend/css/example.json -->


                <!--<iframe src="http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=1T8NgS-TVfDS-VaxSAQrwyuHIEmah5qVJ-C2I70mYzXs&font=DroidSerif-DroidSans&maptype=toner&lang=en&height=450"
                        width="100%" height="450" frameborder="0" class=""></iframe>-->
            </div>
            <div class="row well">
                <div class="tabbable">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="#tab1" data-toggle="tab">Colleagues</a></li>
                        <li><a href="#tab2" data-toggle="tab">Inspiring Statements</a></li>
                        <li><a href="#tab3" data-toggle="tab">Questions Poll</a></li>
                    </ul>
                    <div class="tab-content" style="margin: 10px;">
                        <div class="tab-pane active" id="tab1">
                            <div class="container">
                                <?php 
$_fh1_data = (isset($this->scope["colleagues"]) ? $this->scope["colleagues"] : null);
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['contact'])
	{
/* -- foreach start output */
?>
                                    <div class="row">
                                            <a href="<?php echo $this->scope["base_url"];
echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FirstName',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["contact"], false);?>_<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'LastName',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["contact"], false);?>"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FullName',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["contact"], false);?></a>,
                                            <b><i><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'JobTitle',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["contact"], false);?></i></b>
                                    </div>
                                <?php 
/* -- foreach end output */
	}
}?>

                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="container">
								<dl>
                                    <dt>Followers Name</dt>
                                    <dd>Inspiring statement 1.......</dd>
                                    <dt>Followers Name</dt>
                                    <dd>Inspiring statement 2.......</dd>
									<dt>Followers Name</dt>
                                    <dd>Inspiring statement 3.......</dd>
									<dt>Followers Name</dt>
                                    <dd>Inspiring statement 4.......</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="container">
                                <dl>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                    <dt>What was your age when you started your first company?</dt>
                                    <dd>I was 18.</dd>
                                    <dt>Which was your first company?</dt>
                                    <dd>My first company was News Corporation</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center" style="background-color:black;color:white">
        <h2 style="color:white">
            SAINT LAURENT
        </h2>
        <p>SAC DE JOUR</p>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <iframe width="490" height="315" src="<?php echo $this->scope["youtube_videos"]["0"]["link"];?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-3" style="height: 315px;overflow: scroll;">
                    <div class="row">
                        <iframe width="200" height="150" src="<?php echo $this->scope["youtube_videos"]["1"]["link"];?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="<?php echo $this->scope["youtube_videos"]["2"]["link"];?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="<?php echo $this->scope["youtube_videos"]["3"]["link"];?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row">
                        <iframe width="200" height="150" src="<?php echo $this->scope["youtube_videos"]["4"]["link"];?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row well" style="">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab11" data-toggle="tab">Tweets</a></li>
                        <li><a href="#tab22" data-toggle="tab">Tweets & Photos</a></li>
                        <li><a href="#tab33" data-toggle="tab">Retweets</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab11">
                            <div class="row">
                                <?php if (((isset($this->scope["twitter_timeline"]) ? $this->scope["twitter_timeline"] : null) !== null)) {
?>
                                    <?php 
$_fh2_data = (isset($this->scope["twitter_timeline"]) ? $this->scope["twitter_timeline"] : null);
if ($this->isTraversable($_fh2_data) == true)
{
	foreach ($_fh2_data as $this->scope['status'])
	{
/* -- foreach start output */
?>

                                            <b><i><?php echo Dwoo_Plugin_date_format($this, $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'date',  ),  3 =>   array (    0 => '',    1 => '',  ),), (isset($this->scope["status"]) ? $this->scope["status"]:null), true), '%b %e, %Y', null);?> </i></b>
                                            <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'text',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["status"], false);?><br/>
                                            @<b><?php echo $this->scope["twitter_screenName"];?></b><br/>

                                    <?php 
/* -- foreach end output */
	}
}?>

                                <?php 
}?>

                            </div>
                        </div>
                        <div class="tab-pane" id="tab22">
                            <div class="row">
                                <?php if (((isset($this->scope["twitter_timeline"]) ? $this->scope["twitter_timeline"] : null) !== null)) {
?>
                                    <?php 
$_fh3_data = (isset($this->scope["twitter_timeline"]) ? $this->scope["twitter_timeline"] : null);
if ($this->isTraversable($_fh3_data) == true)
{
	foreach ($_fh3_data as $this->scope['status'])
	{
/* -- foreach start output */
?>

                                            <b><i><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'date',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["status"], false);?> </i></b>
                                            <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'text',  ),  3 =>   array (    0 => '',    1 => '',  ),), $this->scope["status"], false);?>


                                    <?php 
/* -- foreach end output */
	}
}?>

                                <?php 
}?>

                            </div>
                        </div>
                        <div class="tab-pane" id="tab33">
                            <div class="row">
                                <?php if (((isset($this->scope["twitter_screenName"]) ? $this->scope["twitter_screenName"] : null) !== null)) {
?>
                                    <a class="twitter-timeline" href="https://twitter.com/<?php echo $this->scope["twitter_screenName"];?>" data-widget-id="582556794754580480">Tweets by @rupertmurdoch</a>
                                
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                
                                <?php 
}
else {
?>
                                    Twitter User Details Not Found!
                                <?php 
}?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>