<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="navbar navbar-default navbar-fixed-top"  style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
    </div>
    <div class="navbar-collapse collapse ">
        <ul class="nav navbar-nav">
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "home") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>home">Home</a></li>
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "profile") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>profile">Profile</a></li>
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "smiletree" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "issues" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "initiatives" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "impact") {
?>
                class="active"<?php 
}?>

                    class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">View <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $this->scope["base_url"];?>smiletree">Timeline</a></li>
                    <li><a href="<?php echo $this->scope["base_url"];?>issues">Followers</a></li>
                    <li><a href="<?php echo $this->scope["base_url"];?>issues">Idea Network</a></li>
                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-left" method="POST" action="<?php echo $this->scope["base_url"];?>search">
            <input name="search" type="text" class="form-control col-lg-8" placeholder="Search...">
        </form>
        <ul class="nav navbar-nav navbar-right">
            <!-- <li>
                <blockquote style="font-size: small">
                    <p>Hello there ! this is daily quote</p>
                </blockquote>
            </li> -->
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "smiletree" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "issues" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "initiatives" || (isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "impact") {
?>
                class="active"<?php 
}?>

                    class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $this->scope["base_url"];?>smiletree">Edit Account Info</a></li>
                    <li><a href="<?php echo $this->scope["base_url"];?>issues">Edit Timeline</a></li>
                    <li><a href="<?php echo $this->scope["base_url"];?>issues">Edit Privacy</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->scope["userdetails"]["firstname"];?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "messages") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>messages">Messages</a></li>
                    <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "requests") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>requests">Requests</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->scope["base_url"];?>logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>