<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?>    </div> <!--end of row -->
</div> <!-- end of content -->

<div class="navbar navbar-default footer" style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
		<blockquote>
        <p style="color:white">To inspire and learn from the most successful people around the world, in one place.</p>
        </blockquote>
    </div>
	<div class="navbar-collapse collapse ">
        
        
		<a class="navbar-brand navbar-right" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
        
    </div>
</div>

<!-- container ends-->
<!-- JS -->
<!-- Included in header now
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.js"></script> jQuery
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/bootstrap.min.js"></script>  Bootstrap
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery-ui.min.js"></script>  jQuery UI -->


<script src="<?php echo $this->scope["base_url"];?>assets/frontend/js/home.js"></script>
<script src="<?php echo $this->scope["base_url"];?>assets/frontend/js/storyjs-embed.js"></script>

<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
<script src="<?php echo $this->scope["base_url"];?>assets/frontend/js/jquery.tokenize.js"></script>

<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.slimscroll.min.js"></script> <!-- jQuery Slim Scroll -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.dataTables.min.js"></script> <!-- Data tables -->

<!-- jQuery Flot -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/excanvas.min.js"></script>
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.flot.js"></script>
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.flot.resize.js"></script>
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.flot.pie.js"></script>
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.flot.stack.js"></script>

<!-- jQuery Notification - Noty -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.noty.js"></script> <!-- jQuery Notify -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/themes/default.js"></script> <!-- jQuery Notify -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/layouts/bottom.js"></script> <!-- jQuery Notify -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/layouts/topRight.js"></script> <!-- jQuery Notify -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/layouts/top.js"></script> <!-- jQuery Notify -->
<!-- jQuery Notification ends -->

<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/sparklines.js"></script> <!-- Sparklines -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.onoff.min.js"></script> <!-- Bootstrap Toggle -->
<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/filter.js"></script> <!-- Filter for support page -->

<script src="<?php echo $this->scope["base_url"];?>assets/admin/js/charts.js"></script> <!-- Charts & Graphs -->
    <?php 
$_for0_from = (isset($this->scope["jsArray"]) ? $this->scope["jsArray"] : null);
$_for0_to = null;
$_for0_step = abs(1);
if (is_numeric($_for0_from) && !is_numeric($_for0_to)) { $this->triggerError('For requires the <em>to</em> parameter when using a numerical <em>from</em>'); }
$tmp_shows = $this->isArray($_for0_from, true) || (is_numeric($_for0_from) && (abs(($_for0_from - $_for0_to)/$_for0_step) !== 0 || $_for0_from == $_for0_to));
if ($tmp_shows)
{
	if ($this->isArray($_for0_from) == true) {
		$_for0_to = is_numeric($_for0_to) ? $_for0_to - $_for0_step : $this->count($_for0_from) - 1;
		$_for0_from = 0;
	}
	if ($_for0_from > $_for0_to) {
				$tmp = $_for0_from;
				$_for0_from = $_for0_to;
				$_for0_to = $tmp;
			}
	for ($this->scope['i'] = $_for0_from; $this->scope['i'] <= $_for0_to; $this->scope['i'] += $_for0_step)
	{
/* -- for start output */
?>
        <script src="<?php echo $this->readVar("jsArray.".(isset($this->scope["i"]) ? $this->scope["i"] : null));?>"></script>
    <?php /* -- for end output */
	}
}
?>

<script type="text/javascript">
	$("#interest_result").hide();
	$("#interest_search").submit(function(e){
		$("#interest_result").show();
		
		e.preventDefault();
	});
	var url = "loadPageClass/djapi/recommendExec";
	$('#recommend_list_data').html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
	//$('#result_list_data').html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
	
	$( "#recommend_list_data" ).load( url, function( response, status, xhr ) {
	  if ( status == "error" ) {
		var msg = "Sorry but there was an error: ";
		//alert(msg);
		//$( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
	  } else {
		//alert(response);
	  }
	});

	$("#industry_submit").submit(function(e){
		//alert("sdf");
		var selected_ind = "";
		$('#sel_ind option').each(function() {
				selected_ind = selected_ind + "|" + $(this).val();
		});
		//alert(selected_ind);
		
		var base_url_c = "loadPageClass/ajax/saveIndustry";
		$.post( base_url_c, { data : selected_ind}, function( data ) {
			//alert( "Data Loaded: " + data );
			alert("Industry list saved!");
			$('#industry_submit').html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
			var hostname = $(location).attr('hostname'); 
			window.location.replace("/recommend");
			//e.preventDefault();
		});
		
	});
	
	$("#update_execs").click(function(e){
		//alert("sdf");
		var selected_execs = "";
		$('#sel_exec option').each(function() {
				selected_execs = selected_execs + "|" + $(this).val();
		});
		//alert(selected_execs);
		var base_url_c = "loadPageClass/ajax/saveExecs";
		//var data = "data="+selected_execs;
		//alert(data);
		$.post( base_url_c, { data : selected_execs}, function( data ) {
			//alert( "Data Loaded: " + data );
			alert("Execs list updated!");
			$('#update_execs').html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
			//alert(data);
			var hostname = $(location).attr('hostname'); 
			window.location.replace("/home");
			//e.preventDefault();
		});
	});
	
	$("#save_execs").submit(function(e){
		//alert("sdf");
		var selected_execs = "";
		$('#sel_exec option').each(function() {
				selected_execs = selected_execs + "|" + $(this).val();
		});
		//alert(selected_execs);
		var base_url_c = "loadPageClass/ajax/saveExecs";
		$.post( base_url_c, { data : selected_execs}, function( data ) {
			alert( "Data Loaded: " + data );
			alert("Execs list saved!");
			$('#save_execs').html('<img width="40px" height="40px" src="assets/img/loading.gif"> loading...');
			var hostname = $(location).attr('hostname'); 
			window.location.replace("/home");
			//e.preventDefault();
		});
	});

    $(document).on('click', '.flag-content',function(){
        alert("hello");
    });

    $('.flag-content').click(function(e){
       alert("hello");
    });
	
	$(document).on('click', '.addExec',function(){
		//alert($(this).attr('id'));
		//populate the data from api in the result div 1px solid black
		var val = $(this).attr('id');
		var id = val+"_box";
		
		
		//sel_exec
		var exists = 0;
		$('#sel_exec option').each(function() {
			if ( $(this).val() == val ) {
				exists = 1;
			}
		});
		
		if(exists == 0){
			$('#sel_exec').append($('<option>', {
				value: val,
				text: val
			}));
			$("#"+id ).css("border","1px solid black");
		}
	});
	
	$(document).on('click', '.removeExec',function(){
		//alert($(this).attr('id'));
		//populate the data from api in the result div 1px solid black
		var val = $(this).attr('id');
		var id = val+"_box";
		
		
		//sel_exec
		var exists = 0;
		$('#sel_exec option').each(function() {
			if ( $(this).val() == val ) {
				exists = 1;
				$(this).remove();
				$("#"+id ).css("border","1px solid #dddddd");
			}
		});
		
	});
	
	$(document).on('click', '.industryVal',function(){
		if($(this).is(':checked')){
			var val = $(this).val();// + "|" + $(this).attr('id');
			var exists = 0;
			$('#sel_ind option').each(function() {
				if ( $(this).val() == val ) {
					exists = 1;
				}
			});
			if(exists == 0){
				$('#sel_ind').append($('<option>', {
					value: val,
					text: $(this).attr('id')
				}));
			}
		}
		else {
			var val = $(this).val();
			$('#sel_ind option').each(function() {
				if ( $(this).val() == val ) {
					$(this).remove();
				}
			});
		}
		
		
	});
	
	
</script>

</body>
</html>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>