<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="navbar navbar-default navbar-fixed-top" style="padding: 0 10px">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Storyof.CEO<sup><i> demo</i></sup></a>
    </div>
    <div class="navbar-collapse collapse ">
        <ul class="nav navbar-nav">
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "login") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>login">Login</a></li>
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "register") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>register">Register</a></li>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "howitworks") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>howitworks">How it Works!</a></li>
            <li <?php if ((isset($this->scope["uri_1"]) ? $this->scope["uri_1"] : null) == "about") {
?>class="active"<?php 
}?>><a href="<?php echo $this->scope["base_url"];?>about">About Us</a></li>
        </ul>
    </div>
</div>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>