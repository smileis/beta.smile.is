<?php
/* template head */
if (function_exists('Dwoo_Plugin_include')===false)
	$this->getLoader()->loadPlugin('include');
/* end template head */ ob_start(); /* template body */ ?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->scope["title"];?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Stylesheets
        Theme options: boot_flatly yeti cosmo cerulean journal
    -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/frontend/css/theme-core.min.css">

    <link href="<?php echo $this->scope["base_url"];?>assets/frontend/themes/yeti/bootstrap.min.css" rel="stylesheet">
    <!-- Font awesome icon -->
    <link href="<?php echo $this->scope["base_url"];?>assets/css/cus-icons.css" rel="stylesheet">
    <?php 
$_for0_from = (isset($this->scope["cssArray"]) ? $this->scope["cssArray"] : null);
$_for0_to = null;
$_for0_step = abs(1);
if (is_numeric($_for0_from) && !is_numeric($_for0_to)) { $this->triggerError('For requires the <em>to</em> parameter when using a numerical <em>from</em>'); }
$tmp_shows = $this->isArray($_for0_from, true) || (is_numeric($_for0_from) && (abs(($_for0_from - $_for0_to)/$_for0_step) !== 0 || $_for0_from == $_for0_to));
if ($tmp_shows)
{
	if ($this->isArray($_for0_from) == true) {
		$_for0_to = is_numeric($_for0_to) ? $_for0_to - $_for0_step : $this->count($_for0_from) - 1;
		$_for0_from = 0;
	}
	if ($_for0_from > $_for0_to) {
				$tmp = $_for0_from;
				$_for0_from = $_for0_to;
				$_for0_to = $tmp;
			}
	for ($this->scope['i'] = $_for0_from; $this->scope['i'] <= $_for0_to; $this->scope['i'] += $_for0_step)
	{
/* -- for start output */
?>
        <link href="<?php echo $this->readVar("cssArray.".(isset($this->scope["i"]) ? $this->scope["i"] : null));?>" rel="stylesheet"> 
    <?php /* -- for end output */
	}
}
?>

	<link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/frontend/css/custom_login.css">
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/font-awesome.min.css">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/jquery-ui.css">
    <!-- Calendar -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/fullcalendar.css">
    <!-- prettyPhoto -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/prettyPhoto.css">
    <!-- Star rating -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/rateit.css">
    <!-- Date picker -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/bootstrap-datetimepicker.min.css">
    <!-- CLEditor -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/jquery.cleditor.css">
    <!-- Data tables -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/jquery.dataTables.css">
    <!-- Bootstrap toggle -->
    <link rel="stylesheet" href="<?php echo $this->scope["base_url"];?>assets/admin/css/jquery.onoff.css">
	


    <!-- Le fav and touch icons -->
    <link href="<?php echo $this->scope["base_url"];?>assets/ico/favicon.ico'); ?>" rel="shortcut icon">

    <script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery.js"></script> <!-- jQuery -->
    <script src="<?php echo $this->scope["base_url"];?>assets/admin/js/bootstrap.min.js"></script> <!-- Bootstrap -->
    <script src="<?php echo $this->scope["base_url"];?>assets/admin/js/jquery-ui.min.js"></script> <!-- jQuery UI -->


    <?php if ((isset($this->scope["auth"]) ? $this->scope["auth"] : null) == 0) {
?>

    <?php 
}
else {
?>

    <?php 
}?>





</head>
<body>
<?php echo Dwoo_Plugin_include($this, 'navigation.tpl', null, null, null, '_root', null);?>

<div class="container">
    <div class="row clearfix"><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>