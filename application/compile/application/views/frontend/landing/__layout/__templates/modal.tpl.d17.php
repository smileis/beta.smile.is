<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="modal fade" id="modal-container-<?php echo $this->scope["modal_id"];?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->scope["modal_title"];?></h4>
            </div>
            <div class="modal-body">
                POST /....
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary">
                    Save changes
                </button>
            </div>
        </div>

    </div>

</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>