<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			
			<div class="admin-form">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- Widget starts -->
							<div class="widget wred">
								<div class="widget-head">
									
								</div>
								<div class="widget-content">
									<div class="padd">
										<form class="form-horizontal" action="" method="post" id="save_execs">

											<!-- Registration form starts -->
											<!-- Error -->
											<?php if ((isset($this->scope["message"]) ? $this->scope["message"] : null)) {
?>
												<div class="alert alert-warning">
													<?php echo $this->scope["message"];?>

												</div>
											<?php 
}?>

											<!-- F Name -->
											<div class="panel panel-primary">
												<div class="panel-heading"><i class="fa fa-heart-o fa-2x"></i> Recommendations based on your selection</div>
												<div class="panel-body" id="recommend_list_data">
													
												</div>
												<div class="panel-footer">
													<button type="submit" class="btn btn-sm btn-info" id="submit_execs"  width="100%">Next</button>
												</div>
											</div>
											
										</form>
										
										<div class="panel panel-primary" id="execs_selected" style="display:none">
											<div class="panel-heading">Selected Execs</div>
											<div class="panel-body" id="">
												<select name="sel_exec" id="sel_exec" multiple size="5">
														
												</select>
											</div>
										</div>
										
										
									</div>
								</div>
								<div class="widget-foot">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>