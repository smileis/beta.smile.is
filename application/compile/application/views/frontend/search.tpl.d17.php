<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="container">
	<div class="row clearfix">
		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php if (((isset($this->scope["profimg"]) ? $this->scope["profimg"] : null) !== null)) {
?>
						<img src="<?php echo $this->scope["execprofimg"];?>" width="160px" height="160px"
							 class="img-responsive">
					<?php 
}
else {
?>
						<img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 class="img-responsive">
					<?php 
}?>

					<b><?php echo $this->scope["userdetails"]["firstname"];?></b>
					<div class="list-group">
						<a href="#" class="list-group-item active">
							All
						</a>
						<?php 
$_fh0_data = (isset($this->scope["execsObj"]) ? $this->scope["execsObj"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['execdetails'])
	{
/* -- foreach start output */
?>
							<a href="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FirstName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>_<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'LastName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>" class="list-group-item"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'DisplayName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?></a>
						<?php 
/* -- foreach end output */
	}
}?>

					</div>
					<button class="btn btn-xs btn-primary" type="button">Messages</button>
						<button class="btn btn-xs btn-success" type="button">Messages</button>
						<button class="btn btn-xs btn-info" type="button">Messages</button>
						<button class="btn btn-xs btn-warning" type="button">Messages</button>
				
				</div>
			</div>
        </div>
		<div class="col-md-9 column">
			<?php if ((isset($this->scope["searchCheck"]) ? $this->scope["searchCheck"] : null)) {
?>
				<b>Keyword:</b> <?php echo $this->scope["searchKeyword"];?> 
				<div class="row" id="result_list_data">
					<div class="panel-header">
						<button type="submit" class="btn btn-sm btn-info" id="update_execs"  width="100%">Save</button>
					</div>
					<?php 
$_fh1_data = (isset($this->scope["resultsExecs"]) ? $this->scope["resultsExecs"] : null);
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['exec'])
	{
/* -- foreach start output */
?>
						<div class="col-sm-5 col-md-3">
							<div class="thumbnail" id="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'FCode',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?>_box">
							  <img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
											 class="img-responsive">
							  <div class="caption">
								<a href="<?php echo $this->scope["base_url"];
echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'FirstName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?>_<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'LastName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?>" ><h3><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'DisplayName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?></h3></a>
								<p>
								Company Name: <span class="label label-info"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'CompanyName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?></span><br/>Job Title: <span class="label label-primary"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'JobTitle',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?></span><br/> Industry: <span class="label label-success"><?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'Industry',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?></span></p>
								<p><a href="javascript:" id="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'FCode',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?>" class="addExec btn btn-primary" role="button">Add</a>
								<a href="javascript:" id="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',    1 => '->',  ),  2 =>   array (    0 => 'Executive',    1 => 'FCode',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["exec"], false);?>" class="removeExec btn btn-primary" role="button">Remove</a></p>
							  </div>
							</div>
						  </div>
					<?php 
/* -- foreach end output */
	}
}?>

				</div>
				<div class="panel panel-primary" id="execs_selected" style="display:none">
					<div class="panel-heading">Selected Execs</div>
					<div class="panel-body" id="">
						<select name="sel_exec" id="sel_exec" multiple size="5">
								
						</select>
					</div>
				</div>
			<?php 
}
else {
?>
				search form
				<?php echo $this->scope["searchKeyword"];?>

			<?php 
}?>

		</div>
	</div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>