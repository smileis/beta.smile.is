<?php
/* template head */
if (function_exists('Dwoo_Plugin_date_format')===false)
	$this->getLoader()->loadPlugin('date_format');
/* end template head */ ob_start(); /* template body */ ?><div class="container">
    <div class="row">
		<div class="col-md-2 sidebar-outer" >
			<div class="panel panel-default sidebar">
				<div class="panel-body">
					
					<?php if (((isset($this->scope["profimg"]) ? $this->scope["profimg"] : null) !== null)) {
?>
						<img src="<?php echo $this->scope["execprofimg"];?>" width="160px" height="160px"
							 >
					<?php 
}
else {
?>
						<img width="160px" height="160px" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 >
					<?php 
}?><br/>
					<b><?php echo $this->scope["userdetails"]["firstname"];?></b>
					<ul class="list-group">
                        <li class="list-group-item">
                            <a href="home" >
                                All
                            </a>
                        </li>

						<?php 
$_fh0_data = (isset($this->scope["execsObj"]) ? $this->scope["execsObj"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['execdetails'])
	{
/* -- foreach start output */
?>
                            <li class="list-group-item">
                                <a target="_blank" id="execart" href="home/single/<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FCode',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>">
                                    <?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'DisplayName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>

                                </a>
                                <a target="_blank" href="<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'FirstName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>_<?php echo $this->readVarInto(array (  1 =>   array (    0 => '->',  ),  2 =>   array (    0 => 'LastName',  ),  3 =>   array (    0 => '',    1 => '',    2 => '',  ),), $this->scope["execdetails"]["Executive"], false);?>" style="float:right;">
                                    <i class="fa fa-share"></i>
                                </a>

                            </li>
						<?php 
/* -- foreach end output */
	}
}?>

					</ul>
					<!--<button class="btn btn-xs btn-primary" type="button">Messages</button>
						<button class="btn btn-xs btn-success" type="button">Messages</button>
						<button class="btn btn-xs btn-info" type="button">Messages</button>
						<button class="btn btn-xs btn-warning" type="button">Messages</button>-->
				
				</div>
			</div>
        </div>
        <div class="col-md-10" style="position:relative;">
			<?php $this->scope["counter"]=0?>

            <?php echo $this->assignInScope('info', 'class');?>

			<?php 
$_fh1_data = (isset($this->scope["articlesObj"]) ? $this->scope["articlesObj"] : null);
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['articles'])
	{
/* -- foreach start output */
?>
			<?php $this->scope["counter"]=((isset($this->scope["counter"]) ? $this->scope["counter"] : null) + 1)?>

			<?php if ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 1) {
?>
				<?php echo $this->assignInScope('info', 'class');?>

			<?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 2) {
?>
				<?php echo $this->assignInScope('danger', 'class');?>

			<?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 3) {
?>
				<?php echo $this->assignInScope('warning', 'class');?>

			<?php 
}
elseif ((isset($this->scope["counter"]) ? $this->scope["counter"] : null) == 4) {
?>
				<?php echo $this->assignInScope('default', 'class');?>

				<?php $this->scope["counter"]=0?>

			<?php 
}?>

            <div class="panel panel-<?php echo $this->scope["class"];?>">
			  <div class="panel-body">
				<div class="media">
				  
				  <div class="media-body">
					<h4 class="media-heading"><a target="_blank" href="/article/<?php echo $this->scope["articles"]["execId"];?>/<?php echo $this->scope["articles"]["articleID"];?>"><b><?php echo $this->scope["articles"]["headline"];?></b></a></h4>
					<?php echo $this->scope["articles"]["text"];?>

				  </div>
				  <div class="media-right">
					<a href="#">
						<?php if (((isset($this->scope["articles"]["execImgURL"]) ? $this->scope["articles"]["execImgURL"]:null) !== null)) {
?>
						<img class="media-object" src="<?php echo $this->scope["articles"]["execImgURL"];?>" 
							 class="img-responsive">
						<?php 
}
else {
?>
						<img class="media-object" src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
							 class="img-responsive">
						<?php 
}?>

					</a>
					<h5><?php echo $this->scope["articles"]["execFullName"];?></h5><br/>
					<?php echo Dwoo_Plugin_date_format($this, (isset($this->scope["articles"]["startDate"]) ? $this->scope["articles"]["startDate"]:null), '%b %e, %Y', null);?> 
				  </div>
				</div>
			  </div>
			</div>
			<?php 
/* -- foreach end output */
	}
}?>

        </div>
    </div>

    
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>