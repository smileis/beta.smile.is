<?php
/* template head */
if (function_exists('Dwoo_Plugin_include')===false)
	$this->getLoader()->loadPlugin('include');
/* end template head */ ob_start(); /* template body */ ;
if ((isset($this->scope["auth"]) ? $this->scope["auth"] : null) == 0) {
?>
    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["header_login_tag"]) ? $this->scope["header_login_tag"] : null), null, null, null, '_root', null);?>

    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["main_body"]) ? $this->scope["main_body"] : null), null, null, null, '_root', null);?>

    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["footer_login_tag"]) ? $this->scope["footer_login_tag"] : null), null, null, null, '_root', null);?>

<?php 
}
else {
?>
    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["header_tag"]) ? $this->scope["header_tag"] : null), null, null, null, '_root', null);?>

    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["nav_menu"]) ? $this->scope["nav_menu"] : null), null, null, null, '_root', null);?>

    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["main_body"]) ? $this->scope["main_body"] : null), null, null, null, '_root', null);?>

    <?php echo Dwoo_Plugin_include($this, (isset($this->scope["footer_tag"]) ? $this->scope["footer_tag"] : null), null, null, null, '_root', null);?>

<?php 
}?>


<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>