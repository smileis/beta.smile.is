<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><!-- Main bar -->
<div class="mainbar">

    <!-- Page heading -->
    <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left"><i class="fa fa-file-o"></i> <?php echo $this->scope["pagename"];?></h2>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
            <a href="<?php echo $this->scope["base_url"];?>admin_home"><i class="fa fa-home"></i> Dashboard</a>
            <!-- Divider -->
            <span class="divider">/</span>
            <a href="<?php echo $this->scope["base_url"];
echo $this->scope["uri_string"];?>" class="bread-current"><?php echo $this->scope["pagename"];?></a>
        </div>

        <div class="clearfix"></div>

    </div>
    <!-- Page heading ends -->



    <!-- Matter -->

    <div class="matter">
        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    <div class="widget wgreen">

                        <div class="widget-head">
                            <div class="pull-left"><?php echo $this->scope["pagename"];?></div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">
                            <div class="padd">

                                <br />
                                <?php echo $this->scope["output_output"];?>

                            </div>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <!-- Matter ends -->

</div>

<!-- Mainbar ends -->
<div class="clearfix"></div>

</div>
<!-- Content ends -->

<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>