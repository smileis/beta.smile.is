<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?>
<div class="admin-form">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Widget starts -->
                <div class="widget wred">
                    <div class="widget-head">
                        <i class="fa fa-lock"></i> Register
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <form class="form-horizontal" action="<?php echo $this->scope["base_url"];?>admin_register_user" method="post">

                            <!-- <form class="form-horizontal"> -->
                            <!-- Registration form starts -->
                                <!-- Error -->
                                <?php if ((isset($this->scope["message"]) ? $this->scope["message"] : null)) {
?>
                                    <div class="alert alert-warning">
                                        <?php echo $this->scope["message"];?>

                                    </div>
                                <?php 
}?>

                            <!-- F Name -->
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="first_name">First Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                            </div>
                            <!-- L Name -->
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="last_name">Last Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                            </div>
                            <!-- Email -->
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="email">Email</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <!-- Select box -->
                            <!-- <div class="form-group">
                              <label class="control-label col-lg-3">Drop Down</label>
                              <div class="col-lg-4">
                                  <select class="form-control">
                                  <option>&nbsp;</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  </select>
                              </div>
                            </div>      -->
                            <!-- Username -->
                            <!-- <div class="form-group">
                              <label class="control-label col-lg-3" for="username">Username</label>
                              <div class="col-lg-9">
                                <input type="text" class="form-control" id="username">
                              </div>
                            </div> -->
                            <!-- Password -->
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="password">Password</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <!--Confirm Password -->
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="confirm-password">Confirm Password</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control" id="confirm-password" name="password_confirm">
                                </div>
                            </div>
                            <!-- Accept box and button s-->
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-3">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Accept Terms &amp; Conditions
                                        </label>
                                    </div>
                                    <br />
                                    <button type="submit" class="btn btn-sm btn-info">Register</button>
                                    <button type="reset" class="btn btn-sm btn-default">Reset</button>
                                </div>
                            </div>
                            </form>

                        </div>
                    </div>
                    <div class="widget-foot">
                        Already Registred? <a href="<?=base_url('admin_login');?>">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>