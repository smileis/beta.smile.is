<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><!-- Sidebar -->
<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <!--- Sidebar navigation -->
    <ul id="nav">
        <?php 
$_fh1_data = (isset($this->scope["navmenus"]) ? $this->scope["navmenus"] : null);
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['navmenu'])
	{
/* -- foreach start output */
?>
            <li class="<?php if ((isset($this->scope["topmenuname"]) ? $this->scope["topmenuname"] : null) == mb_strtolower((string) (isset($this->scope["navmenu"]["name"]) ? $this->scope["navmenu"]["name"]:null), $this->charset)) {
?> open <?php 
}?> <?php echo $this->scope["navmenu"]["class"];?>" id="<?php echo mb_strtolower((string) (isset($this->scope["navmenu"]["name"]) ? $this->scope["navmenu"]["name"]:null), $this->charset);?>">
                <a href="<?php echo $this->scope["navmenu"]["url"];?>">
                    <i class="fa fa-list-alt"></i> <?php echo $this->scope["navmenu"]["name"];?> <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
                </a>
                <?php 
$_fh0_data = (isset($this->scope["navmenu"]["submenu"]) ? $this->scope["navmenu"]["submenu"]:null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['submenus'])
	{
/* -- foreach start output */
?>
                    <ul>
                        <li>
                            <a href="<?php echo $this->scope["submenus"]["sub_url"];?>">
                                <?php echo $this->scope["submenus"]["sub_name"];?>

                            </a>
                        </li>
                    </ul>
                <?php 
/* -- foreach end output */
	}
}?>

            </li>
        <?php 
/* -- foreach end output */
	}
}?>

    </ul>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>