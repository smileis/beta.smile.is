<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>
  <meta name="description" content="manage your contacts with name, email and phone">
  <meta name="author" content="Admin">
  <link href="<?=base_url('assets/admin/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/admin/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/admin/css/style.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Le fav and touch icons -->
  <link href="<?=base_url('assets/ico/favicon.ico'); ?>" rel="shortcut icon">
</head><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>