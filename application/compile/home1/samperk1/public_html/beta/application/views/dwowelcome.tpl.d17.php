<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ;
'';// checking for modification in file:/home1/samperk1/public_html/beta/application/views/page.tpl
if (!("1413127917" == filemtime('/home1/samperk1/public_html/beta/application/views/page.tpl'))) { ob_end_clean(); return false; };?><html>
<head>
    <title>    Welcome to Dwoo-ed CodeIgniter</title>

    <style type="text/css">

        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 14px;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        code {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        pre {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #B9BAA4;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }
    </style>
</head>
<body>
    <h1>Welcome to Dwoo-ed CodeIgniter!</h1>

    <p>The page you are looking at is being generated dynamically by <b>CodeIgniter</b> in combination with the 'Smarty-killer' <b>Dwoo</b> template engine.
        The page is rendered at <?php echo $this->scope["itshowlate"];?> by the Dwoo_compiler.</p>

    <p>If you would like to edit this page you'll find it located at:</p>
    <code>application/views/dwoowelcome.tpl</code>

    <p>The corresponding controller for this page is found at:</p>
    <code>application/controllers/dwoowelcome.php</code>

    <p>The library for Dwoo integration can be found at:</p>
    <code>application/libraries/Dwootemplate.php</code>

    <p>If you are exploring Dwoo for the very first time, you should start by reading the <?php echo anchor('http://dwoo.org/', 'Dwoo website');?>.</p>
    <p>If you are exploring CodeIgniter for the very first time, you should start by reading the <?php echo anchor('http://codeigniter.com/user_guide/', 'User Guide');?>.</p>

    <pre>
<b>Usage</b>:
$this->load->library('Dwootemplate');
$this->dwootemplate->assign('test', 'test');
$this->dwootemplate->display('dwoowelcome.tpl');
</pre>
</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>