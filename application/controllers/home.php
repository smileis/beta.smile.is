<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends Main_Controller {

	function __construct() {
	        parent::__construct();
	        $this->load->library('authentication/auth');
	        $this->load->library('session');
	        $this->load->library('form_validation');
	
	        // Load MongoDB library instead of native db driver if required
	        $this->config->item('use_mongodb', 'auth') ?
	                        $this->load->library('mongo_db') :
	                        $this->load->database();
	
	        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'), $this->config->item('error_end_delimiter', 'auth'));
	}
	    
	public function index() {
		//demo test
		$this -> load -> view('include/header');
		$this -> load -> view('frontend/login');
		$this -> load -> view('include/footer');
	}

	public function login() {
		
	}

}
