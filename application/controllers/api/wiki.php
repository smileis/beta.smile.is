<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wiki extends Main_Controller {
    public function index() {

    }

    public function json_decode_nice($json, $assoc = FALSE){
        $json = str_replace(array("\n","\r"),"",$json);
        $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
        return json_decode($json,$assoc);
    }

    public function get($text,$section){
        // action=parse: get parsed text
        // page=Baseball: from the page Baseball
        // format=json: in json format
        // prop=text: send the text content of the article
        // section=0: top content of the page

        $url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles='.$text.'&rvsection='.$section.'&rawcontinue';
        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        $c = curl_exec($ch);
        curl_close($ch);
        $json = $c;

        $json = json_decode($json, true);
        //echo $json;
        foreach ($json as $key => $value) {
            //echo "Key: $key ===> Value: ";
            //var_dump($value);
            foreach ($value as $key1 => $value1) {
                //echo "Key: $key1 ===> Value: ";
                //var_dump($value1);
                if($key1 == 'pages'){
                    foreach ($value1 as $key2 => $value2) {
                        //echo "Key: $key1 ===> Value: ";
                        //var_dump($value2);
                        foreach ($value2 as $key3 => $value3) {
                            //echo "Key: $key1 ===> Value: ";
                            //var_dump($value3);
                            if($key3=='revisions'){
                                foreach ($value3 as $key4 => $value4) {
                                    //echo "Key: $key1 ===> Value: ";
                                    //var_dump($value4);
                                    foreach ($value4 as $key5 => $value5) {
                                        //echo "Key: $key1 ===> Value: ";
                                        //var_dump($value4);
                                        if($key5=='*'){
                                            //var_dump($value5);
                                            //echo " <br/>-----------===5============----------<br/>";

                                            $open = 0;
                                            $skipcount = 0;
                                            $close = 0;
                                            $data = array();
                                            $data_builder = "";
                                            for($i=0;$i<strlen($value5);$i++){
                                                if($i == strlen($value5)-1) break;

                                                if($open == 1 && $close == 0){
                                                    $data_builder .= $value5[$i];
                                                    $curly_srch = $value5[$i].$value5[$i+1];
                                                    if($curly_srch == '{{') $skipcount++;
                                                    if($curly_srch == '}}' && $close == 0) {
                                                        if($skipcount>0) {
                                                            $skipcount--;
                                                            continue;
                                                        }
                                                        $close = 1;
                                                        $open = 0;
                                                        array_push($data,$data_builder);
                                                        $data_builder = "";
                                                    }
                                                } else {

                                                    $curly_srch = $value5[$i].$value5[$i+1];
                                                    if($curly_srch == '{{' && $open == 0) {
                                                        $open = 1;
                                                        $close = 0;
                                                    }
                                                }

                                            }
                                            $op = print_r($data,true);
                                            echo $op;
                                            //$content_arr = explode("{{",$value5);
                                            //preg_match_all('/{{(.*?)}}/', $value5, $matches);
                                            //$content_arr = str_replace("{{","<-",$value5);
                                            //$content_arr2 = str_replace("}}","->",$content_arr);
                                            //preg_match_all('/\{(.+)\{(.+)\}\}/', $value5, $matches);
                                            //var_dump($content_arr2);
                                            //print_r(array_map('intval',$matches[1]));
                                        }
                                        //echo "\n -----------===5============----------\n";
                                    }
                                    //echo "\n -----------===4============----------\n";
                                }
                            }
                            //echo "\n -----------===3============----------\n";
                        }
                        //echo "\n -----------===3============----------\n";
                    }
                }
                //echo "\n -----------===2============----------\n";
            }
            //echo "\n -----------====1===========----------\n";
        }
        //echo $json[0]['normalized'][0]['from'];
        /* $content = $json->{'parse'}->{'text'}->{'*'}; // get the main text content of the query (it's parsed HTML)

        // pattern for first match of a paragraph
        $pattern = '#<p>(.*?)</p>#s'; // http://www.phpbuilder.com/board/showthread.php?t=10352690
        if(preg_match_all($pattern, $content, $matches))
        {
            // print $matches[0]; // content of the first paragraph (including wrapping <p> tag)
            print strip_tags(implode("\n\n",$matches[1])); // Content of the first paragraph without the HTML tags.
        } */
    }


}