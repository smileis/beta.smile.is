<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Sachin Rajput
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

require APPPATH.'/libraries/google/contrib/Google_YouTubeService.php';
require APPPATH.'/libraries/google/Google_Client.php';

//http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue
class Demoapi extends REST_Controller
{
    function login(){
        $url = 'demo.storyof.ceo/api/example/user/id/2/format/json';

        $ch = curl_init($url);
        /* $xml_data = '<?xml version="1.0" encoding="UTF-8" ?><SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>'; */
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_GET, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);

        $json = json_decode($c);

        if($json)
        {
            $this->response($json, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }

    public function manualsession_get($sessionid){
        $this->session->set_userdata('session_id',$sessionid);
        echo $this->session->all_userdata();
    }

    public function ff_get(){
        $DEVELOPER_KEY = 'AIzaSyDH5I4BLSX9Dy5rAKKc1d1hXCXR5B9CV2s';

        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);

        $youtube = new Google_YoutubeService($client);


        $htmlBody ="";

        try {
            $searchResponse = $youtube->search->listSearch('id,snippet', array(
                'q' => "rupert+murdoch",
                'maxResults' => 10,
            ));

            $videos = array();
            $channels = '';

            foreach ($searchResponse['items'] as $searchResult) {
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':
                        $videodetail = array();
                        $videodetail['title'] = $searchResult['snippet']['title'];
                        $videodetail['link'] = "http://www.youtube.com/watch?v=".$searchResult['id']['videoId'];
                        array_push($videos,$videodetail);
                        //$videos .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                            //$searchResult['id']['videoId']."<a href=http://www.youtube.com/watch?v=".$searchResult['id']['videoId']." target=_blank>   Watch This Video</a>");
                        break;
                    case 'youtube#channel':
                        $channels .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                            $searchResult['id']['channelId']);
                        break;
                }
            }

        } catch (Google_ServiceException $e) {
            $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        } catch (Google_Exception $e) {
            $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        }
        var_dump($videos);
        //echo "sd";
    }

    function user_get()
    {
        /* if(!$this->get('firstname') || !$this->get('lastname') || !$this->get('sessionid'))
        {
            $this->response(NULL, 400);
        } */


        //$URL = "http://api.beta.dowjones.com/api/1.0/session/";

        $url = 'demo.storyof.ceo/api/example/user/id/2/format/json';

        $ch = curl_init($url);
        /* $xml_data = '<?xml version="1.0" encoding="UTF-8" ?><SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>'; */
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_GET, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);

        $json = json_decode($c);

        if($json)
        {
            $this->response($json, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }

    function youtube_get()
    {
        /* if(!$this->get('firstname') || !$this->get('lastname') || !$this->get('sessionid'))
        {
            $this->response(NULL, 400);
        } */


        //$URL = "http://api.beta.dowjones.com/api/1.0/session/";

        $url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=rupert murdoch&key=AIzaSyDH5I4BLSX9Dy5rAKKc1d1hXCXR5B9CV2s';

        $ch = curl_init($url);
        /* $xml_data = '<?xml version="1.0" encoding="UTF-8" ?><SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>'; */
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));//application/json'));
        //curl_setopt($ch, CURLOPT_GET, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);

        //$xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($c);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }
    }


    function uu_get()
    {

        $url = 'demo.storyof.ceo/api/example/user';///id/1/format/json';

        $params = array("id"=>1);
        $options = array(CURLOPT_USERAGENT=>"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2",
            CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));

        $json = $this->curl->_simple_call('get',$url,$params,$options);

        if($json)
        {
            $this->response($json, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
//        $this->load->spark('restclient/2.1.0');
//
//        $this->load->library('rest', array(
//            'server' => 'http://demo.storyof.ceo/api/example/',
//            //'http_user' => 'admin',
//            //'http_pass' => '1234',
//            //'http_auth' => '' // or 'digest'
//        ));
//
//        $user = $this->rest->get('user', array('id' => $id), 'json');
//
//        echo $user->name;
    }
}