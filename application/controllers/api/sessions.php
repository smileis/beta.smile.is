<?php
if (!defined('BASEPATH'))
    die();
class Sessions extends Main_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');
        $this->load->model($this->config->item('layouts_variable_model', 'uiconfig'),'var_model');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }

    public function manual(){
        $this->session->set_userdata('djsessionid','27140XxX_JUYTIMRYGQYTANRSHAXTEQSXG5GHUNTYO5REEU2NJZZUE4TEMZJFMZKLNB2E42LDGBBGUL2NOR4FUV3FIJBES5SPNE4DEM3IJZHGIOLQHA2DQ6SZM5STQZDVI5ZGSTLKO5VTOZKIINWDMZLDJ5RGQTTXJJATQL3LKZSGUMRUORLUO2TPJB2XA5BSJBDFKZZPOZXW26SVOUVXQ3DNIVKGGVSGOVYDMRRPMVTDIZRSPJBUIWBWK5FWKWLSKFNG44LLGRZW2UDTK4YVERKKPJJE6TCNLJ3GIU3VMEZFSPKH');
        var_dump($this->session->all_userdata());
    }

    function login()
    {
        if ($this->auth->logged_in()) {
            $djseesion_chk = $this->session->userdata('djsessionid');

            if($djseesion_chk !=null){
                redirect('home', 'refresh');
            }

            $checkIndustryFlag = $this->checkIndustryFlag();
			$xmldata = $this->loadDJDetails();
			
			$url = 'http://api.beta.dowjones.com/api/1.0/session/';
			$xml = $this->curlRequest($url,'POST',$xmldata);
            
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
			
            $session_id = $array['SessionId'];
            $this->session->set_userdata('djsessionid',$session_id);

			$url = 'http://suggest.factiva.com/authenticate/1.0/registerUsingSessionId?format=xml&sid='.$session_id;
			
			$xml = $this->curlRequest($url,'GET');
			$json = json_encode($xml);
            $array = json_decode($json,TRUE);
			$this->session->set_userdata('djsuggestcontext',$array[0]);
			
			if($checkIndustryFlag<1){
				//normal user load interest page first 
				
				redirect('interests', 'refresh');
			}
            
			
			//var_dump($array);
			//echo $array[0];
            //echo "test";
			//die();
            //var_dump($this->session->all_userdata());
            redirect('home', 'refresh');
        } else{
            //not allowed
            redirect('login', 'refresh');
        }
    }

    public function logout(){
        $session_id = $this->session->userdata('djsessionid');
        //IAL_JUYTIMRXHEZDEMRSGQXTQOLSINHSWZLBFNJU63SIINBUYTLPOV5GINTDNBKHA4LPMJRDI5KOGBJTIR3IJVATM6KMF5SFSUZSIJLHEN3PINRGKV2EJU3UCSKRLFWE4VTKGFNE232YLJZXANTFORJHIOLKJRWWSZSGJQ4E4YJPMVGFCNL2IVWFEWC2LA3WI4DBM5HXE6CNIYVSW5DLGFCUEZTYFM4TM2BPGBMXGMKSK5DUYZSXNRDXQTKRFMXXKRTUOBTT2PKH
        $url = 'http://api.beta.dowjones.com/api/1.0/session/xml?SessionId='.$session_id;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);

        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));

        $c = curl_exec($ch);
        curl_close($ch);
        redirect('login', 'refresh');
        //$message = array('id' => $session_id, 'message' => 'Logged out!');
        //var_dump($message);
        //$this->response($message, 200); // 200 being the HTTP response code
    }

    public function loadDJDetails(){
        //$username = $this->session->userdata('username');
        $this->db->where('username', 'sachin_rajput');
        $this->db->where('status', 1);
        $query = $this->db->get('DJ_APIConfig');
        $result = $query->result();
        $djdetails = array();
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $djdetails['djusername'] = $row->djusername;
                $djdetails['djpassword'] = $row->djpassword;
                $djdetails['djemail'] = $row->djemail;
                $djdetails['format'] = $row->format;
            }
            return $this->generateXML($djdetails);
        }
        return null;
    }
	
	public function checkIndustryFlag(){
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $query = $this->db->get('users_industry');
        $result = $query->result();
        $ind_flag = 0;
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $ind_flag = $row->industryflag;
            }
            return $ind_flag;
        }
        return 0;
    }

    public function generateXML($djdetails){
        return '<SessionRequest><Email>'.$djdetails['djemail'].'</Email><Password>'.$djdetails['djpassword'].'</Password><Format>'.$djdetails['format'].'</Format></SessionRequest>';
    }

    function slogin()
    {
        $url = 'http://api.beta.dowjones.com/api/1.0/session/';
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        $xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //print $xml;
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        //var_dump($array);
        //SessionId
        $session_id = $array['SessionId'];
        $this->session->set_userdata('djsessionid',$session_id);
        //var_dump($this->session->all_userdata());
        //redirect('home', 'refresh');
        $this->print_session();
    }

    public function slogout(){
        $session_id = $this->session->userdata('djsessionid');
        //IAL_JUYTIMRXHEZDEMRSGQXTQOLSINHSWZLBFNJU63SIINBUYTLPOV5GINTDNBKHA4LPMJRDI5KOGBJTIR3IJVATM6KMF5SFSUZSIJLHEN3PINRGKV2EJU3UCSKRLFWE4VTKGFNE232YLJZXANTFORJHIOLKJRWWSZSGJQ4E4YJPMVGFCNL2IVWFEWC2LA3WI4DBM5HXE6CNIYVSW5DLGFCUEZTYFM4TM2BPGBMXGMKSK5DUYZSXNRDXQTKRFMXXKRTUOBTT2PKH
        $url = 'http://api.beta.dowjones.com/api/1.0/session/xml?SessionId='.$session_id;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);

        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));

        $c = curl_exec($ch);
        curl_close($ch);
        //redirect('login', 'refresh');
        //$message = array('id' => $session_id, 'message' => 'Logged out!');
        //var_dump($message);
        //$this->response($message, 200); // 200 being the HTTP response code
    }

    public function print_session(){
        var_dump($this->session->all_userdata());
    }
	
	public function curlRequest($url,$type,$xml_data=null){
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		if($type == 'POST'){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
		}
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        return $xml;
    }
}

?>