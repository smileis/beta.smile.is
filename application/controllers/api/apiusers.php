<?php
if (!defined('BASEPATH'))
    die();



class ApiUsers extends Main_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'), 'menu');
        $this->load->model($this->config->item('sql_model', 'uiconfig'), 'sql_model');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }
	
	public function demo_call(){
		$session_id = $this->session->userdata('sessionid');
		$params = array();
		$params['sessionid'] = $session_id;
		$params['Parts'] = 'All';
		$params['Id'] = 76120558;
		$url = 'http://api.beta.dowjones.com/api/1.0/Executives/';
		$format = 'xml';
		
		$return = $this->rest_model->get($url.$format,$params,'xml');
		/* "http://api.beta.dowjones.com/api/1.0/Executives/xml?Id=76120558&Parts=All&sessionid=27138XxX_JUYTIMRZGIYDENRSHAXXCK2QIRKEGMLIFNHGWMKNM43VEMTEJN2XKS2BMZAWGOJWPE3UGQ2YJRATSVKDII2WOVDMHBCGYVSTGNZDKN3EMZBTQ22EMVUHO2LBIUYE642PINYTASTVLBJGOZDXIFNE4QKTKRZVATLWOJVTMTZWGU3DKU3BMIYVSM2QPJFHIOJSNNYFS4ZSJI2GOWDIGQXTMURZOBTWWMBSI53XSUCBKNZFMRTRNZVUEZRLN42G222MKJLFSMTZLBSWUYZUMRIVONLMGVDVQUSZLEXVCPKH" */
		
		//$xml = simplexml_load_string($return);
		var_dump($return);
	}

    public function saveexec(){
        $session_id = $this->session->userdata('sessionid');
        $execs = $this->input->post('execs');

        $exec_arr = explode(",",$execs);
        $count = 0;
        foreach($exec_arr as $exec){
            $execdet = explode("-",$exec);
            //echo $execdet[0].':'.$execdet[1];
            $exec_fullname = $execdet[0];

            $exec_fullnames = explode(" ",$exec_fullname);
            $exec_fname = lcfirst($exec_fullnames[0]); //db
            $exec_lname = lcfirst($exec_fullnames[1]); //db
            $exec_username = $exec_fname.'_'.$exec_lname; //db

            $exec_wikilink = "";
            $wiki_pagename = "";

            if(count($execdet)>1){
                $exec_wikilink = $execdet[1]; //db
                $wikiurl_dump = explode("/",$exec_wikilink);
                $url_len = count($wikiurl_dump);
                $wiki_pagename = $wikiurl_dump[$url_len-1]; //db
            }


            $url = 'http://api.beta.dowjones.com/api/1.0/executives/search/xml?firstname='.$exec_fname.'&lastname='.$exec_lname.'&sessionid='.$session_id;
            //echo $url;
            //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
            $ch = curl_init($url);
            //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
            curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $c = curl_exec($ch);
            curl_close($ch);

            //$json = json_decode($c);
            //print $c;
            $xml = simplexml_load_string($c);
            //print_r($xml);
            $profiles = $xml->Executives->ExecutiveProfile;
            //echo $xml->Records;

            foreach ($profiles as $profile)
            {
                //echo $profile->Executive->FCode.'<br/>';
                if(($exec_fname == lcfirst($profile->Executive->FirstName)) && ($exec_lname == lcfirst($profile->Executive->LastName))){

                    $session_id = $this->session->userdata('sessionid');
                    $exec_fcode = $profile->Executive->FCode;
                    //insert in db

                    $format = 'DATE_ATOM';//$this->config->time_format1;
                    $time = standard_date($format, time());

                    $data = array(
                        'FCode' =>  $exec_fcode,
                        'wikilink' => $exec_wikilink ,
                        'site_username' => $exec_username ,
                        'wiki_pagename' => $wiki_pagename ,
                        'fname' => $exec_fname ,
                        'lname' => $exec_lname ,
                        'lastsyncdate' => $time,
                        'createdate' => $time
                    );

                    $this->db->insert('ExecutiveKeys', $data);
                    $count++;
                    /* $parts = 'All';
                    //Biography
                    //echo "ss".$fname.$lname;

                    //die();
                    $url = 'http://api.beta.dowjones.com/api/1.0/Executives/xml?Id='.$id.'&Parts='.$parts.'&SessionId='.$session_id;
                    //echo $url;
                    //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
                    $ch = curl_init($url);
                    //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
                    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
                    //curl_setopt($ch, CURLOPT_POST, 1);
                    //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $c = curl_exec($ch);
                    curl_close($ch);

                    //$json = json_decode($c);
                    //print $c;
                    $xml = simplexml_load_string($c);
                    var_dump($xml); */
                    break;
                }
            }

        }
        if($count>0)
            echo "Successfully inserted ".$count." executive profile.<br/>";
        else
            echo "Zero executive profile found.";

        echo "<br/> <a href=\"".base_url()."admin/manage/managewidget/userexec\">Click here to return to adding execs</a>";

        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        //$json = json_encode($xml);
        //$array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);

//        $records = $array['Records'];

//        for($i=0;$i<$records;$i++){
//
//        }

//        echo $array['Executives']['ExecutiveProfile'][0]['Executive']['FCode'];
//        echo "<br/><br/><br/><br/>";
//        var_dump($array);

    }


}