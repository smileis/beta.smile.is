<?php
if (!defined('BASEPATH'))
    die();
class Ajax extends Main_Controller
{
    /***
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'), 'menu');
        //$this->load->model($this->config->item('ajax', 'uiconfig'), 'ajax');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }

    /***
     * @param $className
     * @param $pageName
     */
    public function loadPageClass($className,$methodName){
        /**
         * Call the method defined in the model of $className
         */
		$classPath = $this->config->item($className,'uiconfig');
		$clsObj = explode("/",$classPath);
		
        $this->load->library($classPath);
		$clsName = "";
		if(count($clsObj) > 1)
			$clsName = $clsObj[count($clsObj)-1];
		else
			$clsName = $clsObj[0];
        call_user_func_array(array($this->$clsName,$methodName),array());
    }

    /***
     * @param $className
     * @param $pageName
     * @param $parameters
     */
    public function loadPageClassParameters($className,$methodName,$parameters){
        /**
         * Call the function defined in the model of $className with $parameters
         */
        $this->load->library($this->config->item($className, 'uiconfig'), 'ajax');
        call_user_func_array(array($this->ajax,$methodName),array());
    }

    /***
     * @param $pageName
     */
    public function loadPageMethod($methodName){
        /**
         * Call the function defined in the model of Ajax
         */
        $this->load->model($this->config->item('ajax', 'uiconfig'), 'ajax');
        call_user_func_array(array($this->ajax,$methodName),array());
    }

    /***
     * @param $pageName
     * @param $parameters
     */
    public function loadPageMethodParameters($pageName,$parameters){
        /**
         * Call the function defined in the model of Ajax with $parameters
         */
        $this->load->model($this->config->item('ajax', 'uiconfig'), 'ajax');
        call_user_func_array(array($this->ajax,$pageName),array());
    }

}