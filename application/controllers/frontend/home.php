<?php
if (!defined('BASEPATH'))
    die();
class Home extends Main_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }

    public function index() {
        if ($this->auth->logged_in()) {
            //redirect them to the login page
            redirect('home', 'refresh');
        }else{
            //not allowed
            redirect('login', 'refresh');
        }
    }
	
	public function error() {
		$this->dwootemplate->assign('main_body','frontend/error.tpl');
		//use redirects instead of loading views for compatibility with MY_Controller libraries
		$this->dwootemplate->display('template.tpl');
	}
	
    //log the user in
    public function login() {
        //redirect them to the home page if loggedin
        if ($this->auth->logged_in())
            redirect('home', 'refresh');

        $this->dwootemplate->assign('title', "Demo - Storyof.CEO - Login");
        //$this->data['title'] = "Login";
        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page

                $this->session->set_flashdata('message', $this->auth->messages());
                redirect('logindjapi', 'refresh');
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                //$this->session->set_flashdata('message', $this->auth->errors());

                //$this->config->set_item('language','arabic');
                //echo $this->config->item('language');

                $this->dwootemplate->assign('message',$this->lang->language['login_error']);
                $this->dwootemplate->assign('main_body','landing/home.tpl');
                //use redirects instead of loading views for compatibility with MY_Controller libraries
                $this->dwootemplate->display('frontend/template.tpl');

                //$this->load->view('template',$this->data);
            }
        } else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'class' => 'input-block-level',
                'placeholder' => 'Email address',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'class' => 'input-block-level',
                'placeholder' => 'Password',
                'type' => 'password',
            );
            $this->data['error_boolean'] = 'style="display:none;"';
            //$this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
            //$this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
            //$this->data['main_body'] = 'admin/login.php';
            $this->dwootemplate->assign('main_body','landing/login.tpl');

            $this->dwootemplate->display('frontend/template.tpl');

            //$this->load->view('template',$this->data);

        }
    }

    function logout() {
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $this->auth->messages());
        redirect('logoutdjapi', 'refresh');
    }

    function register() {
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));
        $this->dwootemplate->assign('message',$this->lang->language['login_subheading']);
        $this->dwootemplate->assign('title', "Smile.is Register");

        $this->dwootemplate->assign('main_body','landing/register.tpl');
        $this->dwootemplate->display('frontend/template.tpl');
    }

    public function FUIhome(){
        if ($this->auth->logged_in()) {
            //redirect them to the home page because they must be an administrator to view this

            $this->dwootemplate->assign('topmenuname','dashboard');
            $this->dwootemplate->assign('pagename','Dashboard');

            $this->dwootemplate->assign('title', "Smile.is - Home");
            $this->dwootemplate->assign('modal_id',123);
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('main_body','landing/home.tpl');
            $this->dwootemplate->display('frontend/template.tpl');

        } else{
            //not allowed
            redirect('login', 'refresh');
        }
    }

    function register_user() {
        $this->data['title'] = "Create User";

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'auth') . ']|max_length[' . $this->config->item('max_password_length', 'auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->input->post('first_name')) . '_' . strtolower($this->input->post('last_name'));
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
            );
        }
        if ($this->form_validation->run() == true && $this->auth->register($username, $password, $email, $additional_data)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->auth->messages());

            /* $this->data['message_element'] = 'auth/login';
            $this->data['message'] = 'Please check '.$email.' for activation link, click on the activation link to activate your account.';
            $this->load->view('template',$this->data); */
            redirect('login', 'refresh');
        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));

            
            $this->dwootemplate->assign('message',$this->data['message']);
            $this->dwootemplate->assign('title', "Smile.is Register");

            $this->dwootemplate->assign('main_body','landing/register.tpl');
            $this->dwootemplate->display('frontend/template.tpl');
        }
    }
	
	//activate the user
    function activate($id, $code = false) {
        if ($code !== false) {
            $activation = $this->auth->activate($id, $code);
        } else if ($this->auth->is_admin()) {
            $activation = $this->auth->activate($id);
        }

        if ($activation) {
            //redirect them to the interest page
            $this->session->set_flashdata('message', $this->auth->messages());
            redirect("interests", 'refresh');
        } else {
            //redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->auth->errors());
            redirect("auth/forgot_password", 'refresh');
        }
    }
	
	//deactivate the user
    function deactivate($id = NULL) {
        $id = $this->config->item('use_mongodb', 'auth') ? (string) $id : (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', 'confirmation', 'required');
        $this->form_validation->set_rules('id', 'user ID', 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->auth->user($id)->row();

            $this->load->view('auth/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    show_error('This form post did not pass our security checks.');
                }

                // do we have the right userlevel?
                if ($this->auth->logged_in() && $this->auth->is_admin()) {
                    $this->auth->deactivate($id);
                }
            }

            //redirect them back to the auth page
            redirect('auth', 'refresh');
        }
    }
}
?>