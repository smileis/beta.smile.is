<?php 
class my404 extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    
		
    } 

    public function index() 
    { 
        $this->dwootemplate->assign('main_body','frontend/error.tpl');
		//use redirects instead of loading views for compatibility with MY_Controller libraries
		$this->dwootemplate->display('template.tpl');
    } 
} 
?> 