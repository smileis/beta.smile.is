<?php
if (!defined('BASEPATH'))
    die();


require APPPATH.'/libraries/simple_html_dom.php';

require APPPATH.'/libraries/google/contrib/Google_YouTubeService.php';
require APPPATH.'/libraries/google/Google_Client.php';

class Pages extends Main_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');

        $this->load->model($this->config->item('layouts_variable_model', 'uiconfig'),'var_model');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));

        $this->load->library('HybridAuthLib');
    }

    public function index() {
        if (!$this->auth->logged_in()) {
            //redirect them to the login page
            redirect('admin_login', 'refresh');
        } elseif ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            redirect('admin_home', 'refresh');
        } else{
            //not allowed
            $this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
            $this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
            $this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
            $this->load->view('template',$this->data);
        }
    }

    public function loadExec($val){
        $session_id = $this->session->userdata('djsessionid');
        $exec_arr = explode("_",$val);//$this->input->post('execs');

        //$exec_arr = explode(",",$execs);
        $count = 0;
        //foreach($exec_arr as $exec){
            //$execdet = explode("-",$exec);
            //echo $execdet[0].':'.$execdet[1];
            //$exec_fullname = $execdet[0];

            $exec_fullnames = $exec_arr[0].' '.$exec_arr[1];
            $exec_fname = lcfirst($exec_arr[0]); //db
            $exec_lname = lcfirst($exec_arr[1]); //db
            $exec_username = $exec_fname.'_'.$exec_lname; //db

            $exec_wikilink = "";
            $wiki_pagename = "";




            $url = 'http://api.beta.dowjones.com/api/1.0/executives/search/xml?firstname='.$exec_fname.'&lastname='.$exec_lname.'&sessionid='.$session_id;
            //echo $url;
            //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
            $ch = curl_init($url);
            //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
            curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $c = curl_exec($ch);
            curl_close($ch);

            //$json = json_decode($c);
            //print $c;
            $xml = simplexml_load_string($c);
            //print_r($xml);
            $profiles = $xml->Executives->ExecutiveProfile;
            //echo $xml->Records;

            foreach ($profiles as $profile)
            {
                //echo $profile->Executive->FCode.'<br/>';
                if(($exec_fname == lcfirst($profile->Executive->FirstName)) && ($exec_lname == lcfirst($profile->Executive->LastName))){

                    $session_id = $this->session->userdata('sessionid');
                    $exec_fcode = $profile->Executive->FCode;
                    //insert in db

                    $format = 'DATE_ATOM';//$this->config->time_format1;
                    $time = standard_date($format, time());

                    $data = array(
                        'FCode' =>  $exec_fcode,
                        'wikilink' => $exec_wikilink ,
                        'site_username' => $exec_username ,
                        'wiki_pagename' => $wiki_pagename ,
                        'fname' => $exec_fname ,
                        'lname' => $exec_lname ,
                        'lastsyncdate' => $time,
                        'createdate' => $time
                    );

                    $this->db->insert('ExecutiveKeys', $data);
                    $count++;
                    /* $parts = 'All';
                    //Biography
                    //echo "ss".$fname.$lname;

                    //die();
                    $url = 'http://api.beta.dowjones.com/api/1.0/Executives/xml?Id='.$id.'&Parts='.$parts.'&SessionId='.$session_id;
                    //echo $url;
                    //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
                    $ch = curl_init($url);
                    //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
                    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
                    //curl_setopt($ch, CURLOPT_POST, 1);
                    //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $c = curl_exec($ch);
                    curl_close($ch);

                    //$json = json_decode($c);
                    //print $c;
                    $xml = simplexml_load_string($c);
                    var_dump($xml); */
                    break;
                }
            }

        //}
        return $count;

    }

    public function loadPage2Level($val1,$val2,$val3){
        if ($this->auth->logged_in()) {
            //echo $val1." ".$val2." ".$val3;
            $pageDetails = $this->getPageDetails($val1."/".$val2);
            //print_r($pageDetails);
            if(!$pageDetails) $pageDetails = $this->getPageDetails($val1);

            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $params = array("val1" => $val1,"val2" => $val2,"val3" => $val3 );
            //var_dump($params);
            //die();
            $this->assignVariablesParams($pageDetails,$params);
            $this->dwootemplate->assign('title', $this->var_model->getpageTitle());

            $pageid = $pageDetails->id;
            $cssArray = $this->loadCSS($pageid);
            $jsArray = $this->loadJS($pageid);

            $this->dwootemplate->assign('jsArray',$jsArray);
            $this->dwootemplate->assign('cssArray',$cssArray);
            $this->dwootemplate->assign('main_body',$pageDetails->templateurl);
            $this->dwootemplate->display('template.tpl');
            //$this->dwootemplate->display();

        } else{
            //not allowed
            redirect('login', 'refresh');
        }
    }

    public function loadPage2($val1,$val2){
        echo $val1." ".$val2;
    }

    public function loadPage($val){

        if ($this->auth->logged_in()) {
            // check if username is in the URL, if true redirect them to profile page
			
			$pageDetails = $this->getPageDetails($val);
			
			if($pageDetails){
				$this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
				$this->assignVariables($pageDetails);
				$this->dwootemplate->assign('title', $this->var_model->getpageTitle());
			}
			else{
				$user_profile = $this->checkUserProfile($val);

				$user_exec_profile = $this->checkExecUserProfile($val);



				if($user_exec_profile == false && $val != 'home'){
					//try to find in dj list and if exist then add it and load it
					$loadExec = $this->loadExec($val);
					if($loadExec > 0){
						$user_profile = $this->checkUserProfile($val);

						$user_exec_profile = $this->checkExecUserProfile($val);
					}
				}
				$execObj = null;
				if($user_profile == true || $user_exec_profile != null){

					$pageDetails = $this->getPageDetails('profile');
					$this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
					//load Exec Object
					//$execObj = $this->auth->getExecUserArray($user_exec_profile);
					$params = array(
						$user_exec_profile->FCode,
						'All|CompensationEx'
					);
					$execXML = $this->dowjonesapi->invokeAPI('execdetail',$params);
					$execObj = $this->dowjonesapi->getExecutiveArray($execXML);
					
					$pageDetails->pagename = $execObj['Executive']->FullName;

					//load news articles for this exec
					$params = array(
						'QueryString' => $execObj['Executive']->FirstName.'+'.$execObj['Executive']->LastName,
						'Records' => 20,
						'SearchResultPartList' => 'KeywordSummary|MetadataSuggestion|companySuggestion',
						'DuplicationMode' => 'off',
						'SearchMode' => 'all',
						'SourceGenre' => 'Publications'
					);
					
					$articleObj = $this->dowjonesapi->invokeAPI('articlesearch',$params,true);
					
					/* $params = array(
						'languageCode' => 'en',
						'searchText' => 'asbestos'
					);
					$industryObj = $this->dowjonesapi->invokeAPI('suggest_industry',$params,true);
					*/
					//var_dump($industryObj);die();
					//$this->apimodel->djarticlesearch($execObj['Executive']->FirstName.'+'.$execObj['Executive']->LastName,20);
					//var_dump($articleObj->Articles->Article->ArticleId);
					//var_dump($articleObj);
					//die();
					$imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName.'+'.$execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
					$execid = $execObj['Executive']->FCode;
					//.$execObj['Executive']->CompanyName.'+'
					if($imgUrl != null){
						$this->dwootemplate->assign('execprofimg', $imgUrl);
					}
					$this->dwootemplate->assign('title',$pageDetails->pagename);
					$this->dwootemplate->assign('execdetails', $execObj);
					$this->dwootemplate->assign('articles', $articleObj->Articles->Article);
					$this->dwootemplate->assign('keywords', $articleObj->SearchResultMetadata->KeywordSummary->DataBucket);
					if(isset($execObj['Colleagues']->Directors->Contact))
						$this->dwootemplate->assign('colleagues', $execObj['Colleagues']->Directors->Contact);
					$this->dwootemplate->assign('comp', $execObj['Compensation']);
					$this->dwootemplate->assign('compex', $execObj['CompensationEx']);
					//var_dump($execObj);
					//die();
					$this->dwootemplate->assign('newsarticle', $execObj['LatestNews']->NewsArticle);
					$json = json_encode($execObj['LatestNews']->NewsArticle);

					$date = array();

					//foreach($execObj['LatestNews']->NewsArticle as $item){
					foreach($articleObj->Articles->Article as $item){
							$headline = $item->Headline;
							$text = $item->Snippet;
							$time = $item->PubDateTime;
                            $articleID = $item->ArticleId;

							$itemarr = array(
								"startDate" => "$time",
								"headline" => "<a target='_blank' href='article/$execid/$articleID'>$headline</a>",
								"text" => "$text <br/><a target='_blank' href='pins/save/$execid-$articleID'>Pin it to home page</a>",
                                "classname" => "$articleID"
							);
							array_push($date,$itemarr);
					}

					/* $headline1 = $execObj['LatestNews']->NewsArticle[0]->Headline;
					$headline2 = $execObj['LatestNews']->NewsArticle[1]->Headline;

					$text1 = $execObj['LatestNews']->NewsArticle[0]->SourceArticleId;
					$text2 = $execObj['LatestNews']->NewsArticle[0]->SourceArticleId;
					$time1 = $execObj['LatestNews']->NewsArticle[0]->PubDateTime;
					$time2 = $execObj['LatestNews']->NewsArticle[1]->PubDateTime;

					$item1 = array(
						"startDate" => "$time1",
						"headline" => "$headline1",
						"text" => "$text1",
					);
					$item2 = array(
						"startDate" => "$time2",
						"headline" => "$headline2",
						"text" => "$text2",
					);
					$date = array($item1,$item2); */
					$timeline = array(
						"headline" => $pageDetails->pagename."\'s Timeline",
						"type" => "default",
						"date" => $date
					);
					$timelineObj = array(
						"timeline" => $timeline
					);
					$timeline_json = json_encode($timelineObj);
					//print_r($timeline_json);
					//die();
					/**
					 * <!-- {foreach from=$newsarticle item=foo}
					{$foo->ArticleId}<br/>
					{/foreach} -->
					 */
					$this->dwootemplate->assign('timelinejSONObj', $timeline_json);


					$twitterAuth = $this->hlogin('Twitter');
					$twitterusername = null;
					$display_name = (string)$execObj['Executive']->DisplayName;//.'+'.(string)$execObj['Executive']->CompanyName;
					//var_dump($display_name);die();
					if($twitterAuth == 1){
						$data['providers'] = $this->hybridauthlib->getProviders();
						foreach($data['providers'] as $provider=>$d) {
							if ($d['connected'] == 1) {
								$data['providers'][$provider]['user_profile'] = $this->hybridauthlib->authenticate($provider)->getUserProfile();
								$twitterusername = $this->hybridauthlib->authenticate($provider)->getUsersSearch($display_name);
								$getActivity = $this->hybridauthlib->authenticate($provider)->getAnyUserActivity($twitterusername);
							}
						}
					}
					$this->dwootemplate->assign('twitter_screenName', $twitterusername);
					$this->dwootemplate->assign('twitter_timeline', $getActivity);
					$videos = $this->get_youtube_videos($display_name);
					$this->dwootemplate->assign('youtube_videos', $videos);
					//echo $twitterAuth;
					//die();
					//print_r($json);
					//die();
				} 
			}
			

            //load js and css vars
            $pageid = $pageDetails->id;
            $cssArray = $this->loadCSS($pageid);
            $jsArray = $this->loadJS($pageid);

            $this->dwootemplate->assign('jsArray',$jsArray);
            $this->dwootemplate->assign('cssArray',$cssArray);
            $this->dwootemplate->assign('main_body',$pageDetails->templateurl);
            $this->dwootemplate->display('template.tpl');
            //$this->dwootemplate->display();

        } else{
            //not allowed
            redirect('login', 'refresh');
        }


    }

    public function get_youtube_videos($keyword) {
        $DEVELOPER_KEY = 'AIzaSyDH5I4BLSX9Dy5rAKKc1d1hXCXR5B9CV2s';

        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);

        $youtube = new Google_YoutubeService($client);


        $htmlBody ="";

        try {
            $searchResponse = $youtube->search->listSearch('id,snippet', array(
                'q' => $keyword,
                'maxResults' => 10,
            ));

            $videos = array();
            $channels = '';

            foreach ($searchResponse['items'] as $searchResult) {
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':
                        $videodetail = array();
                        $videodetail['title'] = $searchResult['snippet']['title'];
                        $videodetail['link'] = "http://www.youtube.com/embed/".$searchResult['id']['videoId'];
                        array_push($videos,$videodetail);
                        //$videos .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                        //$searchResult['id']['videoId']."<a href=http://www.youtube.com/watch?v=".$searchResult['id']['videoId']." target=_blank>   Watch This Video</a>");
                        break;
                    case 'youtube#channel':
                        $channels .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                            $searchResult['id']['channelId']);
                        break;
                }
            }

        } catch (Google_ServiceException $e) {
            $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        } catch (Google_Exception $e) {
            $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
                htmlspecialchars($e->getMessage()));
        }
        //var_dump($videos);
        return $videos;
    }

    public function youtube_no_auth($query)
    {
        $params['apikey'] = 'AIzaSyDH5I4BLSX9Dy5rAKKc1d1hXCXR5B9CV2s';

        $this->load->library('youtube', $params);
        $links = $this->youtube->getKeywordVideoFeed($query);
        var_dump($links);
        echo "<br/><br/>";
        $links_arr = explode("http://",$links);
        var_dump($links_arr);
        die();
    }

    public function getYoutubeLinks($query){
        //http://gdata.youtube.com/feeds/api/videos?alt=json&v=2&q=rupert%20murdoch&safeSearch=none&time=all_time&uploader=partner

        //to fetch all hyperlinks from a webpage
        $links = array();
//var_dump($hh);
        /* foreach($hh->find('a') as $a) {
         $links[] = $a->href;
        }
        print_r($links);  */

        //$url = "http://gdata.youtube.com/feeds/api/videos?alt=atom&v=2&q=".$query."&safeSearch=none&time=all_time&uploader=partner";
        $url = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=rupert murdoch&key=AIzaSyDH5I4BLSX9Dy5rAKKc1d1hXCXR5B9CV2s";
        //echo $url;
        //die();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ff = curl_exec($ch);

        //$xml = simplexml_load_file($url);

        //curl_close($ch);
        $xml = json_decode($ff);
        echo $xml;

        //echo "Sd";
        die();
        //if(count($links)>0) return $links[0];
        return null;
    }

    public function getProfileImgUrl($val){
        $val = str_replace(" ","+",$val);
        //to fetch all hyperlinks from a webpage
        $links = array();
//var_dump($hh);
        /* foreach($hh->find('a') as $a) {
         $links[] = $a->href;
        }
        print_r($links);  */
		//echo $val; die();
        $url = "https://www.google.com/search?q=".$val."+photo&rlz=1C5CHFA_enUS543US543&es_sm=91&source=lnms&tbm=isch&sa=X&ei=t1spVaSnCay1sQTar4CYBg&ved=0CAgQ_AUoAg&biw=1280&bih=705#q=".$val."+photo&tbm=isch&tbs=isz:m";
//$fp = fopen("example_homepage.txt", "w");
        $ch = curl_init();
//curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ff = curl_exec($ch);
        //var_dump($ff);

        $html = str_get_html($ff);

        foreach($html->find('img') as $a) {
            $links[] = $a->src;
        }
        curl_close($ch);
        //var_dump($ff);
        //print_r($links[0]);
        //die();
        if(count($links)>0) return $links[0];
        return null;
    }

    public function checkUserProfile($val){
        $this->db->where('username', $val);
        $query = $this->db->get('users');
        $result = $query->result();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                return true;
            }
        }
        return false;
    }

    public function checkExecUserProfile($val){
        $this->db->where('site_username', $val);
        $query = $this->db->get('ExecutiveKeys');
        $result = $query->result();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                return $row;
            }
        }
        return false;
    }

    //log the user in
    public function login() {
        //redirect them to the home page if loggedin
        if ($this->auth->logged_in())
            redirect('home', 'refresh');

        $this->dwootemplate->assign('title', "StoryOf.CEO - Login");

        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page

                $this->session->set_flashdata('message', $this->auth->messages());
                redirect('logindjapi', 'refresh');
                //redirect('home', 'refresh');
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                //$this->session->set_flashdata('message', $this->auth->errors());

                //$this->config->set_item('language','arabic');
                //echo $this->config->item('language');

                $this->dwootemplate->assign('message',$this->lang->language['login_error']);
                $this->dwootemplate->assign('main_body','frontend/login.tpl');
                //use redirects instead of loading views for compatibility with MY_Controller libraries
                $this->dwootemplate->display('template.tpl');

                //$this->load->view('template',$this->data);
            }
        } else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'class' => 'input-block-level',
                'placeholder' => 'Email address',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'class' => 'input-block-level',
                'placeholder' => 'Password',
                'type' => 'password',
            );
            $this->data['error_boolean'] = 'style="display:none;"';
            //$this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
            //$this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
            //$this->data['main_body'] = 'admin/login.php';
            $this->dwootemplate->assign('main_body','frontend/login.tpl');

            $this->dwootemplate->display('template.tpl');

            //$this->load->view('template',$this->data);

        }
    }

    function logout() {
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->auth->logout();

        //redirect them to the login page
        //$this->session->set_flashdata('message', $this->auth->messages());
        redirect('logoutdjapi', 'refresh');
        //redirect('login', 'refresh');
    }
	
	
	
	public function register(){
		if ($this->auth->logged_in())
            redirect('home', 'refresh');
		$this->dwootemplate->assign('title','Register - StoryOf.CEO');
		$this->dwootemplate->assign('main_body','frontend/register.tpl');
		$this->dwootemplate->display('template.tpl');
	}
	
	public function register_user() {
        $this->data['title'] = "Create User";

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'auth') . ']|max_length[' . $this->config->item('max_password_length', 'auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->input->post('first_name')) . '_' . strtolower($this->input->post('last_name'));
            $email = $this->input->post('email');
            $password = $this->input->post('password');
			
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
            );
        }
        if ($this->form_validation->run() == true && $this->auth->register($username, $password, $email, $additional_data)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->auth->messages());

            /* $this->data['message_element'] = 'auth/login';
            $this->data['message'] = 'Please check '.$email.' for activation link, click on the activation link to activate your account.';
            $this->load->view('template',$this->data); */
			$msg = 'Please check '.$email.' for activation link, click on the activation link to activate your account.';
			$this->dwootemplate->assign('message',$msg);
			$this->dwootemplate->assign('main_body','frontend/login.tpl');

            $this->dwootemplate->display('template.tpl');
            //redirect('login', 'refresh');
        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));
 
            /* $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );

            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );*/


            $this->dwootemplate->assign('message',$this->data['message']);
            $this->dwootemplate->assign('title', "Demo - StoryOf.CEO Register");

            $this->dwootemplate->assign('main_body','frontend/register.tpl');
            $this->dwootemplate->display('template.tpl');
        }
    }

    public function getPageDetails($val){
        $this->db->where('pageurl', $val);
        $query = $this->db->get('UI_Page');
        $result = $query->result();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                return $row;
            }
        }
        return null;
    }

    public function assignVariables($pageDetails){
        /* $pageVars = explode(',',$pageDetails->variables);

        foreach($pageVars as $variable){
            switch($variable){
                case 'modal_id':
                    $getid = 123; //$this->somelib->getModal_id();
                    $mod = 'modal_id';
                    $this->dwootemplate->assign($mod,$getid);
                    break;
                case 'modal_title':
                    $modal_title = 'demo ti1tle'; //$this->somelib->getModal_title();
                    $modt = 'modal_title';
                    $this->dwootemplate->assign($modt,$modal_title);
                    break;
                case 'userdetails':
                    $userdetails = call_user_func_array(array($this->auth,'getUserArray'),array());// $this->auth->getUserArray();
                    $this->dwootemplate->assign('userdetails',$userdetails);
                    break;
                case 'title':
                    $title = $pageDetails->pagename;
                    $this->dwootemplate->assign('title',$title);
                    break;
                case 'pagename':
                    $pagename = $pageDetails->pagename;
                    $this->dwootemplate->assign('pagename',$pagename);
                    break;
                case 'demotest':
                    //$pagename = $pageDetails->pagename;
                    $this->dwootemplate->assign('demotest','hello');
                    break;
            }
        } */

        // save method names using this : call_user_func_array we can call any method
        // i can keep appending the functions from front end to single class file
        // which will keep all functions for variables for any page.
        // add new widget to add variables with function names
        // also we can keep seperate class but for that addition need to be made in
        // code.


        // TODO: Add all this in initialize function

        $pageid = $pageDetails->id;
        $this->var_model->setthisObjDwoo($this->dwootemplate);
        $this->var_model->setpageTitle($pageDetails->pagename);
        $this->var_model->setpageID($pageid);



        $this->db->where('id', $pageid);
        $query = $this->db->get('UI_Page');
        $result = $query->result();

        //print_r($this->db->last_query());
        //die();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $varsArray = explode(',',$row->variables);
                
				if($varsArray[0] != ""){
					foreach($varsArray as $varid => $varID){
						$funcName = $this->var_model->getVarFuncName($varID);
						call_user_func_array(array($this->var_model,$funcName),array());
					}
				}
                //$variableValue =
                //$this->dwootemplate->assign($row->name,$variableValue);
            }
        }
    }

    /**
     * @param $pageDetails
     * @param $params
     */
    public function assignVariablesParams($pageDetails,$params){


        // save method names using this : call_user_func_array we can call any method
        // i can keep appending the functions from front end to single class file
        // which will keep all functions for variables for any page.
        // add new widget to add variables with function names
        // also we can keep seperate class but for that addition need to be made in
        // code.

        //var_dump($params);
        // TODO: Add all this in initialize function

        $pageid = $pageDetails->id;
        $this->var_model->setthisObjDwoo($this->dwootemplate);
        $this->var_model->setpageTitle($pageDetails->pagename);
        $this->var_model->setpageID($pageid);



        $this->db->where('id', $pageid);
        $query = $this->db->get('UI_Page');
        $result = $query->result();

        //print_r($this->db->last_query());
        //die();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $varsArray = explode(',',$row->variables);

                if($varsArray[0] != ""){
                    foreach($varsArray as $varid => $varID){
                        $funcName = $this->var_model->getVarFuncName($varID);
                        //echo $funcName."<br/>";
                        call_user_func_array(array($this->var_model,$funcName),$params);
                    }
                }
                //$variableValue =
                //$this->dwootemplate->assign($row->name,$variableValue);
            }
        }
    }



    // in widget config for future eas e
    // Add actions: Generate Template,,,,generateCssFile
    // Add afterinsert functions :generateCssFile
    // Add afterinsert functions :generateCssFile
    public function loadCSS($pageid){
        $this->db->where('pageid', $pageid);
        $query = $this->db->get('UI_Css');
        $result = $query->result();

        $cssArray = array();
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                array_push($cssArray,base_url().ASSETCSSPATH.$row->cssurl);
            }
        }
        return $cssArray;
    }

    public function loadJS($pageid){
        $this->db->where('pageid', $pageid);
        $query = $this->db->get('UI_Js');
        $result = $query->result();

        $jsArray = array();
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                array_push($jsArray,base_url().ASSETJSPATH.$row->jsurl);
            }
        }
        return $jsArray;
    }
    // in widget config for future eas e
    // Add actions: Generate Template,,,,generateJSFile
    // Add afterinsert functions :generateJSFile
    // Add afterinsert functions :generateJSFile


    public function hlogin($provider)
    {
        log_message('debug', "controllers.HAuth.login($provider) called");

        try
        {
            log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');


            if ($this->hybridauthlib->providerEnabled($provider))
            {
                log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
                $service = $this->hybridauthlib->authenticate($provider);

                if ($service->isUserConnected())
                {
                    log_message('debug', 'controller.HAuth.login: user authenticated.');
                    // Redirect back to the index to show the profile
                    //redirect('hauth/', 'refresh');
                    return true;
                }
                else // Cannot authenticate user
                {
                    //show_error('Cannot authenticate user');
                    return false;
                }
            }
            else // This service is not enabled.
            {
                log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
                //show_404($_SERVER['REQUEST_URI']);
                return false;
            }
        }
        catch(Exception $e)
        {
            $error = 'Unexpected error';
            switch($e->getCode())
            {
                case 0 : $error = 'Unspecified error.'; break;
                case 1 : $error = 'HybridAuth configuration error.'; break;
                case 2 : $error = 'Provider not properly configured.'; break;
                case 3 : $error = 'Unknown or disabled provider.'; break;
                case 4 : $error = 'Missing provider application credentials.'; break;
                case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                    //redirect();
                    if (isset($service))
                    {
                        log_message('debug', 'controllers.HAuth.login: logging out from service.');
                        $service->logout();
                    }
                    show_error('User has cancelled the authentication or the provider refused the connection.');
                    break;
                case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                    break;
                case 7 : $error = 'User not connected to the provider.';
                    break;
            }

            if (isset($service))
            {
                $service->logout();
            }

            log_message('error', 'controllers.HAuth.login: '.$error);
            show_error('Error authenticating user.');
        }
    }

    public function hlogout($provider = "")
    {

        log_message('debug', "controllers.HAuth.logout($provider) called");

        try
        {
            if ($provider == "") {

                log_message('debug', "controllers.HAuth.logout() called, no provider specified. Logging out of all services.");
                $data['service'] = "all";
                $this->hybridauthlib->logoutAllProviders();
            } else {
                if ($this->hybridauthlib->providerEnabled($provider)) {
                    log_message('debug', "controllers.HAuth.logout: service $provider enabled, trying to check if user is authenticated.");
                    $service = $this->hybridauthlib->authenticate($provider);

                    if ($service->isUserConnected()) {
                        log_message('debug', 'controller.HAuth.logout: user is authenticated, logging out.');
                        $service->logout();
                        $data['service'] = $provider;
                    } else { // Cannot authenticate user
                        show_error('User not authenticated, success.');
                        $data['service'] = $provider;
                    }

                } else { // This service is not enabled.

                    log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
                    show_404($_SERVER['REQUEST_URI']);
                }
            }
            // Redirect back to the main page. We're done with logout
            redirect('hauth/', 'refresh');

        }
        catch(Exception $e)
        {
            $error = 'Unexpected error';
            switch($e->getCode())
            {
                case 0 : $error = 'Unspecified error.'; break;
                case 1 : $error = 'Hybriauth configuration error.'; break;
                case 2 : $error = 'Provider not properly configured.'; break;
                case 3 : $error = 'Unknown or disabled provider.'; break;
                case 4 : $error = 'Missing provider application credentials.'; break;
                case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                    //redirect();
                    if (isset($service))
                    {
                        log_message('debug', 'controllers.HAuth.login: logging out from service.');
                        $service->logout();
                    }
                    show_error('User has cancelled the authentication or the provider refused the connection.');
                    break;
                case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                    break;
                case 7 : $error = 'User not connected to the provider.';
                    break;
            }

            if (isset($service))
            {
                $service->logout();
            }

            log_message('error', 'controllers.HAuth.login: '.$error);
            show_error('Error authenticating user.');
        }
    }

    // Little json api and variable output testing function. Make it easy with JS to verify a session.  ;)
    public function hstatus($provider = "")
    {
        try
        {
            if ($provider == "") {
                log_message('debug', "controllers.HAuth.status($provider) called, no provider specified. Providing details on all connected services.");
                $connected = $this->hybridauthlib->getConnectedProviders();

                if (count($connected) == 0) {
                    $data['status'] = "User not authenticated.";
                } else {
                    $connected = $this->hybridauthlib->getConnectedProviders();
                    foreach($connected as $provider) {
                        if ($this->hybridauthlib->providerEnabled($provider)) {
                            log_message('debug', "controllers.HAuth.status: service $provider enabled, trying to check if user is authenticated.");
                            $service = $this->hybridauthlib->authenticate($provider);
                            // exp of using the twitter social api: Returns settings for the authenticating user.
                            $account_settings = $service->api()->get( 'account/settings.json' );

                            // print recived settings
                            echo "Your account settings on Twitter: " . print_r( $account_settings, true );

                            if ($service->isUserConnected()) {
                                log_message('debug', 'controller.HAuth.status: user is authenticated to $provider, providing profile.');
                                $data['status'][$provider] = (array)$this->hybridauthlib->getAdapter($provider)->getUserProfile();
                            } else { // Cannot authenticate user
                                $data['status'][$provider] = "User not authenticated.";
                            }
                        } else { // This service is not enabled.
                            log_message('error', 'controllers.HAuth.status: This provider is not enabled ('.$provider.')');
                            $data['status'][$provider] = "provider not enabled.";
                        }
                    }
                }
            } else {
                if ($this->hybridauthlib->providerEnabled($provider)) {
                    log_message('debug', "controllers.HAuth.status: service $provider enabled, trying to check if user is authenticated.");
                    $service = $this->hybridauthlib->authenticate($provider);
                    if ($service->isUserConnected()) {
                        log_message('debug', 'controller.HAuth.status: user is authenticated to $provider, providing profile.');
                        $data['status'][$provider] = (array)$this->hybridauthlib->getAdapter($provider)->getUserProfile();
                    } else { // Cannot authenticate user
                        $data['status'] = "User not authenticated.";
                    }
                } else { // This service is not enabled.
                    log_message('error', 'controllers.HAuth.status: This provider is not enabled ('.$provider.')');
                    $data['status'] = "provider not enabled.";
                }
            }
            $this->load->view('hauth/status', $data);
        }
        catch(Exception $e)
        {
            $error = 'Unexpected error';
            switch($e->getCode())
            {
                case 0 : $error = 'Unspecified error.'; break;
                case 1 : $error = 'Hybriauth configuration error.'; break;
                case 2 : $error = 'Provider not properly configured.'; break;
                case 3 : $error = 'Unknown or disabled provider.'; break;
                case 4 : $error = 'Missing provider application credentials.'; break;
                case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                    //redirect();
                    if (isset($service))
                    {
                        log_message('debug', 'controllers.HAuth.login: logging out from service.');
                        $service->logout();
                    }
                    show_error('User has cancelled the authentication or the provider refused the connection.');
                    break;
                case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                    break;
                case 7 : $error = 'User not connected to the provider.';
                    break;
            }

            if (isset($service))
            {
                $service->logout();
            }

            log_message('error', 'controllers.HAuth.login: '.$error);
            show_error('Error authenticating user.');
        }
    }

    public function endpoint()
    {

        log_message('debug', 'controllers.HAuth.endpoint called.');
        log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
            $_GET = $_REQUEST;
        }

        log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
        require_once APPPATH.'/third_party/hybridauth/index.php';

    }
}
?>