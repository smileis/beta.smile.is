<?php
if (!defined('BASEPATH'))
    die();
class Dashboard extends Main_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'), 'menu');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }

    public function index() {
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            $this->dwootemplate->assign('topmenuname','dashboard');
            $this->dwootemplate->assign('pagename','Dashboard');

            $this->dwootemplate->assign('title', "Admin Portal");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('main_body','dashboard.tpl');
            $this->dwootemplate->display('admin/template.tpl');
        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }
}
?>