<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crud_modules extends Main_Controller {

	function __construct() {
		parent::__construct();
		$this->load->config('uiconfig', TRUE);
		
		$this->load->library('form_validation');
		$this->load->library($this->config->item('formio', 'uiconfig'));
		$this->load->library($this->config->item('tables', 'uiconfig'));
		$this->load->library($this->config->item('widgets', 'uiconfig'));
		$this->load->library('grocery_CRUD');
		
		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'auth') ?
		                $this->load->library('mongo_db') :
		                $this->load->database();
		
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'), $this->config->item('error_end_delimiter', 'auth'));
	} 
	
	public function index() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        redirect('admin_home', 'refresh');
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; 
			
			//use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	/**
	 * featuresDetails - Gets the data of all features 
	 */
	public function modulesDetails() {
		if (!$this->auth->logged_in()) {
		    //redirect them to the login page
		    redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
		    //redirect them to the home page because they must be an administrator to view this
		    $this->data['header_tag'] = 'admin/include/header_loggedin.php';
		    $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
		    $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
		    $this->data['main_body'] = 'admin/modules.php';
		    
		    $crud = new grocery_CRUD();
		    
	    	$crud->set_table('CRUD_modules_list');
	    	$crud->columns('id','modulename','status','datecreated');
	    	$crud->fields('modulename','status','datecreated');
	    	
	    	$crud->display_as('modulename','Module Name');
	    	$crud->display_as('status','Status');
	    	$crud->display_as('datecreated','Date Created');
	    	
	    	$this->data['output'] = $crud->render();
		    
		    $this->load->view('admin/template',$this->data);
		} else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
}	
?>