    <?php
if (!defined('BASEPATH'))
    die();
class Manage extends Main_Controller {

    function __construct() {
        parent::__construct();

        $this->config->load('uiconfig', TRUE);

        $this->load->library('form_validation');
		$this->load->library($this->config->item('formio', 'uiconfig'));
        $this->load->library($this->config->item('tables', 'uiconfig'));
        $this->load->library($this->config->item('widgets', 'uiconfig'));
        $this->load->library('grocery_CRUD');

        $this->load->model('userinterface/admin/Widget_callbacks');
        $this->load->model($this->config->item('layouts_variable_model', 'uiconfig'),'var_model');

        $this->load->library($this->config->item('admin_widget', 'uiconfig'),'admin_widgets');
		$this->load->library($this->config->item('admin_menu', 'uiconfig'),'menu');
        $this->dwootemplate->initializeValues($this);


        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }



    public function managewidget($widgetName){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this

            if($widgetName == 'userexec'){
                $this->dwootemplate->assign('title', "Admin Portal - Manage ".$widgetName." ");
                $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
                $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
                /* $this->dwootemplate->assign('output_css',$output_render->css_files);
                $this->dwootemplate->assign('output_js',$output_render->js_files);
                $this->dwootemplate->assign('output_output',$output_render->output);*/
                //var_dump($crud->render()->output);


                //die();
                $this->dwootemplate->assign('main_body','manage/userexec.tpl');
                $this->dwootemplate->display('admin/template.tpl');

            } else {
                $crud = new grocery_CRUD();
                $crud->set_theme('datatables');

                $this->admin_widgets->setWidgetName($widgetName);
                $widgetName = $this->admin_widgets->getWidgetName();
                $table_name = $this->admin_widgets->getTableName();
                $columns_array = $this->admin_widgets->getColumns();
                $fields_array = $this->admin_widgets->getFields();
                $unsetfields_array = $this->admin_widgets->getUnsetFields();
                $display_array = $this->admin_widgets->getDisplays();
                $relation_array = $this->admin_widgets->getRelations();
                $actions_array = $this->admin_widgets->getActions();
                $afterinsert_array = $this->admin_widgets->getAfterinsert();
                $afterupdate_array = $this->admin_widgets->getAfterupdate();

                $crud->set_table($table_name);
                $crud->columns($columns_array);
                $crud->fields($fields_array);
                $crud->unset_columns($unsetfields_array);
    //echo $widgetName;
      //          die();
                if($widgetName == 'Pages'){
                    $crud->field_type('variables','multiselect',$this->var_model->getVarDetails());
                        //array( "1"  => "banana", "2" => "orange", "3" => "apple"));
                }

                if($unsetfields_array) {
                    //$i = 0;
                    foreach ($unsetfields_array as $unsetfield) {
                        $crud->unset_columns($unsetfield);//$crud->display_as($fields_array[$i], $display_value);
                        //$i++;
                    }
                }

                if($actions_array) {
                    //$i = 0;
                    for ($i=0;$i<count($actions_array);$i = $i+5) {
                        $url_callback = $actions_array[$i+4];
                        //var_dump($url_callback);
                        //die();
                        if($url_callback != "null"){
                            $crud->add_action($actions_array[$i],$actions_array[$i+1],$actions_array[$i+2],
                                    $actions_array[$i+3],array($this,$actions_array[$i+4]));
                        } else {
                            $crud->add_action($actions_array[$i],$actions_array[$i+1],$actions_array[$i+2],
                                $actions_array[$i+3]);
                        }
                        //$i++;
                    }
                }

                if($display_array) {
                    $i = 0;
                    foreach ($display_array as $display_value) {
                        $crud->display_as($fields_array[$i], $display_value);
                        $i++;
                    }
                }

                if($relation_array) {
                    foreach ($relation_array as $relation) {
                        $crud->set_relation($relation[0], $relation[1], $relation[2]);
                    }
                }

                if($afterinsert_array) {
                    foreach ($afterinsert_array as $afterinsert) {
                        $crud->callback_after_insert(array($this,$afterinsert));
                    }
                }

                if($afterupdate_array) {
                    foreach ($afterupdate_array as $afterupdate) {
                        $crud->callback_after_insert(array($this, $afterupdate));
                    }
                }
                //$this->Widget_callbacks->
                $output_render = $crud->render();



                $this->dwootemplate->assign('title', "Admin Portal - Manage ".$widgetName." ");
                $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
                $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
                $this->dwootemplate->assign('output_css',$output_render->css_files);
                $this->dwootemplate->assign('output_js',$output_render->js_files);
                $this->dwootemplate->assign('output_output',$output_render->output);
                //var_dump($crud->render()->output);


                //die();
                $this->dwootemplate->assign('main_body','manage/manage.tpl');
                $this->dwootemplate->display('admin/template.tpl');
            }
        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }


    public function widgets(){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this

            $this->data = $this->auth->getUserArray();
            $this->data['title'] = "Admin Portal - Manage Widgets ";
            $this->data['navmenu'] = $this->menu->getMenu();

            //echo $userdetails->getUserCreatedOn();
            //var_dump($this->data['navmenu']);
            //die();

            $crud = new grocery_CRUD();

            $crud->set_theme('datatables');

            $crud->set_table('admin_widgets');
            $crud->columns('id','widgetName','widgetVersion','widgetStatus','widgetConfigID');
            $crud->fields('widgetName','widgetVersion','widgetStatus','widgetConfigID');

            $crud->display_as('widgetName','Widget Name');
            $crud->display_as('widgetVersion','Widget Version');
            $crud->display_as('widgetStatus','Widget Status');
            $crud->display_as('widgetConfigID','Widget ConfigID');

            $output_render = $crud->render();

            /* $this->data['header_tag'] = $this->config->item('headerLoggedin', 'uiconfig');
            $this->data['footer_tag'] = $this->config->item('footerLoggedin', 'uiconfig');
            $this->data['nav_menu'] = 'admin/templates/menu.php';
            $this->data['main_body'] = 'admin/manage/manage.php';
            $this->parser->parse('admin/template',$this->data); */
            $url = str_ireplace('admin/','',uri_string());

            $dataPage = $this->menu->getTopMenuName($url);
			
			$topmenuname = $dataPage['menuname'];
			$pagename = $dataPage['pagename'];
            $this->dwootemplate->assign('pagename',$pagename);
			$this->dwootemplate->assign('topmenuname',$topmenuname);

            $this->dwootemplate->assign('title', "Admin Portal - Manage Widgets ");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('output_css',$output_render->css_files);
            $this->dwootemplate->assign('output_js',$output_render->js_files);
            $this->dwootemplate->assign('output_output',$output_render->output);
            //var_dump($crud->render()->output);
            //die();

            $this->dwootemplate->assign('main_body','manage/manage.tpl');
            $this->dwootemplate->display('admin/template.tpl');
        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }

    public function widgets_config(){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this

            $this->data = $this->auth->getUserArray();
            $this->data['title'] = "Admin Portal - Manage Widgets ";
            $this->data['navmenu'] = $this->menu->getMenu();

            //echo $userdetails->getUserCreatedOn();
            //var_dump($this->data['navmenu']);
            //die();

            $crud = new grocery_CRUD();
            $crud->set_theme('datatables');
            $crud->set_table('admin_widget_config');
            $crud->columns('id','adminwidgetid','actiontype','table_name','columns','display_name','fields','setrelation','actions','afterinsert','afterupdate');
            $crud->fields('adminwidgetid','actiontype','table_name','columns','display_name','fields','setrelation','actions','afterinsert','afterupdate');

            $crud->display_as('adminwidgetid','Widget Name');
            $crud->display_as('actiontype','Page Action Type');
            $crud->display_as('table_name','Table Name');
            $crud->display_as('columns','Columns');
            $crud->display_as('display_name','Display Name');
            $crud->display_as('fields','Fields');
            $crud->display_as('setrelation','Foreign Keys');
            $crud->display_as('actions','Add actions');
            $crud->display_as('afterinsert','Add afterinsert functions');
            $crud->display_as('afterupdate','Add afterinsert functions');

            $crud->set_relation('adminwidgetid','admin_widgets','widgetName');

            $output_render = $crud->render();

            /* $this->data['header_tag'] = $this->config->item('headerLoggedin', 'uiconfig');
            $this->data['footer_tag'] = $this->config->item('footerLoggedin', 'uiconfig');
            $this->data['nav_menu'] = 'admin/templates/menu.php';
            $this->data['main_body'] = 'admin/manage/manage.php';
            $this->parser->parse('admin/template',$this->data); */
            $url = str_ireplace('admin/','',uri_string());

            $dataPage = $this->menu->getTopMenuName($url);
            $topmenuname = "";
            $pagename = "";

			if(isset($dataPage['menuname']))
			    $topmenuname = $dataPage['menuname'];
            if(isset($dataPage['pagename']))
			    $pagename = $dataPage['pagename'];
            $this->dwootemplate->assign('pagename',$pagename);
			$this->dwootemplate->assign('topmenuname',$topmenuname);
			
            $this->dwootemplate->assign('title', "Admin Portal - Manage Widget Config ");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('output_css',$output_render->css_files);
            $this->dwootemplate->assign('output_js',$output_render->js_files);
            $this->dwootemplate->assign('output_output',$output_render->output);
            //var_dump($crud->render()->output);
            //die();

            $this->dwootemplate->assign('main_body','manage/manage.tpl');
            $this->dwootemplate->display('admin/template.tpl');
        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }

    public function generatetemplate($pageid){
        //call library function to create tpl file
        $this->admin_widgets->generateTemplateFile($pageid);
    }

    public function generateJSFile($primary_key,$post_array){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.ASSETJSPATH.$post_array->jsurl;
        $dirPath = dirname(FCPATH.ASSETJSPATH.$post_array->jsurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        /* else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/template','refresh');
        }
        return true; */
    }

    public function generateCSSFile($primary_key,$post_array){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.ASSETCSSPATH.$post_array->cssurl;
        $dirPath = dirname(FCPATH.ASSETCSSPATH.$post_array->cssurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        /* else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/template','refresh');
        }
        return true; */
    }

    public function generateTemplateFile($primary_key,$post_array){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->templateurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->templateurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        /* else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/template','refresh');
        }
        return true; */
    }

    public function generateHeaderFile($primary_key,$post_array){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->headerurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->headerurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        /* else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/headers','refresh');
        } */
        //return true;
    }

    public function generateFooterFile($primary_key,$post_array){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->footerurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->footerurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        /* else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/footers','refresh');
        }
        return true; */
    }
}
?>