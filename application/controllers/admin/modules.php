<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modules extends Main_Controller {

	function __construct() {
		parent::__construct();
		$this->load->config('uiconfig', TRUE);
		
		$this->load->library('form_validation');
		$this->load->library($this->config->item('formio', 'uiconfig'));
		$this->load->library($this->config->item('tables', 'uiconfig'));
		$this->load->library($this->config->item('widgets', 'uiconfig'));
		$this->load->library('grocery_CRUD');
		
		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'auth') ?
		                $this->load->library('mongo_db') :
		                $this->load->database();
		
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'), $this->config->item('error_end_delimiter', 'auth'));
	}
	    
	public function index() {
		if (!$this->auth->logged_in()) {
            //redirect them to the login page
            redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            redirect('admin_home', 'refresh');
        } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; 
			
			//use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function addpage() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/addpage.php';
	        $this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function managepage() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/managepage.php';
	        $this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function managemenu() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/modules.php';
	        
	        $crud = new grocery_CRUD();
	        
	        $crud->set_table('CRUD_modules_list');
	        $crud->columns('id','modulename','status','datecreated');
	        $crud->fields('modulename','status','datecreated');
	        
	        $crud->display_as('modulename','Module Name');
	        $crud->display_as('status','Status');
	        $crud->display_as('datecreated','Date Created');
	        
	        $this->data['output'] = $crud->render();
	        
	        $de = 'sachin';
	        $article_code = 'hello'.$de.' -- wassup ';
	        
	        /* $data = array(
	           'filename' => base64_encode($article_code),
	           'location' => 'My date'
	        );
	        
	        $this->db->insert('CRUD_file_details', $data); 
	        */
	        
	        $query = $this->db->get('CRUD_file_details');
	        
	        foreach ($query->result() as $row)
	        {
	        	//echo("sad");
	            $this->data['tp'] = '===='.base64_decode($row->filename);
	        }
	                       
	        $this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function managemenu1() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/modules.php';
	        
	        $crud = new grocery_CRUD();
	        
	        $crud->set_table('CRUD_modules_list');
	        $crud->columns('id','modulename','status','datecreated');
	        $crud->fields('modulename','status','datecreated');
	        
	        $crud->display_as('modulename','Module Name');
	        $crud->display_as('status','Status');
	        $crud->display_as('datecreated','Date Created');
	        
	        $this->data['output'] = $crud->render();
	                       
	        $this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function addmenu() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/managemenu.php';
	        
	        $crud = new grocery_CRUD();
	        
	        $crud->set_table('admin_navigation_menu');
	        $crud->columns('id','menuname','description','urlname','parentid');
	        $fields = array('menuname','description','urlname','parentid');
	        $crud->fields($fields);
	        $this->data['output'] = $crud->render();
	        
	        $this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function insertmenu() {
		if (!$this->auth->logged_in()) {
	        //redirect them to the login page
	        redirect('admin_login', 'refresh');
		} elseif (!$this->auth->is_admin()) {
	        //redirect them to the home page because they must be an administrator to view this
	        $this->data['header_tag'] = 'admin/include/header_loggedin.php';
	        $this->data['footer_tag'] = 'admin/include/footer_loggedin.php';
	        $this->data['nav_menu'] = 'admin/include/navigation_menu.php';
	        $this->data['main_body'] = 'admin/managemenu.php';
	        
	        $crud = new grocery_CRUD();
	        
	        $crud->set_table('admin_navigation_menu');
	        $crud->columns('id','menuname','description','urlname','parentid');
	        $crud->fields('menuname','description','urlname','parentid');
	        $this->data['output'] = $crud->render();
	        redirect('managemenu', 'refresh');
	        //$this->load->view('admin/template',$this->data);
	    } else{
			//not allowed
			$this->data['header_tag'] = 'admin/include/header_login.php';
			$this->data['footer_tag'] = 'admin/include/footer_login.php';
			$this->data['main_body'] = 'admin/login.php'; //use redirects instead of loading views for compatibility with MY_Controller libraries
			$this->load->view('template',$this->data);
		}
	}
	
	public function _example_output($output = null) {
		$this->load->view('example.php',$output);
	}
}
?>