<?php
if (!defined('BASEPATH'))
    die();
class Admin extends Main_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('uiconfig', TRUE);


        $this->load->library('form_validation');
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');
        $this->load->model($this->config->item('sql_model', 'uiconfig'),'sql_model');
		$this->dwootemplate->initializeValues($this);

       
        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'auth') ?
            $this->load->library('mongo_db') :
            $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'auth'),
            $this->config->item('error_end_delimiter', 'auth'));
    }

    public function index() {
        if (!$this->auth->logged_in()) {
            //redirect them to the login page
            redirect('admin_login', 'refresh');
        } elseif ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            redirect('admin_home', 'refresh');
        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }

    //log the user in
    public function login() {
        $this->dwootemplate->assign('title', "Admin Portal - Login");
        //$this->data['title'] = "Login";
        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page

                $this->session->set_flashdata('message', $this->auth->messages());
                redirect('admin_home', 'refresh');
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                //$this->session->set_flashdata('message', $this->auth->errors());

                //$this->config->set_item('language','arabic');
                //echo $this->config->item('language');

                $this->dwootemplate->assign('message',$this->lang->language['login_error']);
                $this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
                $this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
                $this->dwootemplate->assign('main_body','login.tpl');
                //use redirects instead of loading views for compatibility with MY_Controller libraries
                $this->dwootemplate->display('admin/template.tpl');

                //$this->load->view('template',$this->data);
            }
        } else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'class' => 'input-block-level',
                'placeholder' => 'Email address',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'class' => 'input-block-level',
                'placeholder' => 'Password',
                'type' => 'password',
            );
            $this->data['error_boolean'] = 'style="display:none;"';
            //$this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
            //$this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
            //$this->data['main_body'] = 'admin/login.php';
            $this->dwootemplate->assign('main_body','login.tpl');

            $this->dwootemplate->display('admin/template.tpl');

            //$this->load->view('template',$this->data);

        }
    }

    function logout() {
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $this->auth->messages());
        redirect('admin_login', 'refresh');
    }

    function register() {
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));
        $this->dwootemplate->assign('message',$this->lang->language['login_subheading']);
        $this->dwootemplate->assign('title', "Smile.is Admin Register");

        $this->dwootemplate->assign('main_body','register.tpl');
        $this->dwootemplate->display('admin/template.tpl');
    }

    public function home(){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
			
            $this->dwootemplate->assign('topmenuname','dashboard');
			$this->dwootemplate->assign('pagename','Dashboard');
			
            $this->dwootemplate->assign('title', "Admin Portal");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('main_body','index.tpl');
            $this->dwootemplate->display('admin/template.tpl');


        } else{
            //not allowed
            redirect('admin_login', 'refresh');
        }
    }

    function register_user() {
        $this->data['title'] = "Create User";

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'auth') . ']|max_length[' . $this->config->item('max_password_length', 'auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->input->post('first_name')) . '_' . strtolower($this->input->post('last_name'));
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
            );
        }
        if ($this->form_validation->run() == true && $this->auth->register($username, $password, $email, $additional_data)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->auth->messages());

            /* $this->data['message_element'] = 'auth/login';
            $this->data['message'] = 'Please check '.$email.' for activation link, click on the activation link to activate your account.';
            $this->load->view('template',$this->data); */
            redirect('admin_login', 'refresh');
        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );

            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );
            $this->data['header_tag'] = $this->config->item('headerLogin', 'uiconfig');
            $this->data['footer_tag'] = $this->config->item('footerLogin', 'uiconfig');
            $this->data['main_body'] = 'admin/register.php';

            $this->load->view('template',$this->data);
        }
    }

    public function sql(){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this

            $this->dwootemplate->assign('topmenuname','dashboard');
            $this->dwootemplate->assign('pagename','Dashboard');

            $this->dwootemplate->assign('title', "Admin Portal");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('main_body','sql.tpl');
            $this->dwootemplate->display('admin/template.tpl');


        } else{
            //not allowed
            $this->data['main_body'] = 'home.php';
            $this->parser->parse('template',$this->data);
        }
    }

    public function sqlrun(){
        if ($this->auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this

            $query = $this->input->post('query');

            $type = $this->input->post('type');
            $msg = "";
            if($type == 5){
                $queries = explode(";",$query);

                foreach($queries as $q){
                    $result = $this->sql_model->runQuery($q,$type);
                }
                $result = "Done";
                $msg = "Multiple queries executed!";
            }
            else {
                $result = $this->sql_model->runQuery($q,$type);
                if($result > 0){
                    //success
                    switch($type){
                        case 1:
                            $result1 = "";
                            while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                $result1 .= print_r($row,true);
                            }
                            $result = $result1;
                            //$result = var_export(mysql_fetch_array($result), true);
                            $msg = "Selected ";
                            break;
                        case 2:
                            $msg = "Updated ";
                            break;
                        case 3:
                            $msg = "Deleted ";
                            break;
                        case 4:
                            $msg = "Inserted ";
                            break;
                        default:
                            $msg = "";
                            break;
                    }
                    $msg .= "successfully .<br/>Click <a href='sql'>here</a> to go back.";

                } else {
                    $msg = $result['errmsg'];
                }
            }


            $this->dwootemplate->assign('topmenuname','dashboard');
            $this->dwootemplate->assign('pagename','Dashboard');

            $this->dwootemplate->assign('title', "Admin Portal");
            $this->dwootemplate->assign('userdetails', $this->auth->getUserArray());
            $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
            $this->dwootemplate->assign('sqlmsg',$msg);
            $this->dwootemplate->assign('sqlres',$result);
            $this->dwootemplate->assign('main_body','sqlresults.tpl');
            $this->dwootemplate->display('admin/template.tpl');


        } else{
            //not allowed
            $this->data['main_body'] = 'home.php';
            $this->parser->parse('template',$this->data);
        }
    }
}
