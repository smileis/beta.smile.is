<?php
if (!defined('BASEPATH'))
    die();
class Dwoowelcome extends Main_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('dwootemplate', TRUE);
        $this->load->library('Dwootemplate');
        $this->config->load('uiconfig', TRUE);
        $this->load->model($this->config->item('admin_menu_model', 'uiconfig'),'menu');
    }


    function index()
    {
        $this->dwootemplate->assign('itshowlate', date('H:i:s'));
        $this->dwootemplate->assign('title', "Welcome to Dwooed CodeIgniter");
        $this->dwootemplate->assign('navmenus',$this->menu->getMenu());
        $this->dwootemplate->display('dwowelcome.tpl');
    }
}