<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Sachin Rajput
*
*
*/
/*
 * ========================ADMIN CONFIG ---------------------->>
 */
/*
| -------------------------------------------------------------------------
| User Interface Config
| -------------------------------------------------------------------------
*/
$config['formio'] = 'userinterface/formio/formio';
$config['tables'] = 'userinterface/tables/tables';
$config['widgets'] = 'userinterface/widgets/widgets';
$config['ajax'] = 'userinterface/ajax/ajax_lib';
$config['djapi'] = 'api/dowjonesapi';
$config['djindapi'] = 'api/dowjonesapi';
$config['ajax1'] = 'userinterface/ajax/ajax_model';

/*
| -------------------------------------------------------------------------
| Includes Admin Config
| -------------------------------------------------------------------------
*/
$config['headerLogin'] = "include/header_login.tpl";
$config['headerLoggedin'] = 'include/header_loggedin.tpl';
$config['footerLogin'] = 'include/footer_login.tpl';
$config['footerLoggedin'] = "include/footer_loggedin.tpl";

/*
| -------------------------------------------------------------------------
| Library Admin Config
| -------------------------------------------------------------------------
*/
$config['admin_menu'] = 'userinterface/admin/menu';
$config['admin_widget'] = 'userinterface/admin/adminwidgets';
$config['executive_profile'] = 'user/executiveprofile';

/*
| -------------------------------------------------------------------------
| Model Config
| -------------------------------------------------------------------------
*/
$config['admin_menu_model'] = 'userinterface/admin/admin_menu_model';
$config['admin_widget_model'] = 'userinterface/admin/admin_widget_model';
$config['layouts_variable_model'] = 'userinterface/layouts/variables_model';
$config['sql_model'] = 'crudmodel/sql_model';
$config['api_model'] = 'api/api_model';

//$config['userinterface/formio/formio'] = 'formio';

/*
 * ========================FRONTEND CONFIG ---------------------->>
 */

/*
| -------------------------------------------------------------------------
| Includes Frontend Config
| -------------------------------------------------------------------------
*/
$config['FUIheaderLogin'] = "frontend/include/header.tpl";
$config['FUIheaderLoggedin'] = 'frontend/include/header_loggedin.tpl';
$config['FUIfooterLogin'] = 'frontend/include/footer.tpl';
$config['FUIfooterLoggedin'] = "frontend/include/footer_loggedin.tpl";
$config['FUInav_menu'] = 'frontend/templates/menu.tpl';


/* End of file auth.php */
/* Location: ./application/config/auth.php */
