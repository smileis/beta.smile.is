<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'pages/pages/login';
$route['404_override'] = 'my404';
$route['Frontpage'] = 'Frontpage/index';


/*
	Admin routes
*/
$route['admin'] = 'admin/admin';
$route['admin_login'] = "admin/admin/login";
$route['admin_logout'] = "admin/admin/logout";
$route['admin_home'] = "admin/admin/home";
$route['admin/dashboard'] = "admin/dashboard";
$route['admin_register'] = "admin/admin/register";
$route['admin_register_user'] = "admin/admin/register_user";

$route['test'] = 'admin/modules/demo';
$route['admin/sql'] = 'admin/admin/sql';
$route['admin/sqlrun'] = 'admin/admin/sqlrun';


$route['admin/manage/managewidget/([a-z]+)'] = "admin/manage/managewidget/$1";
$route['admin/manage/managewidget/([a-z]+)/add'] = "admin/manage/managewidget/$1";



//$route['admin/manage/([a-z]+)/add'] = "admin/manage/manageWidgets/$1";

/* $route['admin/manage/menu'] = "admin/manage/menu";
$route['admin/manage/menu/add'] = "admin/manage/menu";

$route['admin/manage/layouts'] = "admin/manage/layouts";
$route['admin/manage/layouts/add'] = "admin/manage/layouts";*/

//$route['admin/manage/inserted'] = "admin/manage/inserted";

$route['addpage'] = "admin/modules/addpage";
$route['managepage'] = "admin/modules/managepage";
$route['managepage1'] = "admin/modules/managemenu";
$route['managemenu'] = "admin/modules/managemenu";
$route['managemenu/add'] = "admin/modules/addmenu";
$route['managemenu/insert'] = "admin/modules/insertmenu";
$route['modules_list'] = "admin/modules/managemenu";
$route['saveexec'] = "api/apiusers/saveexec";


/*
 * ========================
 * FRONTEND Routes
 * -----------------
 */
$route['login'] = 'pages/pages/login';
$route['logout'] = 'pages/pages/logout';
$route['register'] = 'pages/pages/register';
$route['howitworks'] = 'pages/pages/howitworks';
$route['about'] = 'pages/pages/about';
$route['register_user'] = 'pages/pages/register_user';
$route['auth/activate/([0-9]+)/([a-z0-9]+)'] = 'frontend/home/activate/$1/$2';

$route['mset'] = 'api/sessions/manual';
$route['logindjapi'] = 'api/sessions/login';
$route['logoutdjapi'] = 'api/sessions/logout';
$route['loginddjapi'] = 'api/sessions/slogin';
$route['logoutddjapi'] = 'api/sessions/slogout';
$route['printdjapi'] = 'api/sessions/print_session';

/* $route['home'] = 'frontend/home/FUIhome';
$route['login'] = 'frontend/home/login';
$route['logout'] = "frontend/home/logout";
$route['home/dashboard'] = "frontend/home/FUIhome";
$route['register'] = "frontend/home/register";
$route['register_user'] = "frontend/home/register_user";

/*
 * Ajax Route
 * URL Jacking
 */
$route['loadPageClass/([a-zA-Z]+)/([a-zA-Z]+)'] = 'frontend/ajax/loadPageClass/$1/$2';

$route['loadPageMethod/([a-z]+)'] = 'frontend/ajax/loadPageMethod/$1';
//$route['api/([a-z]+)/([a-z]+)/([a-z]+)'] = 'api/index/$1/$2/$3';

/*
 * Router for any page
 */
$route['([a-zA-Z0-9_]+)'] = "pages/pages/loadPage/$1";
$route['([a-zA-Z0-9_]+)/([a-zA-Z0-9_]+)'] = "pages/pages/loadPage2/$1/$2";
$route['([a-zA-Z0-9_]+)/([a-zA-Z0-9_]+)/([a-zA-Z0-9_-]+)'] = "pages/pages/loadPage2Level/$1/$2/$3";
/* End of file routes.php */
/* Location: ./application/config/routes.php */