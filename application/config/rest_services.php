<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| REST Services Values 
|--------------------------------------------------------------------------
|
|
|	FALSE
|
*/
$config['rest_services'] = array(
									'dowjonesapi'=>TRUE,
									'twitterapi'=>TRUE,
									'youtubeapi'=>TRUE,
									'imagesapi'=>TRUE,
									'googleapi'=>FALSE,
									'wikiapi'=>TRUE,
									'linkedinapi'=>FALSE
								);


