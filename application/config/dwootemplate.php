<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// The name of the directory where templates are located.
$config['template_dir'] = APPPATH  . 'views/';

// The directory where compiled templates are located
$config['compileDir']   = APPPATH  . 'compile/';

//This tells Dwoo whether or not to cache the output of the templates to the $cache_dir.
$config['caching']      = 0;
$config['cacheDir']     = APPPATH  . 'cache/';
$config['cacheTime']    = 0;

/*
// The name of the directory where templates are located.
$config['template_dir'] = dirname(FCPATH)  . '/beta/application/views/';

// The directory where compiled templates are located
$config['compileDir']   = dirname(FCPATH) . '/beta/application/compile/';

//This tells Dwoo whether or not to cache the output of the templates to the $cache_dir.
$config['caching']      = 0;
$config['cacheDir']     = dirname(FCPATH) . '/beta/application/cache/';
$config['cacheTime']    = 0;
*/