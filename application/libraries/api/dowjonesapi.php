<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Extended API Class
 */
 
class Dowjonesapi extends Api_lib
{
	function __construct($config = array())
    {
		$this->_ci =& get_instance();
		$this->urls = array(
			'execsearch' => 'http://api.beta.dowjones.com/api/1.0/Executives/search/',
			'execdetail' => 'http://api.beta.dowjones.com/api/1.0/Executives/',
			'articlesearch' => 'http://api.beta.dowjones.com/api/1.0/Content/search/',
			'articledetail' => 'http://api.beta.dowjones.com/api/1.0/Content/article/',
			'suggest_industry' => 'http://suggest.factiva.com/search/1.0/industry'
		);
		
		$this->params = array(
			'execsearch' => array("firstname","lastname","company","records"),
			'execdetail' => array("Id","Parts"),
			'articlesearch' => array("QueryString"),
			'articledetail' => array("Id","parts"),
			'suggestindustry' => array("searchText")
		);
		
		$this->format = 'xml';
		
        $this->_ci->load->library('user/executiveprofile');
	}
	
	/**
     *
     * @return XML object
     * @author Sachin rajput
     */
	public function invokeAPI($serviceName,$paramsIn,$custom=false){
		$session_id = $this->_ci->session->userdata('djsessionid');
		$suggest_context = $this->_ci->session->userdata('djsuggestcontext');
		
		$params = array(); 
		if($custom){
			foreach($paramsIn as $fieldName => $value){
				$params[$fieldName] = $value;
			}
		} else {
			for($i=0;$i<count($paramsIn);$i++){
				$params[$this->params[$serviceName][$i]] = $paramsIn[$i];
			} 
		}
		//$urlQuery .= "sessionid=".$session_id;
		$service_check = explode("_",$serviceName);
		if(count($service_check)>1){
			$params['suggestContext'] = $suggest_context;
			$params['languageCode'] = 'en';
			$params['format'] = 'xml';
			$url = $this->urls[$serviceName];
			//echo $url;
			//die();
		} else {
			$params['sessionid'] = $session_id;
			$url = $this->urls[$serviceName];
			$url .= $this->format;
		}
		//echo $url;
		//die();
		$return = $this->_ci->rest_model->get($url,$params,$this->format);
		return $return;
	}
	
	
	/**
     *
     * @return XML object
     * @author Sachin rajput
     */
	public function suggestIndustry(){
		$session_id = $this->_ci->session->userdata('djsessionid');
		$suggest_context = $this->_ci->session->userdata('djsuggestcontext');
		
		$serviceName = $this->_ci->input->post('servicename');
		$custom = $this->_ci->input->post('custom');
		
		$params = array(); 
		
		$params['suggestContext'] = $suggest_context;
		$params['languageCode'] = 'en';
		$params['format'] = 'xml';
		$params['searchText'] = $this->_ci->input->post('params');
		$url = $this->urls[$serviceName];
		
		$return = $this->_ci->rest_model->get($url,$params,$this->format);
		//var_dump($return);
		
		//prepare html and print on the page
		$content = "<div class=\"panel-heading\">Industries</div>
				<div class=\"panel-body\">";
		
		foreach($return->industry as $industryName){
			$checkbox = "<input type=\"checkbox\" id=\"".$industryName."\" class=\"industryVal\" value=\"".$industryName["code"]."\">";
			$content .= $checkbox.' '.$industryName.'<br/>';
		}
		$content .=	"</div>
		";
		print $content;
		//return $return;
	}
	
	
	
	/**
     *
     * @return XML object
     * @author Sachin rajput
     */
	public function recommendExec(){
		$username = $this->_ci->session->userdata('username');
        $this->_ci->db->where('username', $username);
        $query = $this->_ci->db->get('users_industry');
        $result = $query->result();
        $dataArray = "";
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $dataArray = $row->industrylist;
            }
			$dataArr = json_decode($dataArray,true);//explode("|",$dataArray);
			//var_dump($dataArr);
			$recomm_list = "";
			$count = 0;
			print '<div class="row">';
			foreach($dataArr as $dataval){
				$session_id = $this->_ci->session->userdata('djsessionid');
				$suggest_context = $this->_ci->session->userdata('djsuggestcontext');
				
				$serviceName = $this->_ci->input->post('servicename');
				$custom = $this->_ci->input->post('custom');
				
				$params = array(); 
				
				
				$params['Industry'] = $dataval;
				$params['Records'] = 3;
                $params['SortBy'] = 'Industry';
                $params['Offset'] = 25;
				//$params['SortOrder'] = 'ascending';
				$params['sessionid'] = $session_id;
				$url = $this->urls['execsearch'];
				$url .= $this->format;
				$return = $this->_ci->rest_model->get($url,$params,$this->format);
				
				foreach($return->Executives->ExecutiveProfile as $exec){
					$count++;
					$newdata = '
						
						  <div class="col-sm-5 col-md-3">
							<div class="thumbnail" id="'.$exec->Executive->FCode.'_box">
							  <img src="http://www.pubzi.com/f/Generic-Profile-Image-Placeholder-Suit.svg"
											 class="img-responsive">
							  <div class="caption">
								<h3>'.$exec->Executive->DisplayName.'</h3>
								<p>
								Company Name: <span class="label label-info">'.$exec->Executive->CompanyName.'</span><br/>Job Title: <span class="label label-primary">'.$exec->Executive->JobTitle.'</span><br/> Industry: <span class="label label-success">'.$exec->Executive->Industry.'</span></p>
								<p><a href="javascript:" id="'.$exec->Executive->FCode.'" class="addExec btn btn-primary" role="button">Add</a>
								<a href="javascript:" id="'.$exec->Executive->FCode.'" class="removeExec btn btn-primary" role="button">Remove</a></p>
							  </div>
							</div>
						  </div>
						
					';
					
					print $newdata;
					if($count %4 ==0) print '</div><div class="row">';
					//$recomm_list = $recomm_list + $newdata;
				}
				
			}
			print '</div>';
			
        }
		
		
	}
	
	/**
     *
     * @return Executive object
     * @author Sachin rajput
     */
    public function getExecutiveObject($execuser)
    {
        //return $user = $this->authmodel->userObject($user_id);
        //$execuser = $this->apimodel->djexecdetail($user_exec_profile);
        return $userObject = $this->_ci->executiveprofile->setExecUserObject($execuser);
    }

    public function getExecutiveArray($execuser){
        $userObject = $this->getExecutiveObject($execuser);
        $userData = $userObject->getExecArray();
        return $userData;
    }
	
}