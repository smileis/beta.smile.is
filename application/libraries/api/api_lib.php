<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter REST Class
 *
 * Make REST requests to RESTful services with simple syntax.
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Sachin Rajput
 * @created         04/21/2015
 */

class Api_Lib
{	
	/**
	 * 
	 */
	public $urls;
	
	/**
	 * 
	 */
	public $params;
	
	/**
	 * 
	 */
	public $format;
	
	function __construct($config = array())
    {
		$this->_ci =& get_instance();
        log_message('debug', 'API Library Initialized');
		$this->_ci->load->model('api/rest_model','rest_model');
		foreach($this->_ci->config->item('rest_services') as $service => $state ){
			//echo $service.'=>'.$state.'<br/>';
			$service_name = substr($service,0,-3);
			
			if($state){
				
				try{
					//echo $service_name.'<br/>';
					$check = file_exists(APPPATH.'/libraries/api/'.$service.'.php');
					//echo $check;
					if($check){
						$this->_ci->load->library('api/'.$service,lcfirst($service_name));
						//echo "loaded : ". $service_name;
					}
					else{
						$newFile = fopen(APPPATH.'/libraries/api/'.$service.'.php','w');
						$this->createAPIFile($newFile,$service);
						$this->_ci->load->library('api/'.$service,lcfirst($service_name));
					}
				} catch(Exception $e){
					var_dump($e->getMessage()); 
				}
			}
		}
	}
	
	public function createAPIFile($newFile,$service){
		$text = "<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* ".ucfirst($service)." API Class
*/

class ".ucfirst($service)." extends Api_lib
{
	function __construct()
	{
		
	}
	
}";
		fwrite($newFile, $text);
		fclose($newFile);
	}
	
	
}