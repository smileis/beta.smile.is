<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Assign basic template variables
 */

class Dwooinit{

    function __construct(){


    }

    /**
     * Assign admin template variables
     */
    public function adminInit(){
        $this->dwootemplate->assign('base_url',base_url());
        $this->dwootemplate->assign('auth',$this->auth->logged_in());
        $this->dwootemplate->assign('uri_1',$this->uri->segment(1));
        $this->dwootemplate->assign('uri_2',$this->uri->segment(2));
        $this->dwootemplate->assign('uri_3',$this->uri->segment(3));
        $this->dwootemplate->assign('header_login_tag',$this->config->item('headerLogin', 'uiconfig'));
        $this->dwootemplate->assign('footer_login_tag',$this->config->item('footerLogin', 'uiconfig'));
        $this->dwootemplate->assign('header_tag',$this->config->item('headerLoggedin', 'uiconfig'));
        $this->dwootemplate->assign('footer_tag',$this->config->item('footerLoggedin', 'uiconfig'));
        $this->dwootemplate->assign('nav_menu','templates/menu.tpl');
    }
}

?>