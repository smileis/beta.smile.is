<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth
*
* Version: 1.0.0
*
* Author:  Sachin Rajput
* 		   svrajput@thoughtlinks.co
*
*
* Location: https://bitbucket.org/sachinrajput/
*
* Created:  08.10.2014
*
* Description:  This class will be used to get user details.
*
* Requirements: PHP5 or above
*
*/

class User
{
	/**
	 * userid 
	 *
	 * @var string
	 **/
	private  $userid;

	/**
	 * username
	 *
	 * @var string
	 **/
	private $username;

	/**
	 * useremail
	 *
	 * @var string
	 **/
	private $useremail;

	/**
	 * created_on
	 *
	 * @var string
	 **/
	private $created_on;

	/**
	 * last_login
	 *
	 * @var string
	 **/
	private $last_login;

	/**
	 * userstatus
	 *
	 * @var string
	 **/
	private $userstatus;

	/**
	 * first_name
	 *
	 * @var string
	 **/
	private $first_name;

	/**
	 * last_name
	 *
	 * @var string
	 **/
	private $last_name;

	/**
	 * company
	 *
	 * @var string
	 **/
	private $company;

	/**
	 * phone
	 *
	 * @var string
	 **/
	private $phone;

	/**
	 * ip
	 *
	 * @var string
	 **/
	private $ip;

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get($var)
	{
		return get_instance()->$var;
	}

	public function setUserID($user_id){
		$this->userid = $user_id;
	}

	public function getUserID(){
		return $this->userid;
	}

	public function setUserName($user_name){
		$this->username = $user_name;
	}

	public function getUserName(){
		return $this->username;
	}

	public function setUserEmail($user_email){
		$this->useremail = $user_email;
	}

	public function getUserEmail(){
		return $this->useremail;
	}

	public function setUserCreatedOn($user_createdon){
		$this->created_on = $user_createdon;
	}

	public function getUserCreatedOn(){
        $this->created_on = date('m/d/Y', $this->created_on);
		return $this->created_on;
	}

	public function setUserLastLogin($user_lastlogin){
		$this->last_login = $user_lastlogin;
	}

	public function getUserLastLogin(){
        $this->last_login = date('m/d/Y', $this->last_login);
		return $this->last_login;
	}

	public function setUserFirstName($user_fname){
		$this->first_name = $user_fname;
	}

	public function getUserFirstName(){
		return $this->first_name;
	}

	public function setUserLastName($user_lname){
		$this->last_name = $user_lname;
	}

	public function getUserLastName(){
		return $this->last_name;
	}

	public function setUserStatus($user_status){
		$this->userstatus = $user_status;
	}

	public function getUserStatus(){
		return $this->userstatus;
	}

	public function setUserCompany($user_company){
		$this->company = $user_company;
	}

	public function getUserCompany(){
		return $this->company;
	}

	public function setUserPhone($user_phone){
		$this->phone = $user_phone;
	}

	public function getUserPhone(){
		return $this->phone;
	}

	public function setUserIP($user_ip){
		$this->ip = $user_ip;
	}

	public function getUserIP(){
		return $this->ip;
	}

	public function setUserObject($user_row){
		$userObj = $user_row[0];
		$this->setUserID($userObj->id);
		$this->setUserIP($userObj->ip_address);
		$this->setUserName($userObj->username);
		$this->setUserEmail($userObj->email);
		$this->setUserCreatedOn($userObj->created_on);
		$this->setUserLastLogin($userObj->last_login);
		$this->setUserStatus($userObj->active);
		$this->setUserFirstName($userObj->first_name);
		$this->setUserLastName($userObj->last_name);
		$this->setUserCompany($userObj->company);
		$this->setUserPhone($userObj->phone);
		return $this;
	}

    public function getArray(){
        $userArray = array();
        $userArray['firstname'] = $this->getUserFirstName();
        $userArray['lastname'] = $this->getUserLastName();
        $userArray['username'] = $this->getUserName();
        $userArray['useremail'] = $this->getUserEmail();
        $userArray['usercreatedon'] = $this->getUserCreatedOn();
        $userArray['userlastlogin'] = $this->getUserLastLogin();
        $userArray['userstatus'] = $this->getUserStatus();
        $userArray['userphone'] = $this->getUserPhone();
        $userArray['usercompany'] = $this->getUserCompany();
        $userArray['userid'] = $this->getUserID();
        $userArray['userip'] = $this->getUserIP();
        return $userArray;
    }
}