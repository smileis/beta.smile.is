<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Widgets
 * An easier way to construct widgets
 * future work : make theme dynamic in this retrieve theme from DB 
 * @author sachin
 */
 
class Widgets {

	protected $_ci;                 // CodeIgniter instance
	
	public function __construct()
	{
		$this->_ci = & get_instance();
		$this->_ci->load->model('userinterface/widgets/widgets_model');
	}

	public function widgetHeader($label,$placeholder,$name) {
		print $this->widgets_model->widgetHeader($label,$placeholder,$name);
	}
	
	public function widgetFooter() {
		print $this->widgets_model->widgetFooter();
	}
	
	
}

?>