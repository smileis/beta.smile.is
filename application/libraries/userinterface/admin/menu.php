<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Menu
 * An easier way to construct widgets
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Menu {



    protected $_ci;                 // CodeIgniter instance

    public function __construct()
    {
        $this->_ci = & get_instance();
        $this->_ci->load->model($this->_ci->config->item('admin_menu_model', 'uiconfig'),'menuObj');
    }

    public function getMenu(){
        return $this->_ci->menuObj->getMenu();
    }

    public function getSubMenu($parentid){
        return $this->_ci->menuObj->getSubMenu($parentid);
    }

    public function getTopMenuName($url){
        return $this->_ci->menuObj->getTopMenuName($url);
    }


}

?>