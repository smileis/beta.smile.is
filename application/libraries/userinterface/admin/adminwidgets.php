<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Widgets
 * An easier way to construct widgets
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Adminwidgets {

    protected $_ci;                 // CodeIgniter instance

    public function __construct()
    {
        $this->_ci = & get_instance();
        $this->_ci->load->model($this->_ci->config->item('admin_widget_model', 'uiconfig'),'admin_widgets');
    }

    public function setWidgetName($widgetNameIn){
        $this->admin_widgets->setWidgetName($widgetNameIn);
    }

    public function getWidgetName(){
        return $this->admin_widgets->widgetName;
    }

    public function getTableName(){
        return $this->admin_widgets->tablename;
    }

    public function getColumns(){
        return $this->admin_widgets->column_array;
    }

    public function getFields(){
        return $this->admin_widgets->field_array;
    }

    public function getUnsetFields(){
        return $this->admin_widgets->getUnsetFields();
    }

    public function getDisplays(){
        return $this->admin_widgets->display_array;
    }

    public function getRelations(){
        return $this->admin_widgets->getRelations();
    }

    public function generateTemplateFile($pageid){
        $this->admin_widgets->generateTemplateFile($pageid);
    }

}

?>