<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Tables 
 * An easier way to construct tables
 * future work : make theme dynamic in this retrieve theme from DB 
 * @author sachin
 */
 
class Tables {

	protected $_ci;                 // CodeIgniter instance
	
	public function __construct()
	{
		$this->_ci = & get_instance();
		$this->_ci->load->model('userinterface/tables/tables_model');
	}

	public function tableHeader($label,$placeholder,$name) {
		print $this->tables_model->tableHeader($label,$placeholder,$name);
	}
	
	
	
}

?>