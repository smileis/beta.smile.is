<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Filename:    Ajax_model
 * Purpose:     Way to add methods to support our ajax calls from Frontend
 *
 * Usage:       Frontend Developer should pass on the function name to
 *              Backend Developer
 *
 * @author sachin
 */

class Ajax_lib {

    public function __construct()
    {
        $this->_ci =& get_instance();
        $this->_ci->load->database();
        $this->_ci->load->helper('date');
        $this->_ci->lang->load('auth');
        $this->_ci->load->model('db_model','database');

        //initialize db tables data
        $this->_ci->tables = $this->_ci->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    /**
     *
     */
	public function saveIndustry(){
		//var_dump($this->_ci->input->post());
		// save and redirect to recommend page
		$data1 = $this->_ci->input->post('data');
		$datalist = substr($data1, 1);
		//echo $data;
		$username = $this->_ci->session->userdata('username');
		$indData = $this->checkIndustry();
		
		
		if($indData['id'] > 0){
			$indlist = $datalist;
			$indarray1 = explode("|",$indlist);
			$arr2 = json_decode($indData['industrylist'],true);
			$indarray = array_merge($indarray1,$arr2);
			$indarr = array_unique($indarray);
			$indjson = json_encode($indarr);
			$data = array(
							'industrylist' => $indjson
						);
			$this->_ci->db->where('id',$indlist['id']);
			$this->_ci->db->update('users_industry', $data);
			if($this->_ci->db->affected_rows() > 0){
				redirect('recommend', 'refresh');
			}
		} else {
			$indarray = explode("|",$datalist);
			$indarr = array_unique($indarray);
			$indjson = json_encode($indarr);
			$data = array(
							'username' =>  $username,
							'industrylist' => $indjson ,
							'industryflag' => 1 
						);
			
			$this->_ci->db->insert('users_industry', $data);
			if($this->_ci->db->affected_rows() > 0){
				redirect('recommend', 'refresh');
			}
		}
	}

    /**
     * @return array
     */
	public function checkIndustry(){
		$username = $this->_ci->session->userdata('username');
		$this->_ci->db->where('username', $username);
        $query = $this->_ci->db->get('users_industry');
        $result = $query->result();
		$indData = array();
		$indData['id'] = 0;
		
		if ($query->num_rows() > 0)
        {
			foreach ($result as $row)
            {
                $indData['id'] = $row->id;
                $indData['industrylist'] = $row->execslist;
				$indData['industryflag'] = $row->execsflag;
            }
		}
		return $indData; 
	}

    /**
     *
     */
	public function saveExecs(){
		$data1 = $this->_ci->input->post('data');
		$datalist = substr($data1, 1);
		//echo $data;
		$username = $this->_ci->session->userdata('username');
		$execData = $this->checkExecs();
		
		if($execData['id'] > 0){
			$execlist = $datalist;
			$execarray1 = explode("|",$execlist);
			$arr2 = json_decode($execData['execslist'],true);
			
			$execarray = array_merge($execarray1,$arr2);
			$execarr = array_unique($execarray);
			$execjson = json_encode($execarr);

            //update or insert this execs in database
            $this->saveExecsInDB($execjson);

			$data = array(
							'username' =>  $username,
							'execslist' => $execjson ,
							'execsflag' => 1 
						);
			$this->_ci->db->where('id',$execData['id']);
			$this->_ci->db->update('users_executives', $data);
			if($this->_ci->db->affected_rows() > 0){
				redirect('home', 'refresh');
			}
		} else {
			$execarray = explode("|",$datalist);
			$execarr = array_unique($execarray);
			$execjson = json_encode($execarr);

            //update or insert this execs in database
            $this->saveExecsInDB($execjson);

			$data = array(
							'username' =>  $username,
							'execslist' => $execjson ,
							'execsflag' => 1 
						);
			
			$this->_ci->db->insert('users_executives', $data);
			if($this->_ci->db->affected_rows() > 0){
				redirect('home', 'refresh');
			}
		}
	}

    public  function saveExecsInDB($execJSON){

        //list of execs id
        $execArr = json_decode($execJSON,true);

        foreach($execArr as $execid){
            $this->_ci->db->where('execid', $execid);
            $query = $this->_ci->db->get('execs_data');
            $result = $query->result();
            if ($query->num_rows() < 1)
            {
                //insert data
                $params = array(
                    $execid,
                    'All|CompensationEx'
                );
                $execXML = $this->_ci->dowjonesapi->invokeAPI('execdetail',$params);
                $execObj = $this->_ci->dowjonesapi->getExecutiveArray($execXML);
                $execDataArr = array(
                    'FirstName'=>$execObj['Executive']->FirstName,
                    'LastName'=>$execObj['Executive']->LastName,
                    'FullName'=>$execObj['Executive']->FullName,
                    'DisplayName'=>$execObj['Executive']->DisplayName,
                    'FCode'=>$execObj['Executive']->FCode
                                    );
                $execDataJSON = json_encode($execDataArr);
                $data = array(
                    'execid' =>  $execid,
                    'data' => $execDataJSON
                );
                $this->_ci->db->insert('execs_data', $data);
            }
        }
    }


    /**
     * @return array
     */
	public function checkExecs(){
		$username = $this->_ci->session->userdata('username');
		$this->_ci->db->where('username', $username);
        $query = $this->_ci->db->get('users_executives');
        $result = $query->result();
		$execData = array();
		$execData['id'] = 0;
		if ($query->num_rows() > 0)
        {
			foreach ($result as $row)
            {
				$execData['id'] = $row->id;
                $execData['execslist'] = $row->execslist;
				$execData['execsflag'] = $row->execsflag;
            }
		}
		return $execData;
	}
	
    public function commentdemo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        //$comment = $this->input->post('comment');
		var_dump($this->_ci->input->post());echo "sd";
		 die();
        if($comment == "hii") echo "Hello! there!";
        else if ($comment == "bye") echo "Good Night!";
        else echo $comment;
    }

    public function getdemo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        $demovar = array();
        $demovar['a1'] = "ggg";
        $demovar['a2'] = "ggg2";
        echo $demovar['a1'];
        //echo "Hey there !";
    }

    public function demo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        $demovar = array();
        $demovar['a1'] = "ggg";
        $demovar['a2'] = "ggg2";
        echo $demovar['a2'];
        //echo "Hey there !";
    }

}