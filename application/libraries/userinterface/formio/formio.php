<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Formio
 * An easier way to construct elements of HTML form 
 * future work : make theme dynamic in this retrieve theme from DB 
 * @author sachin
 */
 
class Formio {

	protected $_ci;                 // CodeIgniter instance
	
	public function __construct()
	{
		$this->_ci = & get_instance();
		$this->_ci->load->model('userinterface/formio/formio_model');
	}

	public function generateInputTextBox($label,$placeholder,$name) {
		print $this->formio_model->generateInputTextBox($label,$placeholder,$name);
	}
	
	public function generateRadioBox($label,$valarray,$textarray,$name) {
		print $this->formio_model->generateRadioBox($label,$valarray,$textarray,$name);
	}
	
	public function generateTextAreaBox($label,$placeholder,$name) {
		print $this->formio_model->generateTextAreaBox($label,$placeholder,$name);
	}
	
	public function generateCheckBox($label,$valarray,$textarray,$name) {
		print $this->formio_model->generateCheckBox($label,$valarray,$textarray,$name);
	}
	
	public function generateSelectBox($label,$valarray,$textarray,$name) {
		print $this->formio_model->generateSelectBox($label,$valarray,$textarray,$name);
	}
	
	public function generateCLIEditorBox($name,$label){
		print $this->formio_model->generateCLIEditorBox($name,$label);
	}
	
	public function generateButton($type,$label){
		print $this->formio_model->generateButton($type,$label);
	}
	
}

?>