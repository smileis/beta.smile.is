<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  DB Model
 *
 * Version: 1.0.0
 *
 * Author:  Sachin Rajput
 * 		   svrajput@thoughtlinks.co
 *
 *
 *
 *
 * Location: https://bitbucket.org/sachinrajput/
 *
 * Created:  08.10.2014
 *
 * Last Change: 09.15.2014
 *
 * Changelog:
 * * 3-22-13 - Additional entropy added - 52aa456eef8b60ad6754b31fbdcc77bb
 *
 * Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 */

class Db_model extends CI_Model{

    /**
     * Where
     *
     * @var array
     **/
    public $_where = array();

    /**
     * Select
     *
     * @var array
     **/
    public $_select = array();

    /**
     * Like
     *
     * @var array
     **/
    public $_like = array();

    /**
     * Limit
     *
     * @var string
     **/
    public $_limit = NULL;

    /**
     * Offset
     *
     * @var string
     **/
    public $_offset = NULL;

    /**
     * Order By
     *
     * @var string
     **/
    public $_order_by = NULL;

    /**
     * Order
     *
     * @var string
     **/
    public $_order = NULL;

    /**
     * Response
     *
     * @var string
     **/
    protected $response = NULL;

    /**
     * Hooks
     *
     * @var object
     **/
    protected $_hooks;

    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->config('auth', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('hooks_model');

        //initialize db tables data
        $this->tables  = $this->config->item('tables', 'auth');

    }

    public function limit($limit)
    {
        $this->hooks_model->trigger_events('limit');
        $this->_limit = $limit;

        return $this;
    }

    public function offset($offset)
    {
        $this->hooks_model->trigger_events('offset');
        $this->_offset = $offset;

        return $this;
    }

    public function where($where, $value = NULL)
    {
        $this->hooks_model->trigger_events('where');

        if (!is_array($where))
        {
            $where = array($where => $value);
        }

        array_push($this->_where, $where);

        return $this;
    }

    public function like($like, $value = NULL, $position = 'both')
    {
        $this->hooks_model->trigger_events('like');

        if (!is_array($like))
        {
            $like = array($like => array(
                'value'    => $value,
                'position' => $position,
            ));
        }

        array_push($this->_like, $like);

        return $this;
    }

    public function select($select)
    {
        $this->hooks_model->trigger_events('select');

        $this->_select[] = $select;

        return $this;
    }

    public function order_by($by, $order='desc')
    {
        $this->hooks_model->trigger_events('order_by');

        $this->_order_by = $by;
        $this->_order    = $order;

        return $this;
    }

    public function row()
    {
        $this->hooks_model->trigger_events('row');

        $row = $this->response->row();
        $this->response->free_result();

        return $row;
    }

    public function row_array()
    {
        $this->hooks_model->trigger_events(array('row', 'row_array'));

        $row = $this->response->row_array();
        $this->response->free_result();

        return $row;
    }

    public function result()
    {
        $this->hooks_model->trigger_events('result');

        $result = $this->response->result();
        $this->response->free_result();

        return $result;
    }

    public function result_array()
    {
        $this->hooks_model->trigger_events(array('result', 'result_array'));

        $result = $this->response->result_array();
        $this->response->free_result();

        return $result;
    }

    public function num_rows()
    {
        $this->hooks_model->trigger_events(array('num_rows'));

        $result = $this->response->num_rows();
        $this->response->free_result();

        return $result;
    }
}
?>