<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Hooks Model
 *
 * Version: 1.0.0
 *
 * Author:  Sachin Rajput
 * 		   svrajput@thoughtlinks.co
 *
 *
 *
 *
 * Location: https://bitbucket.org/sachinrajput/
 *
 * Created:  08.10.2014
 *
 * Last Change: 09.15.2014
 *
 * Changelog:
 * * 3-22-13 - Additional entropy added - 52aa456eef8b60ad6754b31fbdcc77bb
 *
 * Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 */
class Hooks_model extends CI_Model{

    /**
     * Hooks
     *
     * @var object
     **/
    protected $_hooks;

    public function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->config('auth', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('date');
        $this->lang->load('auth');

        //initialize our hooks object
        $this->_hooks = new stdClass;

    }
    public function set_hook($event, $name, $class, $method, $arguments)
    {
        $this->_hooks->{$event}[$name] = new stdClass;
        $this->_hooks->{$event}[$name]->class     = $class;
        $this->_hooks->{$event}[$name]->method    = $method;
        $this->_hooks->{$event}[$name]->arguments = $arguments;
    }

    public function remove_hook($event, $name)
    {
        if (isset($this->_hooks->{$event}[$name]))
        {
            unset($this->_hooks->{$event}[$name]);
        }
    }

    public function remove_hooks($event)
    {
        if (isset($this->_hooks->$event))
        {
            unset($this->_hooks->$event);
        }
    }

    protected function _call_hook($event, $name)
    {
        if (isset($this->_hooks->{$event}[$name]) && method_exists($this->_hooks->{$event}[$name]->class, $this->_ion_hooks->{$event}[$name]->method))
        {
            $hook = $this->_hooks->{$event}[$name];

            return call_user_func_array(array($hook->class, $hook->method), $hook->arguments);
        }

        return FALSE;
    }

    public function trigger_events($events)
    {
        if (is_array($events) && !empty($events))
        {
            foreach ($events as $event)
            {
                $this->trigger_events($event);
            }
        }
        else
        {
            if (isset($this->_hooks->$events) && !empty($this->_hooks->$events))
            {
                foreach ($this->_hooks->$events as $name => $hook)
                {
                    $this->_call_hook($events, $name);
                }
            }
        }
    }
}
?>