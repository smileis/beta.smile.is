<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 *
 * @author sachin
 */

class Api_model extends CI_Model {



    public function djexeclist(){
        $session_id = $this->session->userdata('djsessionid');
        $fname = $this->get('fname');
        $lname = $this->get('lname');
        $company = $this->get('company');
        $records = $this->get('records');
        //echo "ss".$fname.$lname;

        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/executives/search/xml?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
        //echo $url;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);

    }

    public function djexecdetail($user_exec_profile){
        $session_id = $this->session->userdata('djsessionid');

        $id = $user_exec_profile->FCode;
        $parts = 'All|CompensationEx';
        //Biography

        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/Executives/xml?Id='.$id.'&Parts='.$parts.'&SessionId='.$session_id;
        //echo $url;
        //die();
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        return $this->curlRequest($url);
    }

    public function djarticlesearch($keywordIn,$limit){
        $session_id = $this->session->userdata('djsessionid');

        $keyword = $keywordIn;//$this->get('id');
        //Biography
        //echo $keyword;
        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/Content/search/xml?QueryString='.$keyword.'&Records='.$limit.'&SearchResultPartList=KeywordSummary|MetadataSuggestion|companySuggestion&DuplicationMode=off&SearchMode=all&SourceGenre=Publications&SessionId='.$session_id;
        //echo $url;
        //die();
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        return $this->curlRequest($url);
    }

    public function curlRequest($url){
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        return $xml;
    }


}
?>