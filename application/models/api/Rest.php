<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Sachin Rajput
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/model/api/rest_model.php';

//http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue
class Rest extends Rest_model
{

    public $articleSearchlink = "http://api.beta.dowjones.com/api/1.0/Content/search/xml";
    public $articlelink = "http://api.beta.dowjones.com/api/1.0/Content/article/xml";
    public $execsSearchlink = "http://api.beta.dowjones.com/api/1.0/Executives/search/xml";
    public $execslink = "http://api.beta.dowjones.com/api/1.0/Executives/xml";
    public $wikilink = "http://en.wikipedia.org/w/api.php";

    //27140XxX_JUYTIMRYGQYTANRSHAXTEQSXG5GHUNTYO5REEU2NJZZUE4TEMZJFMZKLNB2E42LDGBBGUL2NOR4FUV3FIJBES5SPNE4DEM3IJZHGIOLQHA2DQ6SZM5STQZDVI5ZGSTLKO5VTOZKIINWDMZLDJ5RGQTTXJJATQL3LKZSGUMRUORLUO2TPJB2XA5BSJBDFKZZPOZXW26SVOUVXQ3DNIVKGGVSGOVYDMRRPMVTDIZRSPJBUIWBWK5FWKWLSKFNG44LLGRZW2UDTK4YVERKKPJJE6TCNLJ3GIU3VMEZFSPKH

    public function manual_session($sessionid){
        $this->session->set_userdata('session_id',$sessionid);
        echo $this->session->all_userdata();
    }

    function djexeclist_get(){
        $session_id = $this->session->userdata('sessionid');
        $fname = $this->get('fname');
        $lname = $this->get('lname');
        $company = $this->get('company');
        $records = $this->get('records');
        //echo "ss".$fname.$lname;

        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/executives/search/xml?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
        //echo $url;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }
    }


    function djexecdetail_get(){
        $session_id = $this->session->userdata('sessionid');
        $id = $this->get('id');
        $parts = $this->get('parts');
        //Biography
        //echo "ss".$fname.$lname;

        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/Executives/xml?Id='.$id.'&Parts='.$parts.'&SessionId='.$session_id;
        //echo $url;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }
    }

    function djapi_get(){
        $session_id = $this->session->userdata('sessionid');
        $queryString = explode("|",$this->get('q'));
        //var_dump($queryString);
        $mode  = $queryString[0];
        $urlQuery = "?";
        $url = ""; 
        switch($mode){
            case 1:
                $params = array("firstname","lastname","company","records");
                for($i=1;$i<count($queryString);$i++){
                    $urlQuery .= $params[$i-1]."=".$queryString[$i]."&";
                }
                //$urlQuery = "?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
				$urlQuery .= "sessionid=".$session_id;
                $url = $this->execsSearchlink;
                break;
            case 2:
                $params = array("Id","Parts");
                for($i=1;$i<count($queryString);$i++){
                    $urlQuery .= $params[$i-1]."=".$queryString[$i]."&";
                }
                //$urlQuery = "?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
				$urlQuery .= "sessionid=".$session_id;
                $url = $this->execslink;
                break;
            case 3:
                $params = array("QueryString");
                for($i=1;$i<count($queryString);$i++){
                    $urlQuery .= $params[$i-1]."=".rawurlencode($queryString[$i])."&";
                }
                //$urlQuery = "?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
                $urlQuery .= "SearchResultPartList=KeywordSummary|MetadataSuggestion|companySuggestion&SearchMode=all&SourceGenre=Publications&sessionid=".$session_id;
                //echo $queryString[1];die();
                $url = $this->articleSearchlink;
                break;
            case 4:
                $params = array("Id","parts");
                for($i=1;$i<count($queryString);$i++){
                    $urlQuery .= $params[$i-1]."=".$queryString[$i]."&";
                }
                //$urlQuery = "?firstname='.$fname.'&lastname='.$lname.'&company='.$company.'&records='.$records.'&sessionid='.$session_id;
				$urlQuery .= "sessionid=".$session_id;
                $url = $this->articlelink;
                break;
            default:
                break;
        }


        $ch = curl_init($url.$urlQuery);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }

    }

    function djarticle_get(){
        $session_id = $this->session->userdata('sessionid');
        $id = $this->get('id');
        $parts = $this->get('parts');
        //Biography
        //echo "ss".$fname.$lname;

        //die();
        $url = 'http://api.beta.dowjones.com/api/1.0/Content/article/xml?Id='.$id.'&Parts='.$parts.'&SessionId='.$session_id;
        //echo $url;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }
    }


    function wiki_get(){
        $session_id = $this->session->userdata('sessionid');
        $text = $this->get('text');
        $section = $this->get('section');
        //echo "ss".$fname.$lname;

        //die();
        $url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=xml&titles='.$text.'&rvsection='.$section.'&rawcontinue';
        //echo $url;
        //$url = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=Rupert_Murdoch&rvsection=0&rawcontinue';
        $ch = curl_init($url);
        //$xml_data = '<SessionRequest><Email>sachin.rajput@dowjones.com</Email><Password>Sachin21</Password><Format>xml</Format></SessionRequest>';
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2"); // required by wikipedia.org server; use YOUR user agent with YOUR contact information. (otherwise your IP might get blocked)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($c);
        //print $c;
        $xml = simplexml_load_string($c);
        //var_dump($xml);
        //echo "<br/>---------==============-----------<br/>";
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);//$this->json_decode_nice($json,TRUE);
        //var_dump($array);
        if($array)
        {
            $this->response($array, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Data could not be found'), 404);
        }
    }

    public function json_decode_nice($json, $assoc = FALSE){
        $json = str_replace(array("\n","\r"),"",$json);
        $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
        return json_decode($json,$assoc);
    }

    function user_post()
    {
        //$this->some_model->updateUser( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');

        $this->response($message, 200); // 200 being the HTTP response code
    }

    function user_delete()
    {
        //$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');

        $this->response($message, 200); // 200 being the HTTP response code
    }

    function users_get()
    {
        //$users = $this->some_model->getSomething( $this->get('limit') );
        $users = array(
            array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
            array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
            3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => array('hobbies' => array('fartings', 'bikes'))),
        );

        if($users)
        {
            $this->response($users, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }


    public function send_post()
    {
        var_dump($this->request->body);
    }


    public function send_put()
    {
        var_dump($this->put('foo'));
    }




}