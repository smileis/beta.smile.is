<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Filename:    Ajax_model
 * Purpose:     Way to add methods to support our ajax calls from Frontend
 *
 * Usage:       Frontend Developer should pass on the function name to
 *              Backend Developer
 *
 * @author sachin
 */

class Ajax_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('db_model','database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    public function commentdemo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        $comment = $this->input->post('comment');
        if($comment == "hii") echo "Hello! there!";
        else if ($comment == "bye") echo "Good Night!";
        else echo $comment;
    }

    public function getdemo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        $demovar = array();
        $demovar['a1'] = "ggg";
        $demovar['a2'] = "ggg2";
        echo $demovar['a1'];
        //echo "Hey there !";
    }

    public function demo(){
        //var_dump($_POST);
        //echo "the data here will be handled: ";
        $demovar = array();
        $demovar['a1'] = "ggg";
        $demovar['a2'] = "ggg2";
        echo $demovar['a2'];
        //echo "Hey there !";
    }

}