<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Design_model
 * An easier way to construct your design structure
 * and pass it to create a nice looking page.
 *
 * @author sachin
 */
class abstract Design_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	private $design_name = "";
	private $version = 1.0;
	private $type = "";
	private $place_holder1;
	private $place_holder2;
	
	//public 
}
?>