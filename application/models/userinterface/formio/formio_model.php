<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Formio
 * An easier way to construct elements of HTML form 
 * future work : make theme dynamic in this retrieve theme from DB 
 * @author sachin
 */
 
class Formio_model extends CI_Model {

	public $element_type;
	
	public function generateInputTextBox($label,$placeholder,$name) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-5">
			    <input type="text" class="form-control" placeholder="'.$placeholder.'" name="'.$name.'">
			  </div>
			</div>
		';
	}
	
	public function generateRadioBox($label,$valarray,$textarray,$name) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-5">';
		
		$len = count($valarray);
		for($i=0;$i<$len;$i++) {
			print '
				<div class="radio">
				  <label>
				    <input type="radio" name="'.$name.'" id="'.$name.($i+1).'" value="'.$valarray[$i].'"
				   ';
				    if($i==0) print 'checked';
				    print '>
				    '.$textarray[$i].'
				  </label>
				</div>
			';
		}
			    
		print '
			  </div>
			</div>
		';
	}
	
	public function generateTextAreaBox($label,$placeholder,$name) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-5">
			    <textarea class="form-control" rows="5" placeholder="'.$placeholder.'" name="'.$name.'"></textarea>
			  </div>
			</div>
		';
	}
	
	public function generateCheckBox($label,$valarray,$textarray,$name) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-5">';
		
		$len = count($valarray);
		for($i=0;$i<$len;$i++) {
			print '
				  <label class="checkbox-inline">
				    <input type="checkbox" name="'.$name.'" id="'.$name.($i+1).'" value="'.$valarray[$i].'" >'.$textarray[$i].'
				  </label>
			';
		}
			    
		print '
			  </div>
			</div>
		';
	}
	
	public function generateSelectBox($label,$valarray,$textarray,$name) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-2">
			  	<select class="form-control" name="'.$name.'">';
		
				$len = count($valarray);
				for($i=0;$i<$len;$i++) {
					print '
						    <option value="'.$valarray[$i].'" >'.$textarray[$i].'</option>
						  ';
				}
			    
		print '
				</select>
			  </div>
			</div>
		';
	}
	
	public function generateCLIEditorBox($name,$label) {
		print '
			<div class="form-group">
			  <label class="col-lg-2 control-label">'.$label.'</label>
			  <div class="col-lg-6">
			    <textarea class="cleditor" name="'.$name.'"></textarea>
			  </div>
			</div>
		';
	}
	
	public function generateButton($type,$label) {
		print '
			<div class="form-group">
			  <div class="col-lg-offset-2 col-lg-6">';
		
		switch ($type) {
			case 'default':
				print '<button type="button" class="btn btn-sm btn-default">'.$label.'</button>';
				break;
			case 'primary':
				print '<button type="button" class="btn btn-sm btn-primary">'.$label.'</button>';
				break;
			case 'success':
				print '<button type="button" class="btn btn-sm btn-success">'.$label.'</button>';
				break;
			case 'info':
				print '<button type="button" class="btn btn-sm btn-info">'.$label.'</button>';
				break;
			case 'warning':
				print '<button type="button" class="btn btn-sm btn-warning">'.$label.'</button>';
				break;
			case 'danger':
				print '<button type="button" class="btn btn-sm btn-danger">'.$label.'</button>';
				break;
			
		}	  
		
		print ' 
			  </div>
			</div>
		';
	}
	
}
?>