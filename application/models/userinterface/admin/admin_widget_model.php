<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Admin_menu_model
 * An easier way to construct elements of HTML form
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Admin_widget_model extends CI_Model {

    private $widgetName;
    private $column_array;
    private $field_array;
    private $unsetfields_array;
    private $display_array;
    private $relation_array;
    private $actions_array;
    private $tablename;
    private $callback_after_insert_array;
    private $callback_after_update_array;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('file');
        $this->lang->load('auth');
        $this->load->model('db_model','database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    public function setWidgetName($widgetNameIn){
        $this->widgetName = $widgetNameIn;
        //fire select query here:
        $this->db->where('widgetName', $widgetNameIn);
        $query = $this->db->get('ADMIN_Widget_View');
        $result = $query->result();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $this->column_array = explode(',',$row->columns);
                $this->field_array = explode(',',$row->fields);
                $this->unsetfields_array = explode(',',$row->unsetfields);
                $this->display_array = explode(',',$row->display_name);
                $this->relation_array = explode(',',$row->setrelation);
                $this->actions_array = explode(',',$row->actions);
                $this->callback_after_insert_array = explode(',',$row->afterinsert);
                $this->callback_after_update_array = explode(',',$row->afterupdate);
                $this->tablename = $row->table_name;
                $this->widgetName = $row->widgetName;
            }
        }
    }

    public function getWidgetName(){
        return $this->widgetName;
    }

    public function getTableName(){
        return $this->tablename;
    }

    public function getColumns(){
        return $this->column_array;
    }

    public function getFields(){
        return $this->field_array;
    }

    public function getUnsetFields(){
        return $this->unsetfields_array;
    }

    public function getDisplays(){
        return $this->display_array;
    }

    public function getRelations(){
        //var_dump($this->relation_array);
        if(!empty($this->relation_array[0])){
            $relations = array();
            $count = count($this->relation_array);
            $count = $count / 3;
            $j=0;
            for($i=0;$i<$count;$i++){
                $temp = array($this->relation_array[$j]);
                $j++;
                array_push($temp,$this->relation_array[$j]);
                $j++;
                array_push($temp,$this->relation_array[$j]);
                $relations[$i] = $temp;
                $j++;
            }

            return $relations;
        }
        return null;
    }

    public function getActions(){
        if(!empty($this->actions_array[0])) {
            return $this->actions_array;
        }
        return null;
    }

    public function getAfterinsert(){
        return $this->callback_after_insert_array;
    }

    public function getAfterupdate(){
        return $this->callback_after_update_array;
    }

    public function generateTemplateFile($pageid){
        //get the pageid, get url where to generate file
        //generate the code by reading the code entries

        $this->db->where('id', $pageid);
        $query = $this->db->get('UI_Page');
        $result = $query->result();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                //get file location
                /* $module = 'sachin';
                $blocks = "<div>$module</div>";
                $codee = $blocks;
                $layout = "<div>$codee</div>"; */

                $data = "<div class=\"container\">".$this->generateCode($pageid)."</div>";
                $filePath = FCPATH.VIEWPATH.$row->templateurl;
                $dirPath = dirname(FCPATH.VIEWPATH.$row->templateurl);
                //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
                if(!is_dir($dirPath)){
                    mkdir($dirPath,0755,TRUE);
                }
                //echo $dirPath.'<br/>';
                //echo $filePath;
                //die();
                if ( ! write_file($filePath, $data))
                {
                    echo 'Unable to write the file';
                }
                else
                {
                    echo 'File written!';
                    //redirect to http://beta.smile.is/admin/manage/managewidget/pages
                    redirect('admin/manage/managewidget/pages','refresh');
                }
            }
        }
    }

    public function generateCode($pageid){
        $code = '';
        $this->db->where('id', $pageid);
        $query = $this->db->get('UI_Page');
        $result = $query->result();
        $colsCode = array();
        $finalCode = "";
        $colcount = 0;
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $layoutid = $row->admin_layouts_id;
                $this->db->where('admin_layouts_id', $layoutid);
                $query1 = $this->db->get('admin_layout_config');
                $result1 = $query1->result();

                if ($query1->num_rows() > 0)
                {
                    foreach ($result1 as $row1)
                    {
                        $rowcount = $row1->numberofrows;
                        $colcount = $row1->numbercols;
                        $colconfigs = explode(':',$row1->colsconfig);
                        //1:3,9 $colconfig[0]
                        for($i=0;$i<$rowcount;$i++) {
                            $colconfig = explode(',',$colconfigs[$i]);
                            $code = "<div class=\"row clearfix\">";
                            for ($j = 0; $j < $colcount; $j++) {
                                $code .= "<div class=\"col-md-$colconfig[$j] column\">";
                                $code .= $this->generateBlockCode($pageid, ($i+1).':'.($j+1));
                                $code .= "</div>";
                            }
                            $code .= "</div>";
                            $finalCode .= $code;
                        }
                    }
                }
            }
        }

        /* for($i=0;$i<$colcount;$i++){
            $code .= $colsCode[$i];
        } */
        return $finalCode;
    }

    public function generateBlockCode($pageid,$colrowno){
        $blockCode = '';
        $this->db->where('UI_Page_id', $pageid);
        $this->db->where('colrowno', $colrowno);
        $this->db->where('status', 1);
        $this->db->order_by("position", "asc");

        $query = $this->db->get('UI_Page_blocks');
        $result = $query->result();
        $colsCode = array();
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $blockid = $row->admin_blocks_id;
                $pageblockid = $row->id;
                $this->db->where('admin_blocks_id', $blockid);
                $query1 = $this->db->get('admin_block_config');
                $result1 = $query1->result();

                if ($query1->num_rows() > 0)
                {
                    foreach ($result1 as $row1)
                    {
                        $modulecode = $this->generateModuleCode($pageid,$pageblockid);
                        $codes = $row1->blockcode;
                        if(empty($codes)){
                            $code = $modulecode;
                            $blockCode .= $code;
                        } else {
                            $codearr = explode("--",$codes);
                            $code = $codearr[0].$modulecode.$codearr[1];
                            $blockCode .= $code;
                        }
                    }
                }
            }
        }
        return $blockCode;
    }

    public function generateModuleCode($pageid,$pageblockid){
        $modulecode = '';
        $this->db->where('UI_Page_id', $pageid);
        $this->db->where('UI_Page_blocks_id', $pageblockid);
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get('UI_Page_modules');
        $result = $query->result();
        //echo $this->db->last_query();
        //die();
        $colsCode = array();
        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $moduleid = $row->admin_modules_id;
                $this->db->where('admin_modules_id', $moduleid);
                $query1 = $this->db->get('admin_module_config');
                $result1 = $query1->result();

                if ($query1->num_rows() > 0)
                {
                    foreach ($result1 as $row1)
                    {
                        $modulecode = $row1->modulecode;
                    }
                }
            }
        }
        return $modulecode;
    }
}