<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Admin_menu_model
 * An easier way to construct elements of HTML form
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Admin_menu_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('db_model','database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    public function getMenu(){
        //fire a query to get menu which are parent
        $this->db->where('parentid', null);
        $this->db->order_by('position','asc');
        $query = $this->db->get('ADMIN_Menu');
        $result = $query->result();

        //print_r($this->db->last_query());
        //die();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                //create array of values
                $data = array();
                $data['name'] = $row->menuname;
                $data['desp'] = $row->description;
                $data['url'] = base_url($this->adminURLprefix.$row->urlname);
                $data['parentid'] = $row->parentid;
                $data['id'] = $row->id;
                $data['class'] = $row->class;

                //append sub-menu
                $data['submenu'] = $this->getSubMenu($data['id']);

                //append to array ...
                array_push($menuArray,$data);
            }
        }
        return $menuArray;
    }

    public function getSubMenu($parentid){
        //fire a query to get sub-menu for $parentid
        $query = $this->db->where('parentid', $parentid)
            ->order_by('position','asc')
            ->get('ADMIN_Menu');
        $result = $query->result();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                //create array of values
                $data = array();
                $data['sub_name'] = $row->menuname;
                $data['sub_desp'] = $row->description;
                $data['sub_url'] = base_url($this->adminURLprefix.$row->urlname);
                $data['sub_parentid'] = $row->parentid;
                $data['sub_id'] = $row->id;
                $data['sub_class'] = $row->class;

                //append to array ...
                array_push($menuArray,$data);
            }
        }

        return $menuArray;
    }

    public function getTopMenuName($url){

        //fire a query to get sub-menu for $parentid
        $query = $this->db->where('urlname', $url)
            ->limit(1)
            ->get('ADMIN_Menu');
        $result = $query->result();
        $data = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                //create array of values
                $parentid = $row->parentid;
				$data['pagename'] = $row->menuname;
                $query1 = $this->db->where('id', $parentid)
                    ->limit(1)
                    ->get('ADMIN_Menu_Parent');
                $result1 = $query1->result();
                if ($query1->num_rows() > 0)
                {
                    foreach ($result1 as $row1)
                    {
                        $data['menuname']  = strtolower($row1->menuname);
                    }
                }
            }
        }

        return $data;
    }
}