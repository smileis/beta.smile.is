<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Admin_menu_model
 * An easier way to construct elements of HTML form
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Widget_callbacks extends CI_Model {


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('file');
        $this->lang->load('auth');
        $this->load->model('db_model','database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    public function generateTemplateFile($post_array,$primary_key){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->templateurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->templateurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/template','refresh');
        }
    }

    public function generateHeaderFile($post_array,$primary_key){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->headerurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->headerurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/headers','refresh');
        }
    }

    public function generateFooterFile($post_array,$primary_key){
        //$data,$location,$mode
        //$data = $this->generateCode($pageid);
        $filePath = FCPATH.VIEWPATH.$post_array->footerurl;
        $dirPath = dirname(FCPATH.VIEWPATH.$post_array->footerurl);
        //$this->ftp->mkdir('/public_html/profile_pictures/username/', DIR_WRITE_MODE);
        if(!is_dir($dirPath)){
            mkdir($dirPath,0755,TRUE);
        }
        //echo $dirPath.'<br/>';
        if ( ! write_file($filePath, $post_array->code))
        {
            echo 'Unable to write the file';
        }
        else
        {
            echo 'File written!';
            //redirect to http://beta.smile.is/admin/manage/managewidget/pages
            redirect('admin/manage/managewidget/footers','refresh');
        }
    }

}
?>