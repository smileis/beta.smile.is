<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Design_model
 * An easier way to construct your design structure
 * and pass it to create a nice looking page.
 *
 * @author sachin
 */
abstract class AbstractPageFactory extends CI_Model {

	public abstract function createPage();
	
	public abstract function openPage();
	
	public abstract function deletePage();
	
	//public 
}
?>