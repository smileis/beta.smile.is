<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Widgets
 * An easier way to construct widgets 
 * future work : make theme dynamic in this retrieve theme from DB 
 * @author sachin
 */
class Widgets_model extends CI_Model {

	public $element_type;
	
	public function widgetHeader($label,$placeholder,$name) {
		print '
			<div class="widget">
				<div class="widget-head">
                	<div class="pull-left">'.$label.'</div>
                  	<div class="widget-icons pull-right">
                    	<a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
                    	<a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  	</div>  
                	<div class="clearfix"></div>
            	</div>
            	<div class="widget-content">
		';
	}
	
	public function widgetFooter() {
		print '
					<div class="widget-foot">
						<ul class="pagination pagination-sm pull-right">
	                      <li><a href="#">Prev</a></li>
	                      <li><a href="#">1</a></li>
	                      <li><a href="#">2</a></li>
	                      <li><a href="#">3</a></li>
	                      <li><a href="#">4</a></li>
	                      <li><a href="#">Next</a></li>
	                    </ul>
			                  
			           	<div class="clearfix"></div> 
			
			        </div>
            	</div>
            </div>
		';
	}
	
	
}
?>