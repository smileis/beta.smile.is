<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Layout_model
 * An easier way to construct layout
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Layout_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('db_model', 'database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }


}