<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Variables_model
 * An easier way to add variables to pages
 * future work : make theme dynamic in this retrieve theme from DB
 * @author sachin
 */

class Variables_model extends CI_Model
{

    private $pageTitle;
    private $pageID;
    public $thisObjDwoo;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('db_model', 'database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    public function setpageTitle($pageTitleIn){
        $this->pageTitle = $pageTitleIn;
    }

    public function getpageTitle(){
        return $this->pageTitle;
    }

    public function setpageID($pageIDIn){
        $this->pageID = $pageIDIn;
    }

    public function getpageID(){
        return $this->pageID;
    }

    public function setthisObjDwoo($thisObjDwooIn){
        $this->thisObjDwoo = $thisObjDwooIn;
    }

    public function loadModalVars(){
        $modalVar = array();
        $modalVar['modal_id'] = 123;
        $modalVar['modal_title'] = 'demo titllle';
        $this->thisObjDwoo->assign('modal_id',$modalVar['modal_id']);
        $this->thisObjDwoo->assign('modal_title',$modalVar['modal_title']);
        return $modalVar;
    }
	
	public function loadExecsObj(){
		//get username 
		$username = $this->session->userdata('username');
		$this->db->where('username', $username);
        $query = $this->db->get('users_executives');
        $result = $query->result();
		$execslist = "";
		if ($query->num_rows() > 0)
        {
			foreach ($result as $row)
            {
                $execslist = $row->execslist;
            }
		}
		$names = "";
		$execlist = json_decode($execslist,true);//explode("|",$execslist);
		$execsObj = array();
		$articleArr = array();
		
		//invoke API for each exec 
		foreach($execlist as $execid){

            $this->db->where('execid', $execid);
            $query = $this->db->get('execs_data');
            $result = $query->result();
            if ($query->num_rows() > 0)
            {
                //create $execObj
                foreach ($result as $row)
                {
                    $execdataObj = json_decode($row->data,true);
                    $execObj = array();
                    $execObj['Executive'] = new stdClass();
                    $execObj['Executive']->FirstName = $execdataObj['FirstName'][0];
                    $execObj['Executive']->LastName = $execdataObj['LastName'][0];
                    $execObj['Executive']->FullName = $execdataObj['FullName'][0];
                    $execObj['Executive']->DisplayName = $execdataObj['DisplayName'][0];
                    $execObj['Executive']->FCode = $execdataObj['FCode'][0];
                    //var_dump($execObj['Executive']->FirstName);

                    //die();
                }


            } else {
                $params = array(
                    $execid,
                    'All|CompensationEx'
                );
                $execXML = $this->dowjonesapi->invokeAPI('execdetail',$params);
                $execObj = $this->dowjonesapi->getExecutiveArray($execXML);
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName.'+'.$execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                //.$execObj['Executive']->CompanyName.'+'
            }


			
			array_push($execsObj,$execObj);
		}
		$this->thisObjDwoo->assign('execsObj',$execsObj);
	}

    public function loadarticlePageObj($page,$execid,$articleid){
        //var_dump($execid."-".$articleid);
        //die();

        $params = array(

            'Id' => $articleid,
            'parts' => 'FULR'
        );

        $articleObj = $this->dowjonesapi->invokeAPI('articledetail', $params, true);
        $articleArr = array();
        //$articleObj = "";
        //$articleObj = $this->dowjonesapi->invokeAPI('articlesearch', $params, true);
        //$article = array();
        //var_dump($articleObj);
        //die();
        //foreach($execObj['LatestNews']->NewsArticle as $item){
        $this->db->where('execid', $execid);
        $query = $this->db->get('execs_data');
        $result = $query->result();

        if ($query->num_rows() > 0) {
            //create $execObj
            foreach ($result as $row) {
                $execdataObj = json_decode($row->data, true);
                $execObj = array();
                $execObj['Executive'] = new stdClass();
                $execObj['Executive']->FirstName = $execdataObj['FirstName'][0];
                $execObj['Executive']->LastName = $execdataObj['LastName'][0];
                $execObj['Executive']->FullName = $execdataObj['FullName'][0];
                $execObj['Executive']->DisplayName = $execdataObj['DisplayName'][0];
                $execObj['Executive']->FCode = $execdataObj['FCode'][0];
                //var_dump($execObj['Executive']->FirstName);

                //die();
            }
            $fullname = $execObj['Executive']->FullName;
            $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
            $execFcode = $execObj['Executive']->FCode;
        } else {
            $params = array(
                $execid,
                'All|CompensationEx'
            );
            $execXML = $this->dowjonesapi->invokeAPI('execdetail', $params);
            $execObj = $this->dowjonesapi->getExecutiveArray($execXML);
            $fullname = $execObj['Executive']->FullName;
            $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
            $execFcode = $execObj['Executive']->FCode;
        }
            $itemarr = array();
        foreach ($articleObj->Articles->Article as $item) {

            $headline = $item->Headline;
            $text = $item->Snippet;
            $time = $item->PubDateTime;
            $articleID = $item->ArticleId;
            $body = $item->Body;
            $leadParagraph = $item->LeadParagraph;
            $trailParagraph = $item->TailParagraphs;

            $bodyParagraphs = array();
            foreach ($body->Paragraph as $paragraphs) {
                $pitems = $paragraphs->PItems;
                array_push($bodyParagraphs,$pitems);
            }

            $leadParagraphs = array();
            foreach ($leadParagraph->Paragraph as $paragraphs) {
                $pitems = $paragraphs->PItems;
                array_push($leadParagraphs,$pitems);
            }

            $trailParagraphs = array();
            foreach ($trailParagraph->Paragraph as $paragraphs) {
                $pitems = $paragraphs->PItems;
                array_push($trailParagraphs,$pitems);
            }

            //var_dump($bodyParagraphs); die();

            $itemarr = array(
                "startDate" => "$time",
                "headline" => "$headline",
                "text" => "$text",
                "articleID" => "$articleID",
                "execFullName" => "$fullname",
                "execImgURL" => "$imgUrl",
                "execId" => "$execFcode",
                "body" => $bodyParagraphs,
                "leadParagraph" => $leadParagraphs,
                "trailParagraph" => $trailParagraphs
            );

            //array_push($articleArr, $itemarr);
        }

        //var_dump($itemarr);
        //die();

        $this->thisObjDwoo->assign('articlePageObj',$itemarr);
    }
	
	public function loadArticlesObj(){
		//get username 
		$username = $this->session->userdata('username');
		$this->db->where('username', $username);
        $query = $this->db->get('users_executives');
        $result = $query->result();
		$execslist = "";
		if ($query->num_rows() > 0)
        {
			foreach ($result as $row)
            {
                $execslist = $row->execslist;
            }
		}
		$names = "";
		$execlist = json_decode($execslist,true);
		$execsObj = array();
		$articleArr = array();

        //echo count($execlist);die();

        $no_of_execs = count($execlist);

        $default_limit = 25; //this can be updated later
        $actual_limit = $default_limit; // this will get value from db or page
        $record_limit = ceil($default_limit / $no_of_execs);
        $article_counter = 0;

		//invoke API for each exec 
		foreach($execlist as $execid) {

            $this->db->where('execid', $execid);
            $query = $this->db->get('execs_data');
            $result = $query->result();

            if ($query->num_rows() > 0) {
                //create $execObj
                foreach ($result as $row) {
                    $execdataObj = json_decode($row->data, true);
                    $execObj = array();
                    $execObj['Executive'] = new stdClass();
                    $execObj['Executive']->FirstName = $execdataObj['FirstName'][0];
                    $execObj['Executive']->LastName = $execdataObj['LastName'][0];
                    $execObj['Executive']->FullName = $execdataObj['FullName'][0];
                    $execObj['Executive']->DisplayName = $execdataObj['DisplayName'][0];
                    $execObj['Executive']->FCode = $execdataObj['FCode'][0];
                    //var_dump($execObj['Executive']->FirstName);

                    //die();
                }
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                $execFcode = $execObj['Executive']->FCode;

                array_push($execsObj, $execObj);

                $params = array(
                    'QueryString' => $execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName,
                    'Records' => $record_limit,
                    'SearchResultPartList' => 'KeywordSummary|MetadataSuggestion|companySuggestion',
                    'DuplicationMode' => 'off',
                    'SearchMode' => 'all',
                    'SourceGenre' => 'Publications'
                );

                $articleObj = $this->dowjonesapi->invokeAPI('articlesearch', $params, true);
                //$article = array();

                //foreach($execObj['LatestNews']->NewsArticle as $item){
                foreach ($articleObj->Articles->Article as $item) {
                    $article_counter++;
                    if ($article_counter > $actual_limit) break;
                    $headline = $item->Headline;
                    $text = $item->Snippet;
                    $time = $item->PubDateTime;
                    $articleID = $item->ArticleId;

                    $itemarr = array(
                        "startDate" => "$time",
                        "headline" => "$headline",
                        "text" => "$text",
                        "execFullName" => "$fullname",
                        "execImgURL" => "$imgUrl",
                        "execId" => "$execFcode",
                        "articleID" => "$articleID"
                    );
                    array_push($articleArr, $itemarr);
                }

            } else {

                $params = array(
                    $execid,
                    'All|CompensationEx'
                );
                $execXML = $this->dowjonesapi->invokeAPI('execdetail', $params);
                $execObj = $this->dowjonesapi->getExecutiveArray($execXML);
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                //.$execObj['Executive']->CompanyName.'+'
                $execFcode = $execObj['Executive']->FCode;

                array_push($execsObj, $execObj);

                $params = array(
                    'QueryString' => $execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName,
                    'Records' => $record_limit,
                    'SearchResultPartList' => 'KeywordSummary|MetadataSuggestion|companySuggestion',
                    'DuplicationMode' => 'off',
                    'SearchMode' => 'all',
                    'SourceGenre' => 'Publications'
                );

                $articleObj = $this->dowjonesapi->invokeAPI('articlesearch', $params, true);
                //$article = array();

                //foreach($execObj['LatestNews']->NewsArticle as $item){
                foreach ($articleObj->Articles->Article as $item) {
                    $article_counter++;
                    if ($article_counter > $actual_limit) break;
                    $headline = $item->Headline;
                    $text = $item->Snippet;
                    $time = $item->PubDateTime;
                    $articleID = $item->ArticleId;

                    $itemarr = array(
                        "startDate" => "$time",
                        "headline" => "$headline",
                        "text" => "$text",
                        "execFullName" => "$fullname",
                        "execImgURL" => "$imgUrl",
                        "execId" => "$execFcode",
                        "articleID" => "$articleID"
                    );
                    array_push($articleArr, $itemarr);
                }

            }
		}
		usort($articleArr,'Variables_model::date_sort');

		//shuffle($articleArr);
        $this->thisObjDwoo->assign('articlesObj',$articleArr);
        //$this->thisObjDwoo->assign('execsObj',$execsObj);
        //return $modalVar;
    }

    public function loadExecsSingleObj($val1,$val2,$val3){
        //echo $execid;
        //die();
        $execid = $val3;
        //$execid = $params["val3"];
        $execsObj = array();
        $articleArr = array();

        //invoke API for each exec
        //foreach($execlist as $execid){

            $this->db->where('execid', $execid);
            $query = $this->db->get('execs_data');
            $result = $query->result();
            if ($query->num_rows() > 0)
            {
                //create $execObj
                foreach ($result as $row)
                {
                    $execdataObj = json_decode($row->data,true);
                    $execObj = array();
                    $execObj['Executive'] = new stdClass();
                    $execObj['Executive']->FirstName = $execdataObj['FirstName'][0];
                    $execObj['Executive']->LastName = $execdataObj['LastName'][0];
                    $execObj['Executive']->FullName = $execdataObj['FullName'][0];
                    $execObj['Executive']->DisplayName = $execdataObj['DisplayName'][0];
                    $execObj['Executive']->FCode = $execdataObj['FCode'][0];
                    //var_dump($execObj['Executive']->FirstName);

                    //die();
                }


            } else {
                $params = array(
                    $execid,
                    'All|CompensationEx'
                );
                $execXML = $this->dowjonesapi->invokeAPI('execdetail',$params);
                $execObj = $this->dowjonesapi->getExecutiveArray($execXML);
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName.'+'.$execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                //.$execObj['Executive']->CompanyName.'+'
            }

            array_push($execsObj,$execObj);
        //}
        $this->thisObjDwoo->assign('execsObj',$execsObj);
    }

    public function loadArticlesSingleObj($val1,$val2,$val3){
        //echo $execid;
        //die();
        $execid = $val3;
        //var_dump($params);
        //die();
        //$execid = $params['val3'];
        $execsObj = array();
        $articleArr = array();

        //echo count($execlist);die();

        //$no_of_execs = count($execlist);

        $default_limit = 25; //this can be updated later
        $actual_limit = $default_limit; // this will get value from db or page
        //$record_limit = ceil($default_limit / $no_of_execs);
        $article_counter = 0;

        //invoke API for each exec
        //foreach($execlist as $execid) {

            $this->db->where('execid', $execid);
            $query = $this->db->get('execs_data');
            $result = $query->result();

            if ($query->num_rows() > 0) {
                //create $execObj
                foreach ($result as $row) {
                    $execdataObj = json_decode($row->data, true);
                    $execObj = array();
                    $execObj['Executive'] = new stdClass();
                    $execObj['Executive']->FirstName = $execdataObj['FirstName'][0];
                    $execObj['Executive']->LastName = $execdataObj['LastName'][0];
                    $execObj['Executive']->FullName = $execdataObj['FullName'][0];
                    $execObj['Executive']->DisplayName = $execdataObj['DisplayName'][0];
                    $execObj['Executive']->FCode = $execdataObj['FCode'][0];
                    //var_dump($execObj['Executive']->FirstName);

                    //die();
                }
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                $execFcode = $execObj['Executive']->FCode;

                array_push($execsObj, $execObj);

                $params = array(
                    'QueryString' => $execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName,
                    'Records' => $actual_limit,
                    'SearchResultPartList' => 'KeywordSummary|MetadataSuggestion|companySuggestion',
                    'DuplicationMode' => 'off',
                    'SearchMode' => 'all',
                    'SourceGenre' => 'Publications'
                );

                $articleObj = $this->dowjonesapi->invokeAPI('articlesearch', $params, true);
                //$article = array();

                //foreach($execObj['LatestNews']->NewsArticle as $item){
                foreach ($articleObj->Articles->Article as $item) {
                    $article_counter++;
                    if ($article_counter > $actual_limit) break;
                    $headline = $item->Headline;
                    $text = $item->Snippet;
                    $time = $item->PubDateTime;
                    $articleID = $item->ArticleId;

                    $itemarr = array(
                        "startDate" => "$time",
                        "headline" => "$headline",
                        "text" => "$text",
                        "execFullName" => "$fullname",
                        "execImgURL" => "$imgUrl",
                        "execId" => "$execFcode",
                        "articleID" => "$articleID"
                    );
                    array_push($articleArr, $itemarr);
                }

            } else {

                $params = array(
                    $execid,
                    'All|CompensationEx'
                );
                $execXML = $this->dowjonesapi->invokeAPI('execdetail', $params);
                $execObj = $this->dowjonesapi->getExecutiveArray($execXML);
                $fullname = $execObj['Executive']->FullName;
                $imgUrl = $this->getProfileImgUrl($execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName);//.'+'.$execObj['Executive']->IndustryDescriptor);
                //.$execObj['Executive']->CompanyName.'+'
                $execFcode = $execObj['Executive']->FCode;

                array_push($execsObj, $execObj);

                $params = array(
                    'QueryString' => $execObj['Executive']->FirstName . '+' . $execObj['Executive']->LastName,
                    'Records' => $actual_limit,
                    'SearchResultPartList' => 'KeywordSummary|MetadataSuggestion|companySuggestion',
                    'DuplicationMode' => 'off',
                    'SearchMode' => 'all',
                    'SourceGenre' => 'Publications'
                );

                $articleObj = $this->dowjonesapi->invokeAPI('articlesearch', $params, true);
                //$article = array();

                //foreach($execObj['LatestNews']->NewsArticle as $item){
                foreach ($articleObj->Articles->Article as $item) {
                    $article_counter++;
                    if ($article_counter > $actual_limit) break;
                    $headline = $item->Headline;
                    $text = $item->Snippet;
                    $time = $item->PubDateTime;
                    $articleID = $item->ArticleId;

                    $itemarr = array(
                        "startDate" => "$time",
                        "headline" => "$headline",
                        "text" => "$text",
                        "execFullName" => "$fullname",
                        "execImgURL" => "$imgUrl",
                        "execId" => "$execFcode",
                        "articleID" => "$articleID"
                    );
                    array_push($articleArr, $itemarr);
                }

            }
        //}
        usort($articleArr,'Variables_model::date_sort');

        //shuffle($articleArr);
        $this->thisObjDwoo->assign('articlesObj',$articleArr);
        //$this->thisObjDwoo->assign('execsObj',$execsObj);
        //return $modalVar;
    }

    public static function date_sort($a, $b) {
        return strcmp($b['startDate'], $a['startDate']); //only doing string comparison
    }

    public function loadpinsSaveandloadObj($val1,$val2,$val3){
        //echo $val3.'<br/> ask for more details and save exec / article id in db and redirect to pins page';
        //die();
        if($val3 == 999){
            echo "yooo";
            die();
        } else {
            $data = explode("-", $val3);
            $execid = $data[0];
            $eventid = $data[1];

            $dataArr = array("execid" => $execid, "eventid" => $eventid);

            $this->thisObjDwoo->assign('pinsSaveandloadObj', $dataArr);
        }

    }

    public function loadpinsObj(){
        //echo "pins will be loaded from db";
    }
	
	public function loadSearchCheck(){
		$check = false;
		$searchKeywords = $this->input->post('search');
		$final = $searchKeywords;
		if($searchKeywords) 
		{
			$searchKeywords = str_replace(" ","+",$searchKeywords);
			$searchKeyword = explode("+",$searchKeywords);
			$check = true;
			$params = array(
				$searchKeyword[0],
				(count($searchKeyword)>1)?$searchKeyword[1]:"",
				(count($searchKeyword)>2)?$searchKeyword[2]:"",
				10
			);
			$execXML = $this->dowjonesapi->invokeAPI('execsearch',$params);
			$profiles = $execXML->Executives->ExecutiveProfile;
            //echo $xml->Records;
			$resultsExecs = array();
            foreach ($profiles as $profile)
            {
				array_push($resultsExecs, $profile);
			}
			$this->thisObjDwoo->assign('resultsExecs',$resultsExecs);
		}
		$this->thisObjDwoo->assign('searchCheck',$check);
		$this->thisObjDwoo->assign('searchKeyword',$final);
	}
	
	public function getProfileImgUrl($val){
        $val = str_replace(" ","+",$val);
        //echo $val.'<br/>';
        //to fetch all hyperlinks from a webpage
        $links = array();
//var_dump($hh);
        /* foreach($hh->find('a') as $a) {
         $links[] = $a->href;
        }
        print_r($links);  */
		//echo $val; die();
        $url = "https://www.google.com/search?q=".$val."+photo&rlz=1C5CHFA_enUS543US543&es_sm=91&source=lnms&tbm=isch&sa=X&ei=t1spVaSnCay1sQTar4CYBg&ved=0CAgQ_AUoAg&biw=1280&bih=705#q=".$val."+photo&tbm=isch&tbs=isz:m";
//$fp = fopen("example_homepage.txt", "w");
        $ch = curl_init();
//curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ff = curl_exec($ch);
        //var_dump($ff);

        $html = str_get_html($ff);

        foreach($html->find('img') as $a) {
            $links[] = $a->src;
        }
        curl_close($ch);
        //var_dump($ff);
        //print_r($links[0]);
        //die();
        if(count($links)>0) return $links[0];
        return null;
    }
	

    public function loadUserDetails(){
        $userdetails = array();
        $userdetails = $this->auth->getUserArray();
        $this->thisObjDwoo->assign('userdetails',$userdetails);
        // we can use dynamic header css styles according to pagename / pageid
        return $userdetails;
    }

    public function loadHeaderVar(){
        $headerVar = array();
        $headerVar['title'] = $this->getpageTitle();
        $this->thisObjDwoo->assign('title',$headerVar['title']);
        // we can use dynamic header css styles according to pagename / pageid
        return $headerVar;
    }

    public function loadDemotest(){
        $demotestVar = array();
        $demotestVar['demotest'] = 'This is a demo test variable';
        $this->thisObjDwoo->assign('demotest',$demotestVar['demotest']);
        // we can use dynamic header css styles according to pagename / pageid
        return $demotestVar;
    }

    public function loadIssueVars(){
        $demotestVar = array();
        $demotestVar['demotest'] = 'Learn about our pr';
        $this->thisObjDwoo->assign('issueVars',$demotestVar['demotest']);
        // we can use dynamic header css styles according to pagename / pageid
        return $demotestVar;
    }




    public function getVarDetails(){
        $varArray = array();
        //$varArray[1] = 'homeVar';
        //$varArray[2] = 'profileVar';
        //$this->db->where('parentid', null);
        //$this->db->order_by('position','asc');
        $query = $this->db->get('UI_Variables');
        $result = $query->result();

        //print_r($this->db->last_query());
        //die();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $varArray[$row->id] = $row->name;
            }
        }
        return $varArray;
    }

    public function getVarFuncName($varID){
        $varFunc = "";
        //$varArray[1] = 'homeVar';
        //$varArray[2] = 'profileVar';
        $this->db->where('id', $varID);
        //$this->db->order_by('position','asc');
        $query = $this->db->get('UI_Variables');
        $result = $query->result();

        //print_r($this->db->last_query());
        //die();
        $menuArray = array();

        if ($query->num_rows() > 0)
        {
            foreach ($result as $row)
            {
                $varFunc = $row->function_name;
            }
        }
        return $varFunc;
    }


    /**
     * public function getHomeVar(){
    $homeVar = array();
    $homeVar['modal_id'] = 123;
    $homeVar['modal_title'] = 'demo titllle';
    //$homeVar['userdetails'] = $this->auth->getUserArray();
    //$homeVar['pagename'] = 123;
    $homeVar['demotest'] = 'geeee';
    return $homeVar;
    }

    public function getprofVar(){
    $profVar = array();
    $profVar['modal_id'] = 123;
    $profVar['modal_title'] = 'demo titllle';
    //$profVar['userdetails'] = $this->auth->getUserArray();
    //$profVar['pagename'] = 123;
    $profVar['demotest'] = 'geeee';
    return $profVar;
    }



    public function getnavVar(){
    $navVar = array();
    $navVar['userdetails'] = $this->auth->getUserArray();
    return $navVar;
    }

    public function getheaderVar(){
    $headerVar = array();
    $headerVar['title'] = $this->getpageTitle();
    // we can use dynamic header css styles according to pagename / pageid
    return $headerVar;
    }
     */


}

