<?php
/**
 * PHP grocery CRUD
 *
 * LICENSE
 *
 * Grocery CRUD is released with dual licensing, using the GPL v3 (license-gpl3.txt) and the MIT license (license-mit.txt).
 * You don't have to do anything special to choose one license or the other and you don't have to notify anyone which license you are using.
 * Please see the corresponding license file for details of these licenses.
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package    	grocery CRUD
 * @copyright  	Copyright (c) 2010 through 2012, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 * @version    	1.2
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 */

// ------------------------------------------------------------------------

/**
 * Grocery CRUD Model
 *
 *
 * @package    	grocery CRUD
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 * @version    	1.2
 * @link		http://www.grocerycrud.com/documentation
 */
class Sql_model  extends CI_Model  {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('date');
        $this->lang->load('auth');
        $this->load->model('db_model', 'database');

        //initialize db tables data
        $this->tables = $this->config->item('tables', 'auth');

        //Admin Prefix can be changed anytime for Security
        $this->adminURLprefix = 'admin/';
    }

    //insert into CRUD_modules_list (modulename,status,datecreated) values ('tryyy',1,now());
    //update CRUD_modules_list set modulename='try1' where id = 3;
    //delete from CRUD_modules_list where id = 3;
    public function runQuery($query,$mode=null){
        $result = mysql_query($query);
        if($mode == 1) return $result;
        if ($this->db->affected_rows() > 0){
            return 1;
        }
        $err['errmsg'] = $this->db->_error_message();
        $err['errno'] =$this->db->_error_number();

        return $err;
    }
}